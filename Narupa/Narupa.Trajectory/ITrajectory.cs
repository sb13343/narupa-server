﻿// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;

namespace Narupa.Trajectory
{
    /// <summary>
    ///     Interface to a molecular simulation trajectory.
    /// </summary>
    public interface ITrajectory : IList<IFrame>
    {
        /// <summary>
        ///     Number of frames in the trajectory.
        /// </summary>
        long NumberOfFrames { get; }

        /// <summary>
        ///     Add a frame to the trajectory.
        /// </summary>
        /// <param name="frame">A trajectory frame.</param>
        void AddFrame(IFrame frame);

    }
}