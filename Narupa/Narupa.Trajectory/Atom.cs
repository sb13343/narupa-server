// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Science;

namespace Narupa.Trajectory
{
    /// <summary>
    ///     Represents an atom in a <see cref="Topology" />, reminiscent of a PDB file.
    /// </summary>
    public class Atom
    {
        /// <summary>
        ///     The element of the atom.
        /// </summary>
        public Element Element;

        /// <summary>
        ///     The index of the atom in the topology.
        /// </summary>
        public int Index;

        /// <summary>
        ///     The name of the atom.
        /// </summary>
        public string Name;

        /// <summary>
        ///     The residue this atom belongs to.
        /// </summary>
        public Residue Residue;

        /// <summary>
        ///     The serial identifier for this atom.
        /// </summary>
        /// <remarks>
        ///     Comes from a PDB, will internally be set to the index of the atom in the topology if not provided.
        /// </remarks>
        public int Serial;

        
        /// <summary>
        /// Constructs an atom.
        /// </summary>
        /// <param name="index"> Index of the atom.</param>
        /// <param name="name">Name of the atom.</param>
        /// <param name="element">Element of the atom.</param>
        /// <param name="residue">Residue the atom belongs to.</param>
        /// <param name="serial">Serial number of the atom.</param>
        public Atom(int index, string name, Element element, Residue residue, int serial)
        {
            Index = index;
            Name = name;
            Element = element;
            Residue = residue;
            Serial = serial;
        }
    }
}