// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano.Science;

namespace Narupa.Trajectory
{
    /// <summary>
    ///     Basic interface for a molecular simulation topology.
    /// </summary>
    public interface ITopology
    {
        /// <summary>
        ///     Number of atoms in the topology.
        /// </summary>
        int NumberOfAtoms { get; }

        /// <summary>
        ///     The bonds between atoms in the topology.
        /// </summary>
        IEnumerable<BondPair> Bonds { get; }

        /// <summary>
        ///     The elements of the atoms in the topology.
        /// </summary>
        IList<Element> Elements { get; }
    
        /// <summary>
        ///     Add a bond to the topology.
        /// </summary>
        /// <param name="a">Index of first atom in bond.</param>
        /// <param name="b">Indexo f second atom in bond.</param>
        /// <returns></returns>
        BondPair AddBond(int a, int b);
    }
}