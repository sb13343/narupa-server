// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using Nano;
using Narupa.Trajectory.Load.PDB;

namespace Narupa.Trajectory.Load
{
    /// <summary>
    /// Methods for loading trajectories.
    /// </summary>
    public static class Loader
    {
        /// <summary>
        /// Loads a trajectory from the given path.
        /// </summary>
        /// <param name="trajectoryPath">The path to the trajectory file.</param>
        /// <param name="reporter">The reporter to print information to.</param>
        /// <param name="topologyPath">Path to a topology file.</param>
        /// <returns>A trajectory object.</returns>
        /// <exception cref="NotSupportedException">Invalid or unknown trajectory file.</exception>
        /// <remarks>
        /// The topology path should be used when the trajectory file contains incomplete or no topology information.
        /// Some trajectory files require a topology, such as DCD, while for others it is highly recommended e.g. XYZ.
        /// </remarks>
        public static ITrajectory LoadTrajectory(string trajectoryPath, IReporter reporter, string topologyPath = "")
        {
            ITopology topology = null;
            if (!string.IsNullOrEmpty(topologyPath)) topology = LoadTopology(topologyPath, reporter);

            var ext = Path.GetExtension(trajectoryPath)?.Trim().ToLower();
            if (ext == null)
                throw new NotSupportedException("Invalid trajectory file extension.");

            //TODO refactor this to use reflection, like ILoadable, so it is extendable.
            if (topology != null)
            {
                if (ext == ".pdb")
                    return LoadPdbTrajectory(trajectoryPath, topology, reporter);
                if (ext == ".xyz")
                    return LoadXyzTrajectory(trajectoryPath, topology, reporter);
                if (ext == ".dcd")
                    return LoadDcdTrajectory(trajectoryPath, topology, reporter);
                throw new NotSupportedException("Unknown or invalid trajectory extension.");
            }

            if (ext == ".pdb")
                return LoadPdbTrajectory(trajectoryPath, reporter);
            if (ext == ".xyz")
                return LoadXyzTrajectory(trajectoryPath, reporter);
            if (ext == ".dcd")
                throw new NotSupportedException("DCD trajectory requires a topology file.");
            throw new NotSupportedException("Unknown or invalid trajectory extension.");
        }

        private static ITrajectory LoadXyzTrajectory(string trajectoryPath, IReporter reporter)
        {
            return XYZ.XyzTrajectory.LoadTrajectory(trajectoryPath, reporter, true);
        }

        private static ITrajectory LoadPdbTrajectory(string trajectoryPath, IReporter reporter)
        {
            return PdbTrajectory.LoadTrajectory(trajectoryPath, reporter, false);
        }

        private static ITrajectory LoadPdbTrajectory(string trajectoryPath, ITopology topology, IReporter reporter)
        {
            return PdbTrajectory.LoadTrajectory(trajectoryPath, reporter, topology);
        }

        private static ITrajectory LoadXyzTrajectory(string trajectoryPath, ITopology topology, IReporter reporter)
        {
            return XYZ.XyzTrajectory.LoadTrajectory(trajectoryPath, reporter, topology);
        }

        private static ITrajectory LoadDcdTrajectory(string trajectoryPath, ITopology topology, IReporter reporter)
        {
            throw new NotImplementedException();
        }

        private static ITopology LoadTopology(string topologyPath, IReporter reporter)
        {
            var ext = Path.GetExtension(topologyPath)?.Trim().ToLower();
            if (ext == null)
                throw new Exception("Invalid topology file extension!");

            if (ext == ".pdb")
                return PdbTrajectory.LoadTopology(topologyPath, reporter);
            throw new NotSupportedException("Unknown or invalid topology extension.");
        }
    }
}