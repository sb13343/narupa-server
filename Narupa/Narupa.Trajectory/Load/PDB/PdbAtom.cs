﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Globalization;
using Nano;
using Nano.Science;
using SlimMath;

namespace Narupa.Trajectory.Load.PDB
{
    /// <summary>
    ///     Initialises the PdbAtom class.
    /// </summary>
    public class PdbAtom : IEquatable<PdbAtom>
    {
        private static readonly TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

        /// <summary>
        ///     The AChar Code for insertion of residues.
        /// </summary>
        public readonly char ACharCode;

        /// <summary>
        ///     The chain identifier the atom belongs to.
        /// </summary>
        public readonly char ChainIdentifier;

        /// <summary>
        ///     The element.
        /// </summary>
        public readonly string Element;

        /// <summary>
        ///     Whether this atom is a heterogen or not.
        /// </summary>
        public readonly bool IsHeterogen;

        /// <summary>
        ///     The name of the atom.
        /// </summary>
        public readonly string Name;

        /// <summary>
        ///     The residue number the atom belongs to.
        /// </summary>
        public readonly int ResidueId;

        /// <summary>
        ///     The name of the residue the atom belongs to.
        /// </summary>
        public readonly string ResidueName;

        /// <summary>
        ///     The serial number.
        /// </summary>
        public readonly int SerialNumber;

        /// <summary>
        ///     The absolute index of the atom within the PDB file.
        /// </summary>
        public int AbsoluteIndex;

        /// <summary>
        ///     The absolute index of the residue within the PDB file.
        /// </summary>
        public int AbsoluteResidueIndex;

        /// <summary>
        ///     The charge on the atom.
        /// </summary>
        public int Charge;

        /// <summary>
        ///     The fraction of unit cells that contain the atom in this particular location.
        /// </summary>
        public float Occupancy;

        /// <summary>
        ///     The position of the atom.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        ///     The segment identifier the atom belongs to.
        /// </summary>
        public string SegmentIdentifier;

        /// <summary>
        ///     The uncertainty factor in the atom's position due to thermal motions.
        /// </summary>
        public float TemperatureFactor;

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:Nano.Science.Simulation.PDB.PDBAtom" /> class.
        /// </summary>
        /// <param name="lineFromPdb"> The line from the pdb file with ATOM or HETATOM.</param>
        /// <param name="pdbFileStructure">Pdb file structure, used to provide context.</param>
        /// <param name="reporter"></param>
        /// <param name="extraParticleIdentifier"> Identifier for extra particles. </param>
        /// <param name="verbose">Whether to print detailed PDB file information.</param>
        public PdbAtom(string lineFromPdb, PdbFile pdbFileStructure, IReporter reporter,
            string extraParticleIdentifier = "EP", bool verbose = false)
        {
            reporter?.PrintDebug($"Reading ATOM from line: {lineFromPdb}");

            if (lineFromPdb.Contains("HETATM"))
                IsHeterogen = true;


            string ReadElement(string s)
            {
                return textInfo.ToTitleCase(s.Substring(76, 2).Trim().ToLower());
            }

            Vector3 ReadPosition(string line)
            {
                var x = float.Parse(line.Substring(30, 7).Trim(), CultureInfo.InvariantCulture);
                var y = float.Parse(line.Substring(38, 7).Trim(), CultureInfo.InvariantCulture);
                var z = float.Parse(line.Substring(46, 7).Trim(), CultureInfo.InvariantCulture);
                return new Vector3(x,y,z);
            }

            int ReadSerialNumber(string line)
            {
                var serialNumberStr = line.Substring(6, 5);
                return int.Parse(serialNumberStr.Trim(), CultureInfo.InvariantCulture);
            }

            try
            {
                SerialNumber = ReadSerialNumber(lineFromPdb);
            }
            catch (Exception e)
            {
                throw new Exception($"Failed to read serial number from pdb line {lineFromPdb}", e);
            }
            
            Name = lineFromPdb.Substring(12, 4).Trim();
            ResidueName = lineFromPdb.Substring(17, 3).Trim();
            var chainString = lineFromPdb.Substring(21, 1).Trim();
            if (chainString == "" || string.IsNullOrEmpty(chainString) || string.IsNullOrWhiteSpace(chainString))
                ChainIdentifier = 'A';
            else
                ChainIdentifier = chainString[0];
            ResidueId = int.Parse(lineFromPdb.Substring(22, 4).Trim(), CultureInfo.InvariantCulture);

            ACharCode = lineFromPdb[27];
            
            try
            {
                Element = ReadElement(lineFromPdb);
            }
            catch (Exception e)
            {
                reporter?.PrintWarning(ReportVerbosity.Normal,
                    $"Failed to read element string from line {lineFromPdb}, will proceed to guess based on atom name");
                
                if (Element == string.Empty)
                {
                    Element = PeriodicTable.GetElementProperties(Utility.GetElement(Name)).Symbol;
                }
            }

            try
            {
                Position = ReadPosition(lineFromPdb);
            }
            catch (Exception e)
            {
                throw new Exception($"Failed to read atom position from line: {lineFromPdb}!", e);
            }
           
            if (verbose)
            {
                if (lineFromPdb.Contains("HETATM"))
                    reporter?.PrintDebug("atom is heterogen.");

                reporter?.PrintDebug($"atom: Element {Element} ");
                reporter?.PrintDebug($"atom: Position {Position} ");
                reporter?.PrintDebug($"atom: SerialNumber {SerialNumber} ");
                reporter?.PrintDebug($"atom: Name {Name} ");
                reporter?.PrintDebug($"atom: ResidueName {ResidueName} ");
                reporter?.PrintDebug($"atom: ChainIdentifier {ChainIdentifier} ");
                reporter?.PrintDebug($"atom: ResidueID {ResidueId} ");
                reporter?.PrintDebug($"atom: ACharCode {ACharCode} ");
                reporter?.PrintDebug($"atom: Occupancy {Occupancy} ");
                reporter?.PrintDebug($"atom: TemperatureFactor {TemperatureFactor} ");
                reporter?.PrintDebug($"atom: SegmentIdentifier {SegmentIdentifier} ");
                reporter?.PrintDebug($"atom: Charge {Charge} ");

            }
        }

        /// <inheritdoc />
        public bool Equals(PdbAtom other)
        {
            if (other == null) return false;
            if (other.SerialNumber != SerialNumber) return false;
            if (other.Name != Name) return false;
            if (other.Element != Element) return false;
            return true;
        }

        /// <inheritdoc />
        public override bool Equals(object other)
        {
            if (other is PdbAtom)
                return Equals(other as PdbAtom);
            return false;
        }

        /// <summary>
        ///     Generates a hashcode for atom using its serial number.
        /// </summary>
        /// <remarks>
        ///     Warning - this method may result in erroneous behaviour if atoms do not have unique serial numbers. TODO Figure out
        ///     a better hash function?
        /// </remarks>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return SerialNumber;
        }
    }
}