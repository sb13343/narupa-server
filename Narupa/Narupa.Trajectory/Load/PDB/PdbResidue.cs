﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;

namespace Narupa.Trajectory.Load.PDB
{
    /// <summary>
    ///     Initialises the PdbResidue class.
    /// </summary>
    public class PdbResidue
    {
        /// <summary>
        ///     Residue number.
        /// </summary>
        public readonly int Number;

        /// <summary>
        ///     The absolute index of the residue in the pdb file.
        /// </summary>
        /// <remarks>
        ///     PDB files only count up to 10000 residues, then start repeating residue numbers,
        ///     this absolute count will sort all that out.
        /// </remarks>
        public int AbsoluteIndex;

        /// <summary>
        ///     The atoms contained in the PDBFile.
        /// </summary>
        public List<PdbAtom> Atoms = new List<PdbAtom>();

        /// <summary>
        ///     Creates a dictionary of atoms by their name.
        /// </summary>
        public Dictionary<string, PdbAtom> AtomsByName = new Dictionary<string, PdbAtom>();

        /// <summary>
        ///     Residue name.
        /// </summary>
        public string Name;

        /// <summary>
        ///     Initialises the properties of a residue.
        /// </summary>
        /// <param name="newResId"></param>
        /// <param name="newResName"></param>
        public PdbResidue(int newResId, string newResName)
        {
            Number = newResId;
            Name = newResName;
        }
    }
}