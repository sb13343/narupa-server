using System;
using System.Collections.Generic;
using System.IO;
using Nano.Science;
using SlimMath;

namespace Narupa.Trajectory.Load.XYZ
{
    /// <summary>
    /// Represents a frame in an XYZ file.
    /// </summary>
    public class XyzFrame
    {
        /// <summary>
        /// Names of atoms in XYZ file.
        /// </summary>
        public List<string> AtomNames = new List<string>();
        /// <summary>
        /// Positions of atoms in Angstroms.
        /// </summary>
        public List<Vector3> AtomPositions = new List<Vector3>();
        /// <summary>
        /// Number of atoms.
        /// </summary>
        public int NumberOfAtoms => AtomPositions.Count;

        /// <summary>
        /// Add an atom to the frame.
        /// </summary>
        /// <param name="name">Name of the atom.</param>
        /// <param name="position">Position of the atom.</param>
        public void AddAtom(string name, Vector3 position)
        {
            AtomNames.Add(name);
            AtomPositions.Add(position);
        }
    }

    /// <summary>
    /// Represents an XYZ file.
    /// </summary>
    public class XyzFile
    {
        /// <summary>
        /// The frames in the file.
        /// </summary>
        public List<XyzFrame> Frames = new List<XyzFrame>();
        
        /// <summary>
        /// An identifier for this file.
        /// </summary>
        public string Identifier;

        /// <summary>
        ///     Read XYZ file into the instance.
        /// </summary>
        /// <param name="path">Path to file</param>
        /// <param name="append">Whether to append to existing frames.</param>
        public void Read(string path, bool append = false)
        {
            if (append == false)
            {
                Frames.Clear();
                Identifier = Path.GetFileName(path);
            }

            var lineNumber = 0;
            using (var reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    var frame = ReadFrame(reader, ref lineNumber);
                    Frames.Add(frame);
                }
            }
        }

        private XyzFrame ReadFrame(StreamReader reader, ref int lineNumber)
        {
            var frame = new XyzFrame();
            var numAtomLine = reader.ReadLine();
            var success = int.TryParse(numAtomLine.Trim(), out var numberOfAtoms);
            if (success == false)
                throw new Exception("XYZ frame did not start with number of atoms.");
            lineNumber++;

            //skip the comment line.
            reader.ReadLine();
            lineNumber++;
            for (var i = 0; i < numberOfAtoms; i++)
            {
                var positionLine = reader.ReadLine();
                var split = positionLine.Trim().Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                var name = split[0];
                var pos = Vector3.Zero;
                for (var j = 0; j < 3; j++)
                {
                    success = float.TryParse(split[j + 1], out var p);
                    if (!success)
                        throw new Exception($"XYZ file line {lineNumber} invalid coordinate index {j}.");
                    pos[j] = p;
                }

                frame.AddAtom(name, pos);
                lineNumber++;
            }

            return frame;
        }
    }
}