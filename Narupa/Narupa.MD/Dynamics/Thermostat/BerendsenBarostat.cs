﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using SimboxApi;
using SimboxApi.Science.Simulation;
using SlimMath;

namespace SimboxDynamics
{
    /// <summary>
    /// Class BerendsenBarostat implementing a Berendsen Barostat for maintaining pressure at constant temperature.
    /// </summary>
    /// <seealso cref="SimboxApi.Science.Simulation.Barostat" />
    class BerendsenBarostat : Barostat
    {
        public float BerendsenCoupling { get; private set; }
        public float RescaleFreq { get; private set; }

        private float step = 1;
        private float timeStep;

        private float averagePressure = 0;

        /// <summary>
        /// Applies the barostat.
        /// </summary>
        /// <param name="system">The system.</param>
        public override void ApplyBarostat(AtomicSystem system)
        {
            step++;
            if (step < RescaleFreq)
            {
                float pressure = system.CalculateInstantaneousPressure();
                averagePressure += pressure;
                return;
            }
            averagePressure /= step;

            step = 0;

            float scaleFactor = (float)Math.Pow(1 - (timeStep / BerendsenCoupling) * (averagePressure - EquilibriumPressure), 1.0 / 3.0);

            system.ScaleMoleculeCenters(scaleFactor);
            BoundingBox newBox = new BoundingBox(system.Properties.SimBoundary.SimulationBox.Minimum * scaleFactor * 0.5f, system.Properties.SimBoundary.SimulationBox.Maximum * scaleFactor * 0.5f);
            system.Properties.SimBoundary.SetSimulationBox(newBox);
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>Barostat.</returns>
        public override Barostat Clone()
        {
            BerendsenBarostat batman = new BerendsenBarostat();
            batman.EquilibriumPressure = EquilibriumPressure;
            batman.EquilibriumPressureIVar = EquilibriumPressureIVar;
            batman.IVarsInitialised = IVarsInitialised;
            batman.MaximumPressure = MaximumPressure;
            batman.MinimumPressure = MinimumPressure;
            batman.BerendsenCoupling = BerendsenCoupling;
            batman.RescaleFreq = RescaleFreq;
            return batman;
        }

        /// <summary>
        /// Instantiates the barostat.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="integrator">The integrator.</param>
        public override void InstantiateBarostat(AtomicSystem system, IIntegrator integrator)
        {
            XmlNode settings = system.Properties.BarostatNode;
            base.Load(settings);
            timeStep = integrator.GetTimeStep();
            BerendsenCoupling = Helper.GetAttributeValue(settings, "berendsenCoupling", 0.5f);
            RescaleFreq = Helper.GetAttributeValue(settings, "rescaleFrequency", 25);
        }
    }
}
