﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Concurrent;

namespace Nano.Client
{
    public class ActionQueue
    {
        private readonly ConcurrentQueue<Action> actionQueue = new ConcurrentQueue<Action>();

        public void BeginInvoke(Action action)
        {
            actionQueue.Enqueue(action);
        }

        public void Execute()
        {
            while (actionQueue.TryDequeue(out Action action) == true)
            {
                action.Invoke();
            }
        }
    }
}