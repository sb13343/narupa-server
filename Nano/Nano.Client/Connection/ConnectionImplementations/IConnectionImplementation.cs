﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using System;

namespace Nano.Client.ConnectionImplementations
{
    public interface IConnectionImplementation : IDisposable
    {
        event EventHandler Connected;

        event EventHandler Disconnected;

        SimboxConnectionInfo Info { get; }

        ConnectionState State { get; }

        ITransportPacketTransmitter Transmitter { get; }

        void AttachReceiver(ITransportPacketReceiver receiver);

        void BeginStart(ClientBase client);
    }
}