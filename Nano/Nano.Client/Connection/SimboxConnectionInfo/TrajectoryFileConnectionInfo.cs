﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using System.Net;

namespace Nano.Client
{
    public sealed class TrajectoryFileConnectionInfo : SimboxConnectionInfo
    {
        public override IPAddress Address => IPAddress.Loopback;

        public override ConnectionString ConnectionString { get; }
        public override SimboxConnectionType ConnectionType => SimboxConnectionType.File;

        public string FilePath { get; }

        public override int Port => 8000;

        public TrajectoryFileConnectionInfo(string descriptor, string filePath) : base(descriptor)
        {
            ConnectionString = null; 
            FilePath = filePath;
        }

        public override bool Equals(SimboxConnectionInfo other)
        {
            if ((other is TrajectoryFileConnectionInfo) == false)
            {
                return false;
            }

            TrajectoryFileConnectionInfo otherTrajectoryFileConnectionInfo = (TrajectoryFileConnectionInfo) other;

            return
                ConnectionType == otherTrajectoryFileConnectionInfo.ConnectionType &&
                FilePath.Equals(otherTrajectoryFileConnectionInfo.FilePath) &&
                Descriptor.Equals(otherTrajectoryFileConnectionInfo.Descriptor) &&
                Address.Equals(other.Address) &&
                Port.Equals(other.Port);
        }
    }
}