﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using System.Net;

namespace Nano.Client
{
    public sealed class NetworkConnectionInfo : SimboxConnectionInfo
    {
        public override IPAddress Address { get; }

        public override ConnectionString ConnectionString { get; }

        public override SimboxConnectionType ConnectionType => SimboxConnectionType.Network;

        public IPAddress NetworkAdapterIPAddress { get; }

        public override int Port { get; }

        public NetworkConnectionInfo(string descriptor, IPAddress networkAdapterIPAddress, IPAddress address, int port) : base(descriptor)
        {
            ConnectionString = new ConnectionString(address, port);

            NetworkAdapterIPAddress = networkAdapterIPAddress;
            Address = address;
            Port = port;
        }

        public override bool Equals(SimboxConnectionInfo other)
        {
            if ((other is NetworkConnectionInfo) == false)
            {
                return false;
            }

            NetworkConnectionInfo otherNetworkConnectionInfo = (NetworkConnectionInfo) other;

            return
                ConnectionType == otherNetworkConnectionInfo.ConnectionType &&
                Descriptor.Equals(otherNetworkConnectionInfo.Descriptor) &&
                NetworkAdapterIPAddress.Equals(otherNetworkConnectionInfo.NetworkAdapterIPAddress) &&
                Address.Equals(otherNetworkConnectionInfo.Address) &&
                Port.Equals(otherNetworkConnectionInfo.Port);
        }
    }
}