﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using NUnit.Framework;
using Rug.Osc;

namespace Nano.Tests.Streams
{
    [TestFixture()]
    public class StreamFormatDescriptorTests
    {
        [Test]
        public void ValuesTest()
        {
            ushort id = 4;
            VariableType type = VariableType.Half;
            int channelCount = 4;
            int maxCount = 10000;

            TransportStreamFormatDescriptor desc = new TransportStreamFormatDescriptor(id, type, channelCount, maxCount);

            Assert.AreEqual(id, desc.ID);
            Assert.AreEqual(type, desc.Type);
            Assert.AreEqual(channelCount, desc.ChannelCount);
            Assert.AreEqual(maxCount, desc.MaxCount);
        }

        [Test]
        public void OscMessageTest()
        {
            OscMessageTest(CommandState.Request, string.Empty);
            OscMessageTest(CommandState.Success, string.Empty);
            OscMessageTest(CommandState.Failed, "THIS IS AN ERROR");
        }

        private void OscMessageTest(CommandState state, string error)
        {
            ushort id = 4;
            VariableType type = VariableType.Half;
            int channelCount = 4;
            int maxCount = 10000;

            TransportStreamFormatDescriptor desc = new TransportStreamFormatDescriptor(id, type, channelCount, maxCount);

            OscMessage message = desc.ToMessage(state, error);

            TransportStreamFormatDescriptor result = TransportStreamFormatDescriptor.FromMessage(message, out CommandState resultState, out string resultError);

            Assert.AreEqual(id, result.ID);
            Assert.AreEqual(type, result.Type);
            Assert.AreEqual(channelCount, result.ChannelCount);
            Assert.AreEqual(maxCount, result.MaxCount);

            Assert.AreEqual(state, resultState);
            Assert.AreEqual(error, resultError);
        }
    }
}