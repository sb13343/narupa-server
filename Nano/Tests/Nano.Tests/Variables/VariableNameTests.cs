﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Transport.Variables;
using NUnit.Framework;

namespace Nano.Tests.Variables
{
    [TestFixture()]
    public class VariableNameTests
    {
        [Test]
        public void CreateNameTest()
        {
            string testKey = "/CreateNameTest_Key";
            string testName = "CreateNameTest_Name";            

            VariableName name = VariableName.CreateName(testKey, testName); 

            Assert.AreEqual(testKey, name.Address);
            Assert.AreEqual(testName, name.Name);
            Assert.AreEqual(VariableName.GetKeyHashCode(testKey), name.GetHashCode());            
        }

        [Test]
        public void CreateNameTest2()
        {
            string testKey = "/CreateNameTest2_Key";
            string testName = "CreateNameTest2_Name";

            VariableName name = null; 

            try
            {
                name = VariableName.CreateName(testKey, testName);

                Assert.AreEqual(testKey, name.Address);
                Assert.AreEqual(testName, name.Name);
                Assert.AreEqual(VariableName.GetKeyHashCode(testKey), name.GetHashCode());
            }
            catch (Exception ex)
            {
                // if an exception is thrown here it is a problem
                Assert.Fail(ex.ToString()); 
            }

            try
            {
                // create a duplicate name
                VariableName duplicate = VariableName.CreateName(testKey, testName);

                // if an exception is not thrown by here it is a problem
                Assert.Fail();
            }
            catch
            {

            }

            try
            {
                Assert.AreEqual(name, VariableName.GetName(testKey));
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }

        [Test]
        public void CreateNameTest_Null()
        {
            string testKey = "/CreateNameTest_Null_Key";
            string testName = "CreateNameTest_Null_Name";

            try
            { 
                VariableName name = VariableName.CreateName(null, testName);

                Assert.Fail();
            }
            catch
            {
                
            }

            try
            {
                VariableName name = VariableName.CreateName(testKey, null);

                Assert.Fail();
            }
            catch
            {

            }
        }


        [Test]
        public void CreateNameTest_Empty()
        {
            string testKey = "/CreateNameTest_Empty_Key";
            string testName = "CreateNameTest_Empty_Name";

            try
            {
                VariableName name = VariableName.CreateName(string.Empty, testName);

                Assert.Fail();
            }
            catch
            {

            }

            try
            {
                VariableName name = VariableName.CreateName(testKey, string.Empty);

                Assert.Fail();
            }
            catch
            {

            }
        }

        [Test]
        public void GetNameTest()
        {
            Assert.AreEqual(VariableName.GetName(VariableName.Density.Address), VariableName.Density);
            Assert.AreEqual(VariableName.GetName(VariableName.NumberOfParticles.Address), VariableName.NumberOfParticles);
            Assert.AreEqual(VariableName.GetName(VariableName.Pressure.Address), VariableName.Pressure);
            Assert.AreEqual(VariableName.GetName(VariableName.Temperature.Address), VariableName.Temperature);
            //Assert.AreEqual(VariableName.GetName(VariableName..Address), VariableName.Volume);
        }

        [Test]
        public void GetNamesTest()
        {
            Assert.IsNotNull(VariableName.GetNames());
            Assert.IsTrue(VariableName.GetNames().Length > 0);
        }

        [Test]
        public void GetKeyHashCodeTest()
        {
            Assert.AreEqual("/TEST".GetHashCode(), VariableName.GetKeyHashCode("/TEST"));
        }

        [Test]
        public void ToStringTest()
        {
            Assert.AreEqual("Density", VariableName.Density.ToString());
            Assert.AreEqual(VariableName.Density.Name, VariableName.Density.ToString());
        }

        [Test]
        public void GetHashCodeTest()
        {
            Assert.AreEqual(VariableName.GetKeyHashCode("/density"), VariableName.Density.GetHashCode());
            Assert.AreEqual(VariableName.GetKeyHashCode(VariableName.Density.Address), VariableName.Density.GetHashCode());
        }

        [Test]
        public void EqualsTest_VariableName()
        {
            //VariableName toTest = VariableName.CreateName("/d", "Density");
            Assert.IsTrue(VariableName.Density.Equals(VariableName.Density));
        }

        [Test]
        public void EqualsTest_Object()
        {
            Assert.IsTrue(VariableName.Density.Equals((object)VariableName.Density));
            Assert.IsTrue(VariableName.Density.Equals((object)VariableName.Density.Address));
            Assert.IsTrue(VariableName.Density.Equals((object)VariableName.Density.HashCode));
        }

        [Test]
        public void EqualsTest_String()
        {
            Assert.IsTrue(VariableName.Density.Equals(VariableName.Density.Address));
            Assert.IsTrue(VariableName.Density.Equals("/density"));
        }

        [Test]
        public void EqualsTest_Int()
        {
            Assert.IsTrue(VariableName.Density.Equals(VariableName.Density.HashCode));
            Assert.IsTrue(VariableName.Density.Equals(VariableName.GetKeyHashCode("/density")));
        }
    }
}