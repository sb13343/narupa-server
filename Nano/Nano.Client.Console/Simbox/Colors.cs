﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Rug.Cmd;

namespace Nano.Client.Console.Simbox
{
    public static class Colors
    {
        public const ConsoleColorExt Heading = ConsoleColorExt.White;
        public const ConsoleColorExt Emphasized = ConsoleColorExt.Cyan;
        public const ConsoleColorExt UserInput = ConsoleColorExt.DarkCyan;
        public const ConsoleColorExt Ident = ConsoleColorExt.DarkGray;
        public const ConsoleColorExt Normal = ConsoleColorExt.White;
        public const ConsoleColorExt Success = ConsoleColorExt.Green;
        public const ConsoleColorExt Error = ConsoleColorExt.Red;

        public const ConsoleColorExt ErrorDetail = ConsoleColorExt.DarkRed;
        public const ConsoleColorExt Transmit = ConsoleColorExt.DarkGreen;
        public const ConsoleColorExt Receive = ConsoleColorExt.DarkRed;
        public const ConsoleColorExt Message = ConsoleColorExt.Gray;

        public const ConsoleColorExt Action = ConsoleColorExt.DarkMagenta;
        public const ConsoleColorExt Debug = ConsoleColorExt.Blue;
        public const ConsoleColorExt Warning = ConsoleColorExt.Yellow;
    }
}