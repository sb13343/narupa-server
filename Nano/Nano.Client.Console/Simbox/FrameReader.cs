﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Streams;
using SlimMath;
using Action = System.Action;

namespace Nano.Client.Console.Simbox
{
    /// <summary>
    /// Big blob of code that understands simbox output for both recordings and
    /// live simulations and translates into a FrameHistory for use by the frontend
    /// </summary>
    public partial class NSBFrameReader
    {
        private static PacketStatistics statistics = new PacketStatistics();

        /// <summary>
        /// Has this reader actually tried to load something since reset
        /// </summary>
        public bool Active => Client != null;

        /// <summary>
        /// Is this reader connected to the network for a live simulation?
        /// </summary>
        public bool Networked => Active && transport != null;

        /// <summary>
        /// Is this reader currently in the process of connecting.
        /// </summary>
        public bool Connecting => Networked && transport.State == ConnectionState.Connecting;

        /// <summary>
        /// Has this reader received at least one frame.
        /// </summary>
        public bool Ready { get; private set; }

        public bool Complete { get; private set; }

        /// <summary>
        /// Indicates whether this reader is currently connecting.
        /// </summary>
        public bool TransportIsConnecting => transport.State == ConnectionState.Connecting
                                             || transport.State == ConnectionState.Handshake;

        /// <summary>
        /// Indicates whether this reader is disconnected from the server.
        /// </summary>
        public bool TransportIsDisconnected => transport.State == ConnectionState.Disconnecting
                                               || transport.State == ConnectionState.Disconnected
                                               || transport.State == ConnectionState.ForcedDisconnection
                                               || transport.State == ConnectionState.NotConnected;

        #region Streams

        private SimboxSyncBuffer<long> frameStream
            = new SimboxSyncBuffer<long>((int)StreamID.SimulationFrame);

        private SimboxSyncBuffer<BondPair> atomBondPairs
            = new SimboxSyncBuffer<BondPair>((int)StreamID.Bonds);

        private SimboxSyncBuffer_UncompressHalf3ToVector3 atomPositions
            = new SimboxSyncBuffer_UncompressHalf3ToVector3((int)StreamID.AtomPositions);

        private SimboxSyncBuffer_UncompressHalf3ToVector3 atomVelocities
            = new SimboxSyncBuffer_UncompressHalf3ToVector3((int)StreamID.AtomVelocities);

        private SimboxSyncBuffer<ushort> atomTypes
            = new SimboxSyncBuffer<ushort>((int)StreamID.AtomTypes);

        private SimboxSyncBuffer<ushort> atomCollisions
            = new SimboxSyncBuffer<ushort>((int)StreamID.AtomCollision);

        private SimboxSyncBuffer<ushort> selectedAtoms
            = new SimboxSyncBuffer<ushort>((int)StreamID.SelectedAtoms);

        private SimboxSyncBuffer<int> visibleAtomMap
            = new SimboxSyncBuffer<int>((int)StreamID.VisibleAtomMap);

        public readonly SimboxSyncBuffer<Nano.Transport.Variables.VRInteraction> VRPositions
            = new SimboxSyncBuffer<Nano.Transport.Variables.VRInteraction>((int)StreamID.VRPositions);

        public List<Nano.Transport.Variables.Interaction.Interaction> Interactions =
            new List<Nano.Transport.Variables.Interaction.Interaction>();

        /// <summary>
        /// The atoms associated with the Interactions list. Note that the order of the lists needs to be maintained.
        /// TODO Refactor once better streams are available.
        /// </summary>
        public List<IList<int>> AtomsToSelect = new List<IList<int>>();

        public List<Nano.Transport.Variables.VRInteraction> VRInputs = new List<Nano.Transport.Variables.VRInteraction>();

        #endregion Streams

        public Client Client;
        private Nano.IReporter reporter = CmdReporter.Instance;

        public List<Vector3> Inputs = new List<Vector3>();

        private TrajectoryPlayer player;
        private Stream playbackStream;

        private TransportClient transport;

        public event Action OnReady;

        public event Action OnComplete;

        private bool readySent, completeSent;

        private bool connecting;

        private event Action OnConnectionSuccess;

        private event Action OnConnectionFailure;

        private event Action OnDisconnect;

        /// <summary>
        /// The history of frames received by this instance.
        /// </summary>
        public FrameHistory<NsbFrame> History = new FrameHistory<NsbFrame>();

        /// <summary>
        /// The latest frame received by this instance.
        /// </summary>
        public NsbFrame LatestFrame;

        // groups (from osc)
        private static Regex addressPattern = new Regex(@"\/sim\/(\w+)/(\d+)/(\w+)/[^\d]*(\d+)");

        public Dictionary<string, List<ushort>> Groups = new Dictionary<string, List<ushort>>();

        public void Update()
        {
            if (Networked)
            {
                if (connecting
                    && transport.State == ConnectionState.Connected)
                {
                    connecting = false;
                    OnConnectionSuccess?.Invoke();
                }
                else if (connecting && !TransportIsConnecting)
                {
                    connecting = false;
                    OnConnectionFailure?.Invoke();
                }
                else if (TransportIsDisconnected)
                {
                    OnDisconnect?.Invoke();
                    Reset();
                }

                SendInputFields();
            }

            if (Ready && !readySent)
            {
                readySent = true;
                OnReady?.Invoke();
            }

            if (Complete && !completeSent)
            {
                completeSent = true;
                OnComplete?.Invoke();
            }
        }

        private void SendInputFields()
        {
            if (Client == null)
            {
                return;
            }

            Client.InputFields.Count = Inputs.Count;

            for (int i = 0; i < Inputs.Count; ++i)
            {
                Client.InputFields.Data[i] = Inputs[i];
            }

            Client.OutgoingStreams.Flush(Client.InputFields.ID);

            SendInteractions();
            SendVrDevicePositions();
        }

        public void PlayFromBytes(byte[] bytes)
        {
            PlayFromStream(new MemoryStream(bytes));
        }

        public void PlayFromFile(string path)
        {
            Reset();

            PlayFromStream(new FileStream(path, FileMode.Open));
        }

        public void PlayFromStream(Stream stream)
        {
            Reset();

            playbackStream = stream;

            Client = new Client(new NullPacketTransmitter(), statistics, reporter, false);

            AttachClient(Client);

            player = new TrajectoryPlayer(new System.Net.IPEndPoint(System.Net.IPAddress.Loopback, 8000),
                Client,
                statistics,
                playbackStream);

            player.EndOfStreamReached += player => Complete = true;

            player.ThreadException += (p, ex) =>
            {
                reporter?.PrintException(ex, "{0}");
            };

            Client.Start();

            player.ContinueReading();
        }

        // TODO: need to clean up all of this when we quit!

        public void TryEnterServer(SimboxConnectionInfo info,
            Action OnSuccess = null,
            Action OnFailure = null,
            Action OnConnectionLost = null,
            ConnectionType connectionType = ConnectionType.Tcp,
            string sessionToken = null)
        {
            Reset();

            connecting = true;
            OnConnectionSuccess = OnSuccess ?? delegate { };
            OnConnectionFailure = OnFailure ?? delegate { };
            OnDisconnect = OnConnectionLost ?? delegate { };

            transport = new TransportClient(info.ConnectionString,
                sessionToken,
                statistics,
                reporter);
            Client = new Client(transport, statistics, reporter, false);

            AttachClient(Client);

            transport.Connected += (s, a) => Client.Start();
            transport.AttachReceiver(Client);
            transport.Start();
        }

        public void Reset()
        {
            Ready = false;
            Complete = false;

            readySent = false;
            completeSent = false;

            bufferCopies.Clear();
            Groups = new Dictionary<string, List<ushort>>();

            if (player != null)
            {
                player.Dispose();
                player = null;
            }

            if (playbackStream != null)
            {
                playbackStream.Close();
                playbackStream.Dispose();
                playbackStream = null;
            }

            if (Client != null)
            {
                Client.Dispose();
                DetachClient(Client);
                Client = null;
            }

            if (transport != null)
            {
                transport.Dispose();
                transport = null;
            }

            lock (History)
            {
                History.Reset();
            }
        }

        private Dictionary<object, object> bufferCopies
            = new Dictionary<object, object>();

        /// <summary>
        /// Return the current data from a buffer - if that data hasn't changed
        /// since the last copy we got, return a reference to that instead of
        /// making a new one
        /// </summary>
        private T[] GetDataCopy<T>(ISimboxSyncBuffer<T> buffer)
        {
            T[] copy;

            // no data to copy yet
            if (buffer.Data == null)
            {
                copy = Enumerable.Empty<T>().ToArray();

                bufferCopies[buffer] = copy;
            }
            // data changed since previous copy or there is no previous copy
            else if (buffer.IsDirty || !bufferCopies.ContainsKey(buffer))
            {
                buffer.IsDirty = false;

                copy = buffer.Data.Take(buffer.Count).ToArray();

                bufferCopies[buffer] = copy;
            }
            // use previous copy
            else
            {
                copy = (T[])bufferCopies[buffer];
            }

            return copy;
        }

        /// <summary>
        /// Add an atom of given id to a named group.
        /// </summary>
        private void AddAtomToGroup(string name, ushort atom)
        {
            List<ushort> atoms;

            if (!Groups.TryGetValue(name, out atoms))
            {
                atoms = new List<ushort>();
                Groups[name] = atoms;
            }

            atoms.Add(atom);
        }

        private void AttachClient(Client client)
        {
            frameStream.Attach(client);
            atomPositions.Attach(client);
            atomTypes.Attach(client);
            atomVelocities.Attach(client);
            atomBondPairs.Attach(client);
            selectedAtoms.Attach(client);
            visibleAtomMap.Attach(client);
            VRPositions.Attach(client);
            frameStream.Updated += OnFrameStreamUpdated;
        }

        private void DetachClient(Client client)
        {
            frameStream.Detach(client);
            atomPositions.Detach(client);
            atomTypes.Detach(client);
            atomVelocities.Detach(client);
            atomBondPairs.Detach(client);
            visibleAtomMap.Detach(client);
            selectedAtoms.Detach(client);
            VRPositions.Detach(client);
            frameStream.Updated -= OnFrameStreamUpdated;
        }

        /// <summary>
        /// When the frame stream is updated, that means the previous frame must
        /// be complete, so we should copy all the existing data into a new frame
        /// in the history
        /// </summary>
        private void OnFrameStreamUpdated(ISimboxSyncBuffer<long> buffer)
        {
            if (player != null)
            {
                player.PauseReading();
            }

            LatestFrame = RecordFrame();

            if (player != null)
            {
                player.ContinueReading();
            }
        }

        /// <summary>
        /// Copy all stream data into a new frame (data that hasn't changed will
        /// reuse the arrays from the last frame)
        /// </summary>
        public NsbFrame RecordFrame()
        {
            var frame = new NsbFrame
            {
                VisibleAtomMap = GetDataCopy(visibleAtomMap),

                FrameNumber = frameStream.Data[0],
                AtomCount = atomTypes.Count,
                BondCount = atomBondPairs.Count,

                AtomTypes = GetDataCopy(atomTypes),
                AtomPositions = GetDataCopy(atomPositions),
                AtomVelocities = GetDataCopy(atomVelocities),
                BondPairs = GetDataCopy(atomBondPairs),

                CollidedAtomIndicies = GetDataCopy(atomCollisions),
                SelectedAtomIndicies = GetDataCopy(selectedAtoms),
            };

            lock (History)
            {
                History.AddNewestFrame(frame);
            }

            Ready = true;
            if (readySent == false)
            {
                readySent = true; 
                OnReady?.Invoke();
            }
            return frame;
        }

        private void SendInteractions()
        {
            Client.Interactions.Count = Interactions.Count;
            int atomsSelected = 0;
            for (int i = 0; i < Interactions.Count; ++i)
            {
                Client.Interactions.Data[i] = Interactions[i];
                if (Client.InteractionAtoms == null)
                    continue;
                //Add all the selected atoms to the stream.
                foreach (int atomIndex in AtomsToSelect[i])
                {
                    Client.InteractionAtoms.Data[atomsSelected] = atomIndex;
                    atomsSelected++;
                }
                //Add a -1 to the selected atoms list to denote the end of the list.
                Client.InteractionAtoms.Data[atomsSelected] = -1;
                atomsSelected++;
            }
            if (Client.InteractionAtoms != null)
            {
                Client.InteractionAtoms.Count = atomsSelected;
                Client.OutgoingStreams.Flush(Client.InteractionAtoms.ID);
            }

            Client.OutgoingStreams.Flush(Client.Interactions.ID);
        }

        private void SendVrDevicePositions()
        {
            Client.LocalVrDevicePositions.Count = VRInputs.Count;
            for (int i = 0; i < VRInputs.Count; ++i)
            {
                Client.LocalVrDevicePositions.Data[i] = VRInputs[i];
            }

            Client.OutgoingStreams.Flush(Client.LocalVrDevicePositions.ID);
        }
    }
}