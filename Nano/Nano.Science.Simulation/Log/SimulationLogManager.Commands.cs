﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Reflection;
using Nano.Transport.Comms;
using Nano.Transport.OSCCommands;
using Rug.Osc;

namespace Nano.Science.Simulation.Log
{
    /// <summary>
    /// Manages simulation logs. 
    /// </summary>
    public partial class SimulationLogManager : ICommandProvider
    {
        private TransportContext transportContext;

        /// <inheritdoc />
        public string Address
        {
            get
            {
                return "/log";
            }
        }

        /// <summary>
        /// Starts any interactive logs controlled by this manager.
        /// </summary>
        /// <param name="message"></param>
        [OscCommand(Help = "Starts any interactive logs")]
        public void StartLogs(OscMessage message)
        {
            if(message.Count > 0)
                runInteractiveLogs = (bool) message[0];
            transportContext.Transmitter.Broadcast(TransportPacketPriority.Normal, message);
        }
        
        /// <inheritdoc />
        public void AttachTransportContext(TransportContext transportContext)
        {
            this.transportContext = transportContext;
        }

        /// <inheritdoc />
        public void DetachTransportContext(TransportContext transportContext)
        {
            this.transportContext = null;
        }
    }
}