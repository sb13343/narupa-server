﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Xml;
using Nano.Loading;
using SlimMath;

namespace Nano.Science.Simulation.Log
{
    /// <summary>
    /// Simple XYZ Writer class for outputting xyz files.
    /// </summary>
    /// <remarks>
    /// Currently outputs space delimited fields rather than fixed width.
    /// Curently outputs elements rather than names (TODO).
    /// </remarks>
    public class XyzWriter
    {
        private static readonly object locker = new object();
        private StreamWriter writer;

        /// <summary>
        /// Constructs an XYZWriter to write to the given filepath. 
        /// </summary>
        /// <param name="filePath">The filepath to write to.</param>
        /// <param name="append">Whether to append to the file.</param>
        public XyzWriter(string filePath, bool append)
        {
            lock (locker)
            {
                writer = new StreamWriter(filePath, append);
            }
        }

        /// <summary>
        /// Writes an XYZ frame.
        /// </summary>
        /// <param name="comment">XYZ Comment string. </param>
        /// <param name="logOptions">Options to indicate what to log.</param>
        /// <param name="elements">Set of elements.</param>
        /// <param name="positions">The atomic positions.</param>
        /// <param name="velocities">The atomic velocities.</param>
        /// <param name="forces">The atomic forces.</param>
        /// <exception cref="Exception"></exception>
        public void WriteFrame(string comment, LogOptions logOptions, Element[] elements, Vector3[] positions = null, Vector3[] velocities = null, Vector3[] forces = null)
        {
            if (logOptions == LogOptions.None)
                return;

            lock (locker)
            {
                if (writer == null)
                    return;

                writer.WriteLine(elements.Length);
                writer.WriteLine(comment);
                bool savePositions = (logOptions & LogOptions.Positions) == LogOptions.Positions;
                bool saveVelocities = (logOptions & LogOptions.Velocities) == LogOptions.Velocities;
                bool saveForces = (logOptions & LogOptions.Forces) == LogOptions.Forces;
                if (positions != null && (savePositions && elements.Length != positions.Length))
                    throw new Exception("Particle and element Length mismatch.");
                if (velocities != null && (saveVelocities && elements.Length != velocities.Length))
                    throw new Exception("Velocity and element Length mismatch.");
                if (forces != null && (saveForces && elements.Length != forces.Length))
                    throw new Exception("Force and element Length mismatch.");

                for (int i = 0; i < elements.Length; i++)
                {
                    string line = "";
                    line += $"{PeriodicTable.GetElementProperties(elements[i]).Symbol}";
                    if (savePositions)
                        if (positions != null)
                            line += $"\t{positions[i].X}\t{positions[i].Y}\t{positions[i].Z}";
                    if (saveVelocities)
                        if (velocities != null)
                            line += $"\t{velocities[i].X}\t{velocities[i].Y}\t{velocities[i].Z}";
                    if (saveForces)
                        if (forces != null)
                            line += $"\t{forces[i].X}\t{forces[i].Y}\t{forces[i].Z}";
                    writer.WriteLine(line);
                }
                writer.Flush();
            }
        }

        /// <summary>
        /// Disposes of this instance.
        /// </summary>
        public void Dispose()
        {
            lock (locker)
            {
                if (writer != null)
                {
                    writer.Flush();
                    writer.Close();
                    writer = null;
                }
            }
        }
    }

    /// <summary>
    /// XYZ logger.
    /// </summary>
    [XmlName("XYZ")]
    public class XYZLogger : SimulationLoggerBase
    {
        private bool logCreated;

        private XyzWriter xyzWriter;

        private Element[] elements;
        private Vector3[] positions;
        private Vector3[] velocities;
        private Vector3[] forces;

        private float positionUnitConverter = 1;
        private float velocitiesUnitConverter = 1;
        private float forcesUnitConverter = 1;
        private float energyUnitsConverter = 1;

        private bool useSimboxUnits;
        private string energyUnitsComment = "kJ/mol";
        private string positionUnitsComment = "nm";
        private string velocityUnitsComment = "nm/ps";
        private string forceUnitsComment = "kJ/(mol*nm)";
        private bool storePotentialEnergy = true;

        /// <inheritdoc />
        public override string Identifier
        {
            get
            {
                return Helper.ResolvePath(LogPath + "/xyz");
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        /// <inheritdoc />
        public XYZLogger() : base()
        {
        }

        /// <inheritdoc />
        public XYZLogger(string logPath, LogOptions logOptions, int writeFrequency, bool useDateTimeStamp, bool useSimboxUnits = false)
        {
            LogPath = Helper.ResolvePath(logPath);
            LogOptions = logOptions;
            WriteFrequency = writeFrequency;
            UseDateTimeStamp = useDateTimeStamp;
            this.useSimboxUnits = useSimboxUnits;
            SetupUnits();
        }

        private void SetupUnits()
        {
            if (useSimboxUnits == false)
            {
                positionUnitConverter = Units.AngstromsPerNm;
                positionUnitsComment = "A";
                velocitiesUnitConverter = Units.AngstromsPerNm / Units.FsPerPs;
                velocityUnitsComment = "A/fs";
                energyUnitsComment = "kcal/mol";
                energyUnitsConverter = Units.KCalPerKJ;
                forcesUnitConverter = Units.KCalPerKJ / Units.AngstromsPerNm;
                forceUnitsComment = "kcal/(mol*A)";
            }
        }

        /// <inheritdoc />
        public override void Load(LoadContext context, XmlNode node)
        {
            base.Load(context, node);
            if (FlagsHelper.IsSet<LogOptions>(LogOptions, LogOptions.Interactions))
            {
                context.Error("Requested interaction logging, the XYZ logger does not currently support this.");
            }
            storePotentialEnergy = Helper.GetAttributeValue(node, "PotentialEnergy", true);
            useSimboxUnits = Helper.GetAttributeValue(node, "SimboxUnits", false);
            SetupUnits();
        }

        /// <inheritdoc />
        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            XmlElement e = base.Save(context, element);
            Helper.AppendAttributeAndValue(e, "SimboxUnits", useSimboxUnits);
            Helper.AppendAttributeAndValue(e, "PotentialEnergy", storePotentialEnergy);
            return e;
        }

        /// <inheritdoc />
        public override void CloseLog()
        {
            xyzWriter.Dispose();
        }

        /// <inheritdoc />
        public override void Dispose()
        {
            CloseLog();
            logCreated = false;
        }

        /// <inheritdoc />
        public override void LogState(IAtomicSystem system)
        {
            if (logCreated == false)
            {
                throw new Exception(string.Format("Attempted to log state before initialising log {0}!", Identifier));
            }

            if (elements == null)
            {
                GenerateParticleLists(system);
            }

            for (int i = 0; i < system.NumberOfParticles; i++)
            {
                elements[i] = system.Topology.Elements[i];
                if ((LogOptions.Positions & LogOptions) == LogOptions.Positions) positions[i] = system.Particles.Position[i] * positionUnitConverter;
                if ((LogOptions.Velocities & LogOptions) == LogOptions.Velocities) velocities[i] = system.Particles.Velocity[i] * velocitiesUnitConverter;
                if ((LogOptions.Forces & LogOptions) == LogOptions.Forces) forces[i] = system.Particles.Force[i] * forcesUnitConverter;
            }
            string comment = "";
            comment += $"Simulation Step: {Step} Simulation Time (ps): {SimulationTime} ";
            if (storePotentialEnergy) comment += $"PotentialEnergy: {system.PotentialEnergy * energyUnitsConverter} {energyUnitsComment}";
            if ((LogOptions.Positions & LogOptions) == LogOptions.Positions) comment += string.Format(" {0}: {1}", "Positions", positionUnitsComment);
            if ((LogOptions.Velocities & LogOptions) == LogOptions.Velocities) comment += string.Format(" {0}: {1}", "Velocities", velocityUnitsComment);
            if ((LogOptions.Forces & LogOptions) == LogOptions.Forces) comment += string.Format(" {0}: {1}", "Forces", forceUnitsComment);

            xyzWriter.WriteFrame(comment, LogOptions, elements, positions, velocities, forces);
        }

        private void GenerateParticleLists(IAtomicSystem system)
        {
            elements = new Element[system.NumberOfParticles];
            if (FlagsHelper.IsSet(LogOptions, LogOptions.Positions))
            {
                positions = new Vector3[system.NumberOfParticles];
            }
            if (FlagsHelper.IsSet(LogOptions, LogOptions.Velocities))
            {
                velocities = new Vector3[system.NumberOfParticles];
            }
            if (FlagsHelper.IsSet(LogOptions, LogOptions.Forces))
            {
                forces = new Vector3[system.NumberOfParticles];
            }
        }

        /// <inheritdoc />
        public override void CreateLog(bool append = false, params string[] logIdentifiers)
        {
            base.CreateLog(append, logIdentifiers);

            if (logCreated == true)
            {
                throw new Exception("Log already created!");
            }

            string xyzPath = CreateLogFilePath(LogPath, "trajectory", logIdentifiers, UseDateTimeStamp, "xyz");

            xyzWriter = new XyzWriter(xyzPath, append);

            logCreated = true;
        }

        /// <summary>
        /// Clones this logging instance.
        /// </summary>
        /// <returns></returns>
        public override ISimulationLogger Clone()
        {
            XYZLogger logger = new XYZLogger
            {
                LogOptions = LogOptions,
                LogPath = LogPath,
                WriteFrequency = WriteFrequency,
                UseDateTimeStamp = UseDateTimeStamp,
                useSimboxUnits = useSimboxUnits,
                storePotentialEnergy = storePotentialEnergy,
                IsInteractive = IsInteractive,
            };

            logger.SetupUnits();
            xyzWriter?.Dispose();
            return logger;
        }
    }
}