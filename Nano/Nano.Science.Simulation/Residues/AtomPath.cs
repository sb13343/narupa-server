﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Rug.Osc;

namespace Nano.Science.Simulation.Residues
{
    /// <summary>
    /// Indicator for type of atom path part.
    /// </summary>
    public enum AtomPathPartType
    {
        /// <summary>
        /// String based atom path part.
        /// </summary>
        String,
        
        /// <summary>
        /// Index based atom path part.
        /// </summary>
        Index,
    }

    /// <summary>
    /// Represents part of an atom path.
    /// </summary>
    public struct AtomPathPart : IEquatable<AtomPathPart>, IEquatable<string>
    {
        /// <summary>
        /// Type of this atom path part.
        /// </summary>
        public readonly AtomPathPartType Type;

        /// <summary>
        /// String associated with atom path part.
        /// </summary>
        public readonly string String;

        /// <summary>
        /// Index associated with atom path part.
        /// </summary>
        public readonly int Index;

        /// <summary>
        /// Constructs an atom path part from the given string.
        /// </summary>
        /// <param name="partString"></param>
        public AtomPathPart(string partString)
        {
            String = partString;

            Type = int.TryParse(partString, out Index) == true ? AtomPathPartType.Index : AtomPathPartType.String;
        }

        /// <inheritdoc />
        public bool Equals(AtomPathPart other)
        {
            return String.Equals(other.String);
        }

        /// <inheritdoc />
        public bool Equals(string other)
        {
            return String.Equals(other);
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return String;
        }
    }

    /// <summary>
    /// Represents a path to an atom in the topology as an OSC Address.
    /// </summary>
    /// <remarks>
    /// Molecular systems often have a tree like topology of macromolecules, molecules, residues and atoms. The <see cref="AtomPath"/> 
    /// represents this structure by storing relative atom paths in the form of a <see cref="OscAddress"/>. 
    /// </remarks>
    /// <example>
    /// A alpha carbon atom (the 10th atom in the system) in the first residue alanine in the protein 2efv may have a path like:
    /// /2efv/ALA-1-1/CA-10-10
    /// 
    /// The additional numbers are used to provide uniquely identifying information about the atom.
    /// </example>
    public class AtomPath : IEquatable<AtomPath>, IEquatable<string>, IEnumerable<AtomPathPart>
    {
        private readonly string original;

        private readonly AtomPathPart[] pathParts;

        /// <summary>
        /// Last part of the atom parts.
        /// </summary>
        public readonly AtomPathPart AtomPart;

        /// <summary>
        /// Returns the specified part of the atom path.
        /// </summary>
        /// <param name="index"></param>
        public AtomPathPart this[int index] { get { return pathParts[index]; } }

        /// <summary>
        /// Number of parts to this atom path.
        /// </summary>
        public int Count { get { return pathParts.Length; } } 

        /// <summary>
        /// Constructs an atom path from a given string.
        /// </summary>
        /// <param name="path"></param>
        /// <exception cref="FlexibleFileFormatException">Atom path is not an OSC Address.</exception>
        public AtomPath(string path)
        {
            original = path;

            if (path.StartsWith("/") == false)
            {
                path = "/" + path; 
            }

            if (OscAddress.IsValidAddressLiteral(path) == false)
            {
                throw new FlexibleFileFormatException("Atom path is not a literal.", path);
            }

            OscAddress address = new OscAddress(path);

            List<AtomPathPart> parts = new List<AtomPathPart>();

            for (int i = 0; i < address.Count; i++)
            {
                switch (address[i].Type)
                {
                    case OscAddressPartType.Literal:
                        parts.Add(new AtomPathPart(address[i].Value));
                        break;
                    default:
                        break;
                }
            }

            pathParts = parts.ToArray();

            AtomPart = pathParts[pathParts.Length - 1];
        }


        /// <inheritdoc />
        public bool Equals(string other)
        {
            return original.Equals(other);
        }

        /// <inheritdoc />
        public bool Equals(AtomPath other)
        {
            return original.Equals(other.original);
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return original;
        }

        /// <inheritdoc />
        public IEnumerator<AtomPathPart> GetEnumerator()
        {
            return (pathParts as IEnumerable<AtomPathPart>).GetEnumerator(); 
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return pathParts.GetEnumerator(); 
        }

        /// <summary>
        /// Combines an address with an index.
        /// </summary>
        /// <param name="address1">The address</param>
        /// <param name="index">The index</param>
        /// <returns>A new address of the form address1/index </returns>
        public static string Combine(string address1, int index)
        {
            return string.Format("{0}/{1}", address1, index); 
        }

        /// <summary>
        /// Combines an address with a sequence of address parts through concatenation.
        /// </summary>
        /// <param name="address1">The address.</param>
        /// <param name="addressParts">The address parts.</param>
        /// <returns>A new address of the form address1/addressPart0/addressPart1/.../addressPartN. </returns>
        public static string Combine(string address1, params string[] addressParts)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(address1);

            bool endsWithSlash = address1.EndsWith("/");

            foreach (string part in addressParts)
            {
                string cleanedPart = part;

                bool partStartsWithSlash = cleanedPart.StartsWith("/");
                bool partEndsWithSlash = cleanedPart.EndsWith("/");

                if (endsWithSlash == true && partStartsWithSlash == true)
                {
                    sb.Append(cleanedPart.Substring(1)); 
                }
                else if (endsWithSlash == false && partStartsWithSlash == false)
                {
                    sb.Append("/"); 
                    sb.Append(cleanedPart);
                }
                else 
                {
                    sb.Append(cleanedPart);
                }

                endsWithSlash = partEndsWithSlash; 
            }

            return sb.ToString(); 
        }
    }
}
