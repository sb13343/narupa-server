﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;

namespace Nano.Science.Simulation.Residues
{
    /// <inheritdoc />
    public interface IBond : ILoadable
    {
        /// <summary>
        /// Indicates whether this bond has linkage.
        /// </summary>
        bool HasLinkage { get; } 

        /// <summary>
        /// Path to the first atom in the bond.
        /// </summary>
        AtomPath PathToA { get; set; }

        /// <summary>
        /// Path to the second atom in the bond.
        /// </summary>
        AtomPath PathToB { get; set; }

        /// <summary>
        /// Linkage of atom A.
        /// </summary>
        AtomLinkage A { get; set; }

        /// <summary>
        /// Linkage of atom B.
        /// </summary>
        AtomLinkage B { get; set; }

        /// <summary>
        /// Given the specified atom paths, tries to resolve linkage between the atoms, ensuring that the paths are
        /// consistent.
        /// </summary>
        /// <param name="owner">Residue associated with atom.</param>
        /// <param name="throwExceptions">Whether to throw exceptions.</param>
        /// <returns></returns>
        bool TryResolveLinkage(IResidue owner, bool throwExceptions = false);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="throwExceptions"></param>
        /// <returns></returns>
        bool TryResolvePaths(IResidue owner, bool throwExceptions = false);
    }
}
