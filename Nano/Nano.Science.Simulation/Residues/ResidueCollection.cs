﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections;
using System.Collections.Generic;

namespace Nano.Science.Simulation.Residues
{
    /// <summary>
    /// Represents a collection of groups of atoms.
    /// </summary>
    public class ResidueCollection : ResidueCollection<IResidue>
    {
    }

    /// <summary>
    /// Represents a collection of a groups of atoms of a particular type.
    /// </summary>
    /// <typeparam name="TResidueType">Atom group type.</typeparam>
    public class ResidueCollection<TResidueType> : ICollection<TResidueType> where TResidueType : IResidue
    {
        private readonly Dictionary<string, TResidueType> namedResidueLookup = new Dictionary<string, TResidueType>();
        private readonly List<TResidueType> residues = new List<TResidueType>();

        /// <inheritdoc />
        public int Count => residues.Count;

        /// <inheritdoc />
        public bool IsReadOnly => false;

        /// <summary>
        /// Returns the atom group by index.
        /// </summary>
        /// <param name="index"></param>
        public TResidueType this[int index] => residues[index];

        /// <summary>
        /// Returns the atom group by name.
        /// </summary>
        /// <param name="name"></param>
        public TResidueType this[string name] => namedResidueLookup[name];

        /// <summary>
        /// Attempts to retrieve the given residue by name.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="residue"></param>
        /// <returns></returns>
        public bool TryGet(string name, out TResidueType residue)
        {
            return namedResidueLookup.TryGetValue(name, out residue);
        }

        /// <inheritdoc />
        public void Add(TResidueType residue)
        {
            residues.Add(residue);

            AddName(residue);
        }

        /// <summary>
        /// Adds the set of atom groups to this collection.
        /// </summary>
        /// <param name="residues">Atom groups to add.</param>
        public void AddRange(IEnumerable<TResidueType> residues)
        {
            this.residues.AddRange(residues);

            foreach (TResidueType residue in residues)
            {
                AddName(residue);
            }
        }

        /// <inheritdoc />
        public void Clear()
        {
            residues.Clear();
            namedResidueLookup.Clear();
        }

        /// <inheritdoc />
        public bool Contains(TResidueType item)
        {
            return residues.Contains(item);
        }

        /// <summary>
        /// Indicates whether this collection contains an atom group with the specified name.
        /// </summary>
        /// <param name="name">Name of the atom group.</param>
        /// <returns><c>true</c> if atom group is in the collection, <c>false</c> otherwise.</returns>
        public bool Contains(string name)
        {
            return namedResidueLookup.ContainsKey(name);
        }

        /// <inheritdoc />
        public void CopyTo(TResidueType[] array, int arrayIndex)
        {
            residues.CopyTo(array, arrayIndex);
        }

        /// <inheritdoc />
        public IEnumerator<TResidueType> GetEnumerator()
        {
            return residues.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (residues as IEnumerable).GetEnumerator();
        }

        /// <inheritdoc />
        public bool Remove(TResidueType residue)
        {
            bool result = residues.Remove(residue);

            if (result == true)
            {
                RemoveName(residue);
            }

            return result;
        }

        /// <summary>
        /// Removes the atom group at the specified index.
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            RemoveName(residues[index]);

            residues.RemoveAt(index);
        }

        /// <summary>
        /// Converts this collection to an array.
        /// </summary>
        /// <returns></returns>
        public TResidueType[] ToArray()
        {
            return residues.ToArray();
        }

        private void AddName(TResidueType residue)
        {
            if (string.IsNullOrEmpty(residue.Name) == true)
            {
                return;
            }

            //TODO HACK @review reside names need to be unique??
            string uniqueName = residue.Name + "-" + residue.Id;

            try
            {
                namedResidueLookup.Add(uniqueName, residue);
            }
            catch (Exception e)
            {
                uniqueName = " . ";
            }
        }

        private void RemoveName(TResidueType residue)
        {
            if (string.IsNullOrEmpty(residue.Name) == true)
            {
                return;
            }

            string uniqueName = residue.Name + "-" + residue.Id;
            namedResidueLookup.Remove(uniqueName);
        }
    }
}