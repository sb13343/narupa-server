﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Xml;
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Transport.Agents;
using Nano.Transport.Comms;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents a molecular dynamics simulation.
    /// </summary>
    public abstract class SimulationBase : ISimulation
    {
        /// <inheritdoc />
        public abstract string Name { get; set; }

        /// <inheritdoc />
        public abstract IAtomicSystem ActiveSystem { get; }

        /// <inheritdoc />
        public abstract InstantiatedTopology ActiveTopology { get; set; }

        /// <inheritdoc />
        public abstract string Address { get; }

        /// <inheritdoc />
        public abstract bool HasReset { get; set; }

        /// <inheritdoc />
        public abstract bool VariablesInitialised { get; }
        public abstract bool EndOfSimulation { get; }

        /// <inheritdoc />
        public abstract float SimulationTime { get; }

        /// <inheritdoc />
        public abstract void AttachTransportContext(TransportContext transportContext);

        /// <inheritdoc />
        public abstract void DetachTransportContext(TransportContext transportContext);

        /// <inheritdoc />
        public abstract void Dispose();

        /// <inheritdoc />
        public abstract void InitialiseVariables(VariableManager variableManager);

        /// <inheritdoc />
        public abstract void Load(LoadContext context, XmlNode node);

        /// <inheritdoc />
        public abstract void Reset();

        /// <inheritdoc />
        public abstract void ResetVariableDesiredValues();

        /// <inheritdoc />
        public abstract void Pause();

        /// <inheritdoc />
        public abstract void Run(int steps, List<Interaction> interactionPoints = null);

        /// <inheritdoc />
        public abstract XmlElement Save(LoadContext context, XmlElement element);

        /// <inheritdoc />
        public abstract void UpdateVariableValues();

        protected readonly IReporter Reporter;

        /// <summary>
        /// Constructs an empty simulation from a reporter.
        /// </summary>
        /// <param name="reporter"></param>
        protected SimulationBase(IReporter reporter)
        {
            Reporter = reporter;
        }
    }
}