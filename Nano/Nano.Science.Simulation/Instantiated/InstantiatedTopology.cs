﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Net;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Spawning;
using Nano.Transport.Comms;
using Nano.Transport.OSCCommands;
using QuickGraph;
using QuickGraph.Algorithms;
using Rug.Osc;
using Rug.Osc.Packaging;
using SlimMath;

namespace Nano.Science.Simulation.Instantiated
{
    /// <summary>
    /// Represents an instantiated topology, instantiated from a <see cref="Topology"/>.
    /// </summary>
    public class InstantiatedTopology : ICommandProvider
    {
        /// <summary>
        /// Gets the number of particles.
        /// </summary>
        /// <value>The number of particles.</value>
        public int NumberOfParticles { get { return Positions.Count; } }

        /// <inheritdoc />
        public string Address
        {
            get
            {
                return "/top";
            }
        }

        /// <summary> List of all bonds in the system. </summary>
        public BondList Bonds = new BondList();

        /// <summary>
        /// The set of dynamic bonds in the system.
        /// </summary>
        public IDynamicBonds DynamicBonds;

        /// <summary>
        /// Delegate BondsChangedHandler
        /// </summary>
        /// <param name="b">The bonds.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public delegate void BondsChangedHandler(BondList b, EventArgs e);

        /// <summary>
        /// Occurs when bonds have changed.
        /// </summary>
        public event BondsChangedHandler BondsChanged;

        /// <summary> List of molecules. </summary>
        public List<Molecule> Molecules = new List<Molecule>();

        /// <summary> List of Element numbers for each atom in the system. </summary>
        public readonly List<Element> Elements = new List<Element>();

        /// <summary> Initial positions of atoms. </summary>
        public readonly List<Vector3> Positions = new List<Vector3>();

        /// <summary>
        /// The template that each atom originates from.
        /// </summary>
        public readonly List<ISpawnable> Sources = new List<ISpawnable>();

        /// <summary>
        /// The addresses of instances in this topology.
        /// </summary>
        public readonly List<string> Addresses = new List<string>();
        
        /// <summary>
        /// A map from address to atom index. 
        /// </summary>
        public readonly Dictionary<string, int> AddressToAtomIndexMap = new Dictionary<string, int>();
        
        /// <summary>
        /// The force field terms in this topology.
        /// </summary>
        public readonly List<IInstantiatedForceFieldTerms> ForceFieldTerms = new List<IInstantiatedForceFieldTerms>();

        /// <summary>
        /// Selection JSON associated with this Topology
        /// </summary>        
        public string SelectionData { get; set; }

        private Transport.Comms.TransportContext transportContext;

        private readonly HashSet<string> addressSet = new HashSet<string>();

        /// <summary>
        /// Add an atom address to the topology.
        /// </summary>
        /// <param name="address">Atom OSC address to add.</param>
        /// <remarks>
        /// For performance reasons, this should be used instead of adding to Addresses directly. 
        /// </remarks>
        public void AddAddress(string address)
        {
            Addresses.Add(address);
            addressSet.Add(address);
            AddressToAtomIndexMap[address] = Addresses.Count - 1;
        }

        public bool ContainsAddress(string address)
        {
            return addressSet.Contains(address);
        }
        
        /// <summary>
        /// The list of <see cref="ISpawner"/> names associated with this topology.
        /// </summary>
        public readonly List<string> Spawners = new List<string>();

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            Bonds.Clear();
            Molecules.Clear();
            Elements.Clear();
            Positions.Clear();
            Sources.Clear();
            Addresses.Clear();
            Spawners.Clear();
        }

        /// <summary>
        /// Selects the atoms that match the given osc address.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>List&lt;System.UInt16&gt;.</returns>
        public List<ushort> SelectAtomsByOscAddress(OscAddress address)
        {
            List<ushort> atoms = new List<ushort>();
            ushort index = 0;
            foreach (string atomAddress in Addresses)
            {
                if (address.Match(atomAddress))
                {
                    atoms.Add(index);
                }
                index++;
            }
            return atoms;
        }

        /// <summary>
        /// Selects the atoms by substring in the address.
        /// </summary>
        /// <param name="addresses">Addresses to search.</param>
        /// <param name="substring">The substring.</param>
        /// <returns>List&lt;System.UInt16&gt;.</returns>
        public List<ushort> SelectAtomsBySubstring(List<string> addresses, string substring)
        {
            List<ushort> atoms = new List<ushort>();
            ushort index = 0;
            foreach (string atomAddress in addresses)
            {
                if (atomAddress.Contains(substring))
                {
                    atoms.Add(index);
                }
                index++;
            }
            return atoms;
        }

        /// <inheritdoc />
        public void DetachTransportContext(Transport.Comms.TransportContext transportContext)
        {
        }

        /// <summary>
        /// Transmits the addresses of the atoms in the topology.
        /// </summary>
        /// <param name="endPoint">The end point.</param>
        /// <exception cref="System.NullReferenceException">Topology does not have a transport context with which to send topology.</exception>
        public void TransmitTopologyAddresses(IPEndPoint endPoint, IAtomSelectionPool selectionPool, bool sendInvisible=false)
        {
            if (transportContext == null)
            {
                throw new NullReferenceException("Topology does not have a transport context with which to send topology.");
            }

            OscPackageBuilder builder = new OscPackageBuilder(0) { Mode = OscPackageBuilderMode.Bundled };

            OscPackegeEvent handlePackage = delegate (ulong packageId, OscBundle oscBundle)
            {
                transportContext.Transmitter.Transmit(TransportPacketPriority.Critical, endPoint, oscBundle);
            };

            builder.BundleComplete += handlePackage;

            if (sendInvisible == false)
            {
                foreach (var atomId in selectionPool.VisibleAtoms)
                {
                    OscMessage topAddress = new OscMessage(Addresses[atomId], atomId);
                    builder.Add(topAddress);
                }
            }
            else
            {
                for (int i = 0; i < NumberOfParticles; i++)
                {
                    OscMessage topAddress = new OscMessage(Addresses[i], i);
                    builder.Add(topAddress);
                }
            }

            builder.Flush();

            builder.BundleComplete -= handlePackage;
        }

        /// <summary>
        /// Transmits the selection data associated with the instantiated topology. 
        /// Submits as a json string.
        /// </summary>
        /// <param name="endPoint">The end point.</param>
        /// <exception cref="System.NullReferenceException">Topology does not have a transport context with which to send topology.</exception>
        public void TransmitSelectionData(IPEndPoint endPoint)
        {
            if (SelectionData == string.Empty) return;
            if (transportContext == null)
            {
                throw new NullReferenceException("Topology does not have a transport context with which to send topology.");
            }

            OscPackageBuilder builder = new OscPackageBuilder(0) { Mode = OscPackageBuilderMode.Immediate };

            OscPackegeEvent handlePackage = delegate (ulong packageId, OscBundle oscBundle)
            {
                transportContext.Transmitter.Transmit(TransportPacketPriority.Critical, endPoint, oscBundle);
            };

            builder.BundleComplete += handlePackage;

            OscMessage selectionAddress = new OscMessage("/top/selectiondata", SelectionData);
            builder.Add(selectionAddress);

            builder.Flush();
            builder.BundleComplete -= handlePackage;
        }

        /// <inheritdoc />
        public void AttachTransportContext(Transport.Comms.TransportContext transportContext)
        {
            this.transportContext = transportContext;
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>FlattenedTopology.</returns>
        public InstantiatedTopology Clone()
        {
            InstantiatedTopology clone = new InstantiatedTopology();

            clone.Bonds.AddRange(Bonds);

            foreach (Molecule m in Molecules)
            {
                clone.Molecules.Add(m.Clone());
            }
            clone.Elements.AddRange(Elements);

            clone.Positions.AddRange(Positions);

            clone.Sources.AddRange(Sources);

            clone.Addresses.AddRange(Addresses);

            clone.ForceFieldTerms.AddRange(ForceFieldTerms);

            return clone;
        }

        /// <summary>
        /// Removes the bond.
        /// </summary>
        /// <param name="bond">The bond.</param>
        public void RemoveBond(BondPair bond)
        {
            Bonds.RemoveBond(bond);
            GenerateMolecules();
        }

        /// <summary>
        /// Adds the bond.
        /// </summary>
        /// <param name="bond">The bond.</param>
        public void AddBond(BondPair bond)
        {
            Bonds.AddBond(bond);
            GenerateMolecules();
        }

        /// <summary>
        /// Generates the molecules from this topology.
        /// </summary>
        public void GenerateMolecules()
        {
            //TODO decide whether we should implement topology etc as a graph?
            UndirectedGraph<ushort, Edge<ushort>> graph = new UndirectedGraph<ushort, Edge<ushort>>();
            for (ushort i = 0; i < NumberOfParticles; i++)
            {
                //TODO add virtual element check.
                graph.AddVertex(i);
            }
            graph.AddEdgeRange(Bonds.Edges);

            ConnectedComponentsAlgorithm<ushort, Edge<ushort>> cc = new ConnectedComponentsAlgorithm<ushort, Edge<ushort>>(graph);
            cc.Compute();

            List<UndirectedGraph<ushort, Edge<ushort>>> moleculeGraphs = new List<UndirectedGraph<ushort, Edge<ushort>>>(cc.ComponentCount);
            for (int i = 0; i < cc.ComponentCount; i++)
            {
                moleculeGraphs.Add(new UndirectedGraph<ushort, Edge<ushort>>(false));
            }

            //loop over graph adding all vertices
            foreach (KeyValuePair<ushort, int> atomToComponent in cc.Components)
            {
                moleculeGraphs[atomToComponent.Value].AddVertex(atomToComponent.Key);
            }
            //loop over graph adding all edges
            foreach (KeyValuePair<ushort, int> atomToComponent in cc.Components)
            {
                moleculeGraphs[atomToComponent.Value].AddEdgeRange(graph.AdjacentEdges(atomToComponent.Key));
            }

            Molecules.Clear();
            Molecules.Capacity = moleculeGraphs.Count;
            foreach (UndirectedGraph<ushort, Edge<ushort>> g in moleculeGraphs)
            {
                Molecules.Add(new Molecule(g));
            }
        }

        /// <summary>
        /// Update the bonds in this topology.
        /// </summary>
        /// <param name="system"></param>
        /// <returns><c>True</c> if the bonds have changed, <c>False</c> otherwise. </returns>
        public bool UpdateBonds(IAtomicSystem system)
        {
            if (DynamicBonds != null)
            {
                Bonds = DynamicBonds.CalculateBonds(system, out bool bondsChanged);
                return bondsChanged;
            }
            return false;
        }
    }
}