﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using SlimMath;

namespace Nano.Science.Simulation.Spawning
{
    /// <summary>
    /// Utility functions for spawning templates.
    /// </summary>
    public static class SpawnHelper
    {

        /// <summary>
        /// Determines whether the inner box is indeed entirely contained within the outer box.
        /// </summary>
        /// <param name="outerBox">The outer box.</param>
        /// <param name="innerBox">The inner box.</param>
        /// <returns><c>true</c> if the inner box is contained within the outer box, <c>false</c> otherwise.</returns>
        public static bool BoxContainsBox(BoundingBox outerBox, BoundingBox innerBox)
        {
            Vector3 maxOuter = outerBox.Maximum;
            Vector3 maxInner = innerBox.Maximum;
            if (maxInner.X > maxOuter.X) return false;
            if (maxInner.Y > maxOuter.Y) return false;
            if (maxInner.Z > maxOuter.Z) return false;
            Vector3 minOuter = outerBox.Minimum;
            Vector3 minInner = innerBox.Minimum;
            if (minInner.X < minOuter.X) return false;
            if (minInner.Y < minOuter.Y) return false;
            if (minInner.Z < minOuter.Z) return false;
            return true;
        }

        
        /// <summary>
        /// Translate and rotates a set of positions by the specified translation and rotation.
        /// </summary>
        /// <param name="positions">Positions to rotate.</param>
        /// <param name="translation">Translation vector.</param>
        /// <param name="rotation">Rotation.</param>
        public static void TranslateAndRotate(ref List<Vector3> positions, Vector3 translation, Quaternion rotation)
        {
            for (int i = 0; i < positions.Count; i++)
            {
                Vector3 pos = positions[i];
                Vector3.Transform(ref pos, ref rotation, out pos);
                pos += translation;
                positions[i] = pos;
            }
        }

        /// <summary>
        /// Generates a random offset within a specified bounding box.
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        public static Vector3 RandomOffset(ref BoundingBox box)
        {
            Vector3 centre = box.Maximum - box.Minimum;

            centre.X *= (float)MathUtilities.Rand.NextDouble();
            centre.Y *= (float)MathUtilities.Rand.NextDouble();
            centre.Z *= (float)MathUtilities.Rand.NextDouble();

            centre += box.Minimum;

            return centre;
        }

        /// <summary>
        /// Generates a random sphere within a specified bounding box.
        /// </summary>
        /// <param name="sphere">Bounding sphere to provide radius.</param>
        /// <param name="box">Box within which to spawn.</param>
        /// <returns></returns>
        public static BoundingSphere RandomSphere(ref BoundingSphere sphere, ref BoundingBox box)
        {
            Vector3 centre = new Vector3();
            float radius = sphere.Radius;
            centre.X = (float)MathUtilities.Rand.NextDouble() * ((box.Maximum.X - radius) - (box.Minimum.X + radius)) + (box.Minimum.X + radius);
            centre.Y = (float)MathUtilities.Rand.NextDouble() * ((box.Maximum.Y - radius) - (box.Minimum.Y + radius)) + (box.Minimum.Y + radius);
            centre.Z = (float)MathUtilities.Rand.NextDouble() * ((box.Maximum.Z - radius) - (box.Minimum.Z + radius)) + (box.Minimum.Z + radius);
            sphere.Center = centre;
            return sphere;
        }

        /// <summary>
        /// Computes a bounding sphere around a list of positions.
        /// </summary>
        /// <param name="positions">The positions.</param>
        /// <param name="bufferRadius">The buffer radius.</param>
        /// <returns>BoundingSphere.</returns>
        public static BoundingSphere CalculateBoundingSphere(List<Vector3> positions, float bufferRadius = 0.5f)
        {
            if (positions.Count == 0)
            {
                return new BoundingSphere(Vector3.Zero, 0f);
            }

            Vector3 midpoint = new Vector3();
            foreach (Vector3 p in positions)
            {
                midpoint += p;
            }
            midpoint /= positions.Count;

            Vector3 max = positions[0] - midpoint;
            foreach (Vector3 p in positions)
            {
                if ((p - midpoint).Length > max.Length)
                    max = p;
            }

            return new BoundingSphere(midpoint, (max - midpoint).Length + bufferRadius);
        }
    }
}