﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using SlimMath;
using System.Collections.Generic;

namespace Nano.Science.Simulation.ForceField
{
    /// <summary>
    /// Represents a force field.
    /// </summary>
    /// <remarks>
    /// A force field represents the interactions between a set of particles within a system. As such, a force field should take the system and act upon it
    /// typically resulting in Cartesian forces on the relevant atoms (but not necessarily).
    ///
    /// Any IForceField will also need an accompanying <see cref="IForceFieldTerms"/> and <see cref="IInstantiatedForceFieldTerms"/> so it can be loaded
    /// from file for a given molecule/system.
    /// </remarks>
    public interface IForceField
    {
        /// <summary> Name of ForceField. </summary>
        string Name { get; }

        /// <summary>
        /// Calculates the energy and forces of the forcefield at the given positions
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="forces">The forces array to output to.</param>
        /// <returns>Total energy of forcefield in kcal / mol</returns>
        float CalculateForceField(IAtomicSystem system, List<Vector3> forces);

        /// <summary>
        /// Disposes this instance.
        /// </summary>
        void Dispose();
    }
}