﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Residues;
using Nano.Science.Simulation.Spawning;

namespace Nano.Science.Simulation.ForceField
{
    /// <summary>
    /// Interface <see cref="IForceFieldTerms"/> representing the data required to create the corresponding <see cref="IInstantiatedForceFieldTerms"/> for a particular template.
    /// </summary>
    /// <remarks>
    /// This interface is the first stage of loading a force field in the 3 part bake of spawning a simulation.
    /// When a given template is spawned, <see cref="AddToTopology(InstantiatedTopology, IResidue, ref SpawnContext)"/> is called to instantiate the force field terms
    /// in the topology.
    /// </remarks>
    /// <seealso cref="Nano.Loading.ILoadable" />
    public interface IForceFieldTerms : ILoadable
    {
        /// <summary>
        /// Instantiates the force field terms into the instantiateed topology.
        /// </summary>
        /// <param name="parentTopology">The parent topology.</param>
        /// <param name="residue">The residue.</param>
        /// <param name="context">The context.</param>
        void AddToTopology(InstantiatedTopology parentTopology, IResidue residue, ref SpawnContext context);
    }
}