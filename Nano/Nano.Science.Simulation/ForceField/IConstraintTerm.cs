﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;

namespace Nano.Science.Simulation.ForceField
{
    /// <summary>
    /// Indicates that the object implements constraints./>
    /// </summary>
    /// <seealso cref="Nano.Loading.ILoadable" />
    public interface IConstraintTerm : ILoadable
    {
        /// <summary>
        /// Determines whether this instance has constraints that need to be applied.
        /// </summary>
        /// <returns><c>true</c> if this instance has constraints; otherwise, <c>false</c>.</returns>
        bool HasConstraints();
    }
}