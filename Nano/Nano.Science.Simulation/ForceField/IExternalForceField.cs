﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Science.Simulation.ForceField
{
    /// <summary>
    /// Interface for a forcefield that is external to the intramolecular forces. 
    /// </summary>
    public interface IExternalForceField
    {
        /// <summary>
        /// Returns the potential energy contribution for this forcefield. 
        /// </summary>
        /// <returns>The potential energy contribution of this forcefield.</returns>
        float GetPotentialEnergy();
    }
}