﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Science.Simulation.ForceField
{
    /// <summary>
    /// Represents a constraint.
    /// </summary>
    public class Constraint
    {
        /// <summary>
        /// The first atom in the constraint.
        /// </summary>
        public int A;
        /// <summary>
        /// The second atom in the constraint.
        /// </summary>
        public int B;
        /// <summary>
        /// The target distance between the atoms. 
        /// </summary>
        public double Distance;

        /// <summary>
        /// Constructs a constraint. 
        /// </summary>
        /// <param name="a">First atom in the constraint.</param>
        /// <param name="b">Second atom in the constraint.</param>
        /// <param name="d">Distance between atoms.</param>
        public Constraint(int a, int b, double d)
        {
            A = a;
            B = b;
            Distance = d;
        }
    }
}