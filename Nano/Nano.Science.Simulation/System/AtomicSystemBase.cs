// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using SlimMath;
using Nano.Transport.Variables;
using Nano.Science.Simulation.Integrator;
using Nano.Science.Simulation.Log;
using Nano.Transport.Streams;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Base class for a system of atoms.
    /// </summary>
    public class AtomicSystemBase : IAtomicSystem
    {
        /// <inheritdoc />
        public InstantiatedTopology Topology { get; set; }

        /// <inheritdoc />
        public ParticleCollection Particles { get; set; }

        /// <inheritdoc />
        public List<IForceField> ForceFields { get; set; }

        /// <inheritdoc />
        public InteractionList Interactions { get; set; }

        /// <inheritdoc />
        public IIntegrator Integrator { get; set; }

        /// <inheritdoc />
        public ISimulationBoundary Boundary { get; set; }

        /// <inheritdoc />
        public List<IConstraintImplementation> Constraints { get; set; }

        /// <inheritdoc />
        public string Address { get; set; }

        /// <summary>
        /// The reporter.
        /// </summary>
        protected IReporter Reporter;

        private bool iVarsInitialised;

        /// <summary> Kinetic Energy of the system in kcal / mol </summary>
        public float KineticEnergy;

        /// <inheritdoc />
        public float PotentialEnergy { get; set; }

        /// <summary>
        /// The potential energy of the external forces being applied to the system.
        /// </summary>
        public float ExternalPotentialEnergy { get; private set; }
        
        /// <summary>
        /// Occurs when [topology change].
        /// </summary>
        public event EventHandler TopologyChange;

        /// <summary>
        /// Default constructor creates empty AtomicSystem
        /// </summary>
        public AtomicSystemBase()
        {
            Topology = new InstantiatedTopology();
            InitialiseFields();
        }

        /// <summary>
        /// Initialise arrays and fields to default values.
        /// </summary>
        protected void InitialiseFields()
        {
            Particles = new ParticleCollection();
            ForceFields = new List<IForceField>();
            Constraints = new List<IConstraintImplementation>();
            SimulationLogManager = new SimulationLogManager();
            Interactions = new InteractionList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IAtomicSystem" /> class.
        /// </summary>
        /// <param name="highTopology">The high topology.</param>
        /// <param name="systemProperties">The system properties.</param>
        /// <param name="reporter">The reporter.</param>
        /// <param name="integrator">Whether to use integrator.</param>
        public AtomicSystemBase(InstantiatedTopology highTopology, SystemProperties systemProperties, IReporter reporter, bool integrator = true)
        {
            Topology = highTopology;
            InitialiseFields();
            reporter?.PrintEmphasized("Creating atomic system.");
            Particles = new ParticleCollection();
            Reporter = reporter;

            for (int i = 0; i < highTopology.NumberOfParticles; i++)
            {
                bool isVirtual = highTopology.Elements[i] == Element.Virtual;
                Particles.AddParticle(PeriodicTable.GetElementProperties(highTopology.Elements[i]).AtomicWeight, highTopology.Positions[i], Vector3.Zero, Vector3.Zero, isVirtual);
            }

            Boundary = systemProperties.SimBoundary;
            foreach (IInstantiatedForceFieldTerms forceFieldTerm in Topology.ForceFieldTerms)
            {
                IForceField forceField = forceFieldTerm.GenerateForceField(Topology, systemProperties, Reporter);

                ForceFields.Add(forceField);

                // ReSharper disable once SuspiciousTypeConversion.Global, there is this in another solution.
                if (forceField is IConstraintImplementation item)
                {
                    // ReSharper disable once SuspiciousTypeConversion.Global this exists in Simbox.OpenMM
                    Constraints.Add(item);
                }
            }

            GetDegreesOfFreedom();

            foreach (var logger in systemProperties.SimulationLoggers)
            {
                SimulationLogManager.AddLogger(logger);
            }
            SimulationLogManager.InitialiseActiveLogs();
        }

        /// <summary>
        /// Gets the number of non virtual particles.
        /// </summary>
        /// <value>The number of non virtual particles.</value>
        public int NumberOfNonVirtualParticles => Particles.NumberOfNonVirtualParticles;

        /// <summary> Number of particles currently in the system </summary>
        public int NumberOfParticles => Particles.NumberOfParticles;

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="T:Nano.Transport.Variables.IVariableProvider" />  is initialised
        /// </summary>
        /// <value><c>true</c> if the IVariableProvider; otherwise, <c>false</c>.</value>
        public bool VariablesInitialised => iVarsInitialised;

        /// <inheritdoc />
        public void Dispose()
        {
            foreach (IForceField forceField in ForceFields)
            {
                forceField.Dispose();
            }
            ForceFields.Clear();

            SimulationLogManager.Dispose();
        }

        private int degreesOfFreedom;
        /// <summary>
        /// The instantaneous measurement of temperature, as a function of atomic velocities, in Kelvin.
        /// </summary>
        public float InstantaneousTemperature;
        private const float FloatComparisonTolerance = 1.0e-06f;

        /// <inheritdoc />
        public IAtomSelectionPool AtomSelections { get; set; }

        /// <inheritdoc />
        public int GetDegreesOfFreedom()
        {
            int dof = 0;
            for (int i = 0; i < Particles.NumberOfParticles; i++)
            {
                if (Particles.Mass[i] > 0)
                    dof += 3;
            }
            foreach (IConstraintImplementation constraintImpl in Constraints)
            {
                dof -= constraintImpl.GetNumberOfConstraints();
            }
            if (Integrator != null)
            {
                if (Integrator.RemovesCmMotion())
                    dof -= 3;
            }
            degreesOfFreedom = dof;
            return dof;
        }

        /// <summary>
        /// Adds the specified force field to the atomic system.
        /// </summary>
        /// <param name="forcefield">The force field.</param>
        public void AddForceField(IForceField forcefield)
        {
            ForceFields.Add(forcefield);
        }

        /// <summary>
        /// Calculates the potential energy (kJ/mol) and applies cartesian forces
        /// to this system
        /// </summary>
        /// <returns> Potential Energy (kJ/mol) </returns>
        public virtual float CalculateForceFields()
        {
            PotentialEnergy = 0f;
            ExternalPotentialEnergy = 0f;
            Particles.ZeroForces();
            foreach (IForceField field in ForceFields)
            {
                float energyContribution = field.CalculateForceField(this, Particles.Force);
                if (field is IExternalForceField)
                    ExternalPotentialEnergy += energyContribution;
                else
                    PotentialEnergy += energyContribution;
            }
            return PotentialEnergy;
        }

        /// <summary>
        /// Sets the topology.
        /// </summary>
        /// <param name="topology">The topology.</param>
        public void SetTopology(InstantiatedTopology topology)
        {
            Topology = topology;
            TopologyChange?.Invoke(this, null);
        }

        /// <summary>
        /// Resets velocities with specified velocities
        /// </summary>
        /// <param name="velocity">Source of new atomic velocities</param>
        /// <exception cref="Exception">Length of array of source velocities differs from target.</exception>
        public void InitializeVelocities(List<Vector3> velocity)
        {
            if (velocity.Count != NumberOfParticles)
            {
                throw new Exception("Length of array of source velocities differs from target.");
            }
            for (int i = 0; i < NumberOfParticles; i++)
            {
                Particles.Velocity[i] = velocity[i];
            }
        }

        /// <summary>
        /// Resets velocities by sampling from a Boltzmann distribution
        /// </summary>
        /// <param name="temperature">Temperature</param>
        /// <exception cref="System.Exception">Attempted to initialize velocities at temperature less than 0 Kelvin!</exception>
        public void InitializeVelocitiesToTemperature(float temperature)
        {
            if (temperature <= 0f)
            {
                throw new Exception("Attempted to initialize velocities at temperature <= 0 Kelvin!");
            }

            List<double> randomNoise = MathUtilities.GenerateListOfGaussianNoise(NumberOfParticles * 3);

            for (int i = 0; i < NumberOfParticles; i++)
            {
                if (Math.Abs(Particles.Mass[i]) < FloatComparisonTolerance)
                    continue;
                //get molar gas constant in kJ / (K * mol)
                const float boltz = PhysicalConstants.MolarGasConstantR / 1000;
                double velocityScale = Math.Sqrt(boltz * temperature / Particles.Mass[i]);
                Vector3 velocity = new Vector3((float)randomNoise[3 * i + 0], (float)randomNoise[3 * i + 1], (float)randomNoise[3 * i + 2]);
                Particles.Velocity[i] = velocity * (float)velocityScale;
            }
            CalculateKineticEnergyAndTemperature();
        
        }

        /// <summary>
        /// Scales the molecule center positions by the given scale factor in each dimension.
        /// </summary>
        /// <param name="scaleFactor">The scale factor.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void ScaleMoleculeCenters(float scaleFactor)
        {
            //Generate a list of atoms in molecules
            HashSet<int> atomsInComponents = new HashSet<int>();
            foreach (Molecule c in Topology.Molecules)
            {
                Vector3 centroid = Vector3.Zero;
                foreach (var atom in c.AtomIndexes)
                {
                    centroid += Particles.Position[atom];
                    atomsInComponents.Add(atom);
                }
                centroid /= c.AtomIndexes.Count;
                Vector3 diff = centroid * scaleFactor - centroid;
                foreach (var atom in c.AtomIndexes)
                {
                    Particles.Position[atom] += diff;
                }
            }
            for (var i = 0; i < NumberOfParticles; i++)
            {
                if (!(atomsInComponents.Contains(i)))
                {
                    Particles.Position[i] *= scaleFactor * 0.5f;
                }
            }
        }

        /// <summary>
        /// Calculates the KE (kJ/mol) and Temperature (Kelvin) of system
        /// in its current state
        /// </summary>
        /// <returns></returns>
        public virtual Vector2 CalculateKineticEnergyAndTemperature()
        {
            KineticEnergy = 0f;
            for (int i = 0; i < NumberOfParticles; i++)
            {
                //KE in kJ/mol. The beauty of our unit system emerges, we don't have to do any conversion!
                KineticEnergy += 0.5f * Particles.Mass[i] * (Particles.Velocity[i]).LengthSquared;
                //Full conversion not necessary, left here for reference: Calculate kinetic energy in kJ/mol, converting velocities from nm/ps to m/s and mass from amu to kg.
                //KineticEnergy += 0.5f * PhysicalConstants.AvogadroC * Units.KgPerAmu * Particles.Mass[i] * (Particles.Velocity[i] * Units.MPerNm * Units.PsPerS).LengthSquared ;
            }
            //get molar gas constant in kJ / (K * mol)
            const float boltz = PhysicalConstants.MolarGasConstantR / 1000;
            InstantaneousTemperature = 2 * KineticEnergy / (degreesOfFreedom * boltz);
            //InstantaneousTemperature = KineticEnergy / (NumberOfNonVirtualParticles * PhysicalConstants.KB);
            return new Vector2(KineticEnergy, InstantaneousTemperature);
        }

        /// <summary>
        /// Gets the center of mass of each molecule in the system.
        /// </summary>
        /// <param name="cmList">If passed, this list will be used to perform the operation in place.</param>
        /// <returns></returns>
        public List<Vector3> GetMoleculesCenterOfMass(List<Vector3> cmList = null)
        {
            int i;
            if (cmList == null || cmList.Count != Topology.Molecules.Count)
            {
                cmList = new List<Vector3>(Topology.Molecules.Count);
                for (i = 0; i < Topology.Molecules.Count; i++)
                {
                    cmList.Add(Vector3.Zero);
                }
            }
            i = 0;
            foreach (Molecule molecule in Topology.Molecules)
            {
                Vector3 cm = molecule.GetCentreOfMass(Particles);
                cmList[i] = cm;
                i++;
            }
            return cmList;
        }

        /// <inheritdoc />
        public virtual void InitialiseVariables(VariableManager variableManager)
        {
            iVarsInitialised = true;
            foreach (IForceField forceField in ForceFields)
            {
                (forceField as IVariableProvider)?.InitialiseVariables(variableManager);
            }
            (Integrator as IVariableProvider)?.InitialiseVariables(variableManager);
            (Boundary as IVariableProvider)?.InitialiseVariables(variableManager);
        }

        /// <inheritdoc />
        public virtual void ResetVariableDesiredValues()
        {
            if (VariablesInitialised == false)
                return;

            foreach (IForceField forceField in ForceFields)
            {
                (forceField as IVariableProvider)?.ResetVariableDesiredValues();
            }
            (Integrator as IVariableProvider)?.ResetVariableDesiredValues();
            (Boundary as IVariableProvider)?.ResetVariableDesiredValues();
        }

        /// <inheritdoc />
        public virtual void UpdateVariableValues()
        {
            if (!VariablesInitialised) return;
            foreach (IForceField forceField in ForceFields)
            {
                (forceField as IVariableProvider)?.UpdateVariableValues();
            }
            (Integrator as IVariableProvider)?.UpdateVariableValues();
            (Boundary as IVariableProvider)?.UpdateVariableValues();
        }

        /// <inheritdoc />
        public void AttachTransportContext(TransportContext transportContext)
        {
        }

        /// <inheritdoc />
        public void DetachTransportContext(TransportContext transportContext)
        {
        }

        /// <summary>
        /// Tests whether the topology has changed, by updating any dynamic bonds.
        /// </summary>
        /// <returns><c>true</c> if the bonds in the topology have changed, <c>false</c> otherwise.</returns>
        public bool HasTopologyChanged()
        {
            
            bool changed = Topology.UpdateBonds(this);
            if (changed) TopologyChange?.Invoke(this, null);
            return changed;
        }

        /// <summary>
        /// Populates the bond stream.
        /// </summary>
        /// <param name="bondStream">The bonds.</param>
        public void PopulateBondStream(IDataStream<BondPair> bondStream)
        {
            int i = 0;
            
            //TODO this must be slow?
            foreach (BondPair bond in Topology.Bonds.BondIndexes)
            {
                if (AtomSelections.VisibleAtoms.Contains(bond.A) && AtomSelections.VisibleAtoms.Contains(bond.B))
                {
                    bondStream.Data[i] = new BondPair(AtomSelections.VisibleAtoms.IndexOf(bond.A), AtomSelections.VisibleAtoms.IndexOf(bond.B));
                    i++;
                }
            }
            bondStream.Count = i;
        }

        /// <summary>
        /// Populates the element stream.
        /// </summary>
        /// <param name="atomElements">The atom elements.</param>
        public void PopulateElementStream(IDataStream<ushort> atomElements)
        {
            //TODO speed this up with a bulk transfer
            for (int i = 0; i < AtomSelections.VisibleAtoms.Count; i++)
            {
                atomElements.Data[i] = (ushort)Topology.Elements[AtomSelections.VisibleAtoms[i]];
            }

            atomElements.Count = AtomSelections.VisibleAtoms.Count;
        }

        /// <summary>
        /// Populates the position stream.
        /// </summary>
        /// <param name="atomPositions">The atom positions.</param>
        public void PopulatePositionStream(IDataStream<Vector3> atomPositions)
        {
            //TODO speed this up with a bulk transfer
            for (int i = 0; i < AtomSelections.VisibleAtoms.Count; i++)
            {
                atomPositions.Data[i] = Particles.Position[AtomSelections.VisibleAtoms[i]];
            }

            atomPositions.Count = AtomSelections.VisibleAtoms.Count;
        }

        /// <summary>
        /// Populates the stream of visible atoms based on the currently visible atoms in the system.
        /// </summary>
        /// <param name="visibleAtoms"></param>
        public void PopulateVisibleAtomStream(IDataStream<int> visibleAtoms)
        {
            for(int i=0; i < AtomSelections.VisibleAtoms.Count; i++)
            {
                visibleAtoms.Data[i] = AtomSelections.VisibleAtoms[i];
            }

            visibleAtoms.Count = AtomSelections.VisibleAtoms.Count;
        }
        /// <summary>
        /// Populates the velocity stream.
        /// </summary>
        /// <param name="atomVelocities">The atom velocities.</param>
        public void PopulateVelocityStream(IDataStream<Vector3> atomVelocities)
        {
            //TODO speed this up with a bulk transfer
            for (int i = 0; i < AtomSelections.VisibleAtoms.Count; i++)
            {
                atomVelocities.Data[i] = Particles.Velocity[AtomSelections.VisibleAtoms[i]];
            }

            atomVelocities.Count = AtomSelections.VisibleAtoms.Count;
        }

        /// <inheritdoc />
        public SimulationLogManager SimulationLogManager { get; set; }
    }
}