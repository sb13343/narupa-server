// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano.Transport.OSCCommands;
using Nano.Transport.Variables;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Integrator;
using Nano.Science.Simulation.Log;
using SlimMath;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents an system of atoms in a molecular dynamics simulation.
    /// </summary>
    public interface IAtomicSystem : IVariableProvider, IDisposable, ICommandProvider
    {
        /// <summary> The elements and connectivity of the system </summary>
        InstantiatedTopology Topology { get; set; }

        /// <summary>
        /// The particles in the simulation.
        /// </summary>
        ParticleCollection Particles { get; set; }

        /// <summary>
        /// The constraints the particles in this system are subject to.
        /// </summary>
        List<IConstraintImplementation> Constraints { get; set; }

        /// <summary>
        /// The set of force fields acting on this system.
        /// </summary>
        List<IForceField> ForceFields { get; set; }

        /// <summary>
        /// The set of active interactions on the system.
        /// </summary>
        InteractionList Interactions { get; set; }

        /// <summary>
        /// The integrator operating on this system.
        /// </summary>
        IIntegrator Integrator { get; set; }

        /// <summary>
        /// The boundary of this system.
        /// </summary>
        ISimulationBoundary Boundary { get; set; }

        /// <summary>
        /// The sets of atomic selections made by users, controlling what will be visible and interactable.
        /// </summary>
        IAtomSelectionPool AtomSelections { get; set; }

        /// <summary>
        /// Manages any trajectory logging.
        /// </summary>
        SimulationLogManager SimulationLogManager { get; set; }

        /// <summary>
        /// Gets the number of non virtual particles.
        /// </summary>
        /// <value>The number of non virtual particles.</value>
        int NumberOfNonVirtualParticles { get; }

        /// <summary> Number of particles currently in the system </summary>
        int NumberOfParticles { get; }

        /// <summary>
        /// The potential energy of the system in kJ/mol.
        /// </summary>
        float PotentialEnergy { get; set;}

        /// <summary>
        /// Indicates that the topology has changed.
        /// </summary>
        event EventHandler TopologyChange;

        /// <summary>
        /// Gets the degrees of freedom in the system.
        /// </summary>
        /// <returns></returns>
        int GetDegreesOfFreedom();

        /// <summary>
        /// Adds the specified force field to the atomic system.
        /// </summary>
        /// <param name="forcefield">The force field.</param>
        void AddForceField(IForceField forcefield);

        /// <summary>
        /// Calculates the potential energy (kJ/mol) and applies cartesian forces
        /// to this system
        /// </summary>
        /// <returns> Potential Energy (kJ/mol) </returns>
        float CalculateForceFields();

        /// <summary>
        /// Sets the topology.
        /// </summary>
        /// <param name="topology">The topology.</param>
        void SetTopology(InstantiatedTopology topology);

        /// <summary>
        /// Resets velocities with specified velocities
        /// </summary>
        /// <param name="velocity">Source of new atomic velocities</param>
        /// <exception cref="Exception">Length of array of source velocities differs from target.</exception>
        void InitializeVelocities(List<Vector3> velocity);

        /// <summary>
        /// Resets velocities by sampling from a Boltzmann distribution
        /// </summary>
        /// <param name="temperature">Temperature</param>
        /// <exception cref="System.Exception">Attempted to initialize velocities at temperature less than 0 Kelvin!</exception>
        void InitializeVelocitiesToTemperature(float temperature);

        /// <summary>
        /// Calculates the KE (kJ/mol) and Temperature (Kelvin) of system
        /// in its current state
        /// </summary>
        /// <returns></returns>
        Vector2 CalculateKineticEnergyAndTemperature();

        /// <summary>
        /// Scales the molecular centers by the given length scale.
        /// </summary>
        /// <param name="lengthScale"></param>
        void ScaleMoleculeCenters(float lengthScale);
    }
}