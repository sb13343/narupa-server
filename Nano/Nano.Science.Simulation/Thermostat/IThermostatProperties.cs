﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using System;
using Nano.Science.Simulation.Integrator;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents the loadable properties of a thermostat.
    /// </summary>
    public interface IThermostatProperties : ILoadable, IEquatable<IThermostatProperties>
    {
        /// <summary>
        /// Name of this thermostat.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Instantiates the stat in the specified system.
        /// </summary>
        /// <param name="system">The system.</param>
        /// <param name="integrator">The integrator.</param>
        /// <returns>IStationary.</returns>
        IThermostat InitialiseStat(IAtomicSystem system, IIntegrator integrator);

        /// <summary>
        /// Copies this instance.
        /// </summary>
        /// <returns></returns>
        IThermostatProperties Clone();
    }
}