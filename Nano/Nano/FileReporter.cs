﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Net;
using Rug.Osc;

namespace Nano
{
    /// <summary>
    /// Helper class for writing to file.
    /// </summary>
    public class FileWriterHelper
    {
        private static readonly object SyncLock = new object();

        /// <summary>
        /// Gets or sets a value indicating whether to show full errors.
        /// </summary>
        /// <value><c>true</c> if [show full errors]; otherwise, <c>false</c>.</value>
        public bool ShowFullErrors { get; set; }

        private StreamWriter writer;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileWriterHelper"/> class.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        public FileWriterHelper(string fileName)
        {
            writer = new StreamWriter(fileName);
        }

        /// <summary>
        /// Opens the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        public void OpenFile(string fileName)
        {
            lock (SyncLock)
            {
                try
                {
                    writer.Close();
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch
                {
                }

                writer = new StreamWriter(fileName);
            }
        }

        /// <summary>
        /// Closes the file.
        /// </summary>
        public void CloseFile()
        {
            lock (SyncLock)
            {
                writer.Close();
            }
        }

        private void Write_Inner(string str)
        {
            writer.Write(str);
        }

        private void WriteLine_Inner(string str)
        {
            writer.WriteLine(str);
        }

        private void WriteStackTrace_Inner(string trace)
        {
            if (string.IsNullOrEmpty(trace) != false)
            {
                return;
            }

            writer.WriteLine();
            writer.WriteLine(new string('=', 80));

            writer.WriteLine("  " + trace.Replace("\n", "\n  "));
            writer.WriteLine();

            writer.WriteLine(new string('=', 80));
            writer.WriteLine();
        }

        /// <summary>
        /// Writes the specified string and adds a new line.
        /// </summary>
        /// <param name="str">The string.</param>
        public void WriteLine(string str)
        {
            lock (SyncLock)
            {
                WriteLine_Inner(str);
            }
        }

        /// <summary>
        /// Writes the exception.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        public void WriteException(string message, Exception ex)
        {
            lock (SyncLock)
            {
                WriteLine_Inner(message);

                if (ShowFullErrors == true)
                {
                    WriteException_Inner(ex);
                }
                else
                {
                    WriteLine_Inner(ex.Message);
                }
            }
        }

        private void WriteException_Inner(Exception ex)
        {
            WriteLine_Inner(ex.Message);

            WriteStackTrace_Inner(ex.StackTrace);
        }

        /// <summary>
        /// Writes the message.
        /// </summary>
        /// <param name="direction">The direction.</param>
        /// <param name="timeTag">The time tag.</param>
        /// <param name="message">The message.</param>
        public void WriteMessage(Direction direction, OscTimeTag? timeTag, string message)
        {
            lock (SyncLock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        Write_Inner("TX ");
                        break;

                    case Direction.Receive:
                        Write_Inner("RX ");
                        break;

                    case Direction.Action:
                        Write_Inner("   ");
                        break;
                }

                if (timeTag != null)
                {
                    string timeTagString = timeTag + " ";
                    Write_Inner(timeTagString);
                }

                WriteLine_Inner(message);
            }
        }

        /// <summary>
        /// Writes the message.
        /// </summary>
        /// <param name="direction">The direction.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="message">The message.</param>
        public void WriteMessage(Direction direction, string prefix, string message)
        {
            lock (SyncLock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        Write_Inner("TX ");
                        break;

                    case Direction.Receive:
                        Write_Inner("RX ");
                        break;

                    case Direction.Action:
                        Write_Inner("   ");
                        break;
                        
                }

                if (string.IsNullOrEmpty(prefix) == false)
                {
                    Write_Inner(prefix + " ");
                }

                WriteLine_Inner(message);
            }
        }

        internal void Flush()
        {
            lock (SyncLock)
            {
                writer.Flush();
            }
        }
    }

    public class FileReporter : IReporter, IDisposable
    {
        private readonly object syncLock = new object();

        public IOscMessageFilter OscMessageFilter
        {
            get => oscMessageFilter;
            set
            {
                oscMessageFilter = value;

                if (cmdReporter != null) cmdReporter.OscMessageFilter = value;
            }
        }

        public ReportVerbosity ReportVerbosity
        {
            get => reportVerbosity;
            set
            {
                reportVerbosity = value;

                if (cmdReporter != null) cmdReporter.ReportVerbosity = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [show full errors].
        /// </summary>
        /// <value><c>true</c> if [show full errors]; otherwise, <c>false</c>.</value>
        public bool ShowFullErrors { get; set; }

        private readonly IReporter cmdReporter;
        private IOscMessageFilter oscMessageFilter;
        private ReportVerbosity reportVerbosity;

        private readonly FileWriterHelper writer;

        public FileReporter(string filePath, IReporter reporter = null)
        {
            writer = new FileWriterHelper(Helper.ResolvePath(filePath));
            ReportVerbosity = ReportVerbosity.Debug;
            oscMessageFilter = reporter?.OscMessageFilter;
            cmdReporter = reporter;
        }

        public void PrintBlankLine(ReportVerbosity verbosity)
        {
            cmdReporter?.PrintBlankLine(verbosity);
            
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                writer.WriteLine("");
            }
        }

        public void PrintDebug(string format, params object[] args)
        {
            cmdReporter?.PrintDebug(format, args);
            
            Print(format, args, ReportVerbosity.Debug);
        }

        public void PrintDebug(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            cmdReporter?.PrintDebug(direction, endPoint, format, args);
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Debug);
        }

        public void PrintDebug(Direction direction, string ident, string format, params object[] args)
        {
            cmdReporter?.PrintDebug(direction, ident, format, args);
            Print(direction, ident, format, args, ReportVerbosity.Debug);
        }

        public void PrintDetail(string format, params object[] args)
        {
            cmdReporter?.PrintDetail(format, args);
            Print(format, args, ReportVerbosity.Detail);
        }

        public void PrintDetail(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            cmdReporter?.PrintDetail(direction, endPoint, format, args);
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Detail);
        }

        public void PrintDetail(Direction direction, string ident, string format, params object[] args)
        {
            cmdReporter?.PrintDetail(direction, ident, format, args);
            Print(direction, ident, format, args, ReportVerbosity.Detail);
        }

        public void PrintEmphasized(string format, params object[] args)
        {
            cmdReporter?.PrintEmphasized(format, args);
            Print(format, args, ReportVerbosity.Emphasized);
        }

        public void PrintEmphasized(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            cmdReporter?.PrintEmphasized(direction, endPoint, format, args);
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Emphasized);
        }

        public void PrintEmphasized(Direction direction, string ident, string format, params object[] args)
        {
            cmdReporter?.PrintEmphasized(direction, ident, format, args);
            Print(direction, ident, format, args, ReportVerbosity.Emphasized);
        }

        public void PrintError(string format, params object[] args)
        {
            cmdReporter?.PrintError(format, args);
            
            lock (syncLock)
            {
                writer.WriteLine(args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintError(Direction direction, IPEndPoint origin, string format, params object[] args)
        {
            cmdReporter?.PrintError(direction, origin, format, args);
            
            lock (syncLock)
            {
                writer.WriteMessage(direction, origin.ToString(), args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintError(Direction direction, string ident, string format, params object[] args)
        {
            cmdReporter?.PrintError(direction, ident, format, args);
            
            lock (syncLock)
            {
                writer.WriteMessage(direction, ident, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        //TODO FINISH THIS! and remove OnPrint etc.
        public void PrintException(Exception ex, string format, params object[] args)
        {
            cmdReporter?.PrintException(ex, format, args);
            
            lock (syncLock)
            {
                writer.WriteException(args.Length == 0 ? format : string.Format(format, args), ex);
            }
        }

        /// <inheritdoc />
        public void PrintHeading(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Emphasized);
            
            cmdReporter?.PrintHeading(format, args);              
        }

        public void PrintNormal(string format, params object[] args)
        {
            Print(format, args, ReportVerbosity.Normal);
            
            cmdReporter?.PrintNormal(format, args);
        }

        public void PrintNormal(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            Print(direction, endPoint?.ToString(), format, args, ReportVerbosity.Normal);
            cmdReporter?.PrintNormal(direction, endPoint, format, args);
        }

        public void PrintNormal(Direction direction, string ident, string format, params object[] args)
        {
            Print(direction, ident, format, args, ReportVerbosity.Normal);
            cmdReporter?.PrintNormal(direction, ident, format, args);
        }

        public void PrintOscPackets(Direction direction, params OscPacket[] packets)
        {
            cmdReporter?.PrintOscPackets(direction, packets);
            lock (syncLock)
            {
                foreach (OscPacket packet in packets)
                {
                    switch (packet)
                    {
                        case OscMessage oscMessage:
                            PrintOscMessage(direction, oscMessage);
                            break;
                        case OscBundle bundle:
                            foreach (OscPacket sub in bundle)
                            {
                                PrintOscPackets(direction, sub);
                            }
                            break;
                    }
                }
            }
        }

        public void PrintOscPackets(Direction direction, IPEndPoint endPoint, params OscPacket[] packets)
        {
            cmdReporter?.PrintOscPackets(direction, endPoint, packets);
            lock (syncLock)
            {
                foreach (OscPacket packet in packets)
                {
                    switch (packet)
                    {
                        case OscMessage oscMessage:
                            PrintOscMessage(direction, endPoint, oscMessage);
                            break;
                        case OscBundle bundle:
                            foreach (OscPacket sub in bundle)
                            {
                                PrintOscPackets(direction, endPoint, sub);
                            }
                            break;
                    }
                }
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, string format, params object[] args)
        {
            cmdReporter?.PrintWarning(verbosity, format, args);
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                writer.WriteLine(args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
            cmdReporter?.PrintWarning(verbosity, direction, endPoint, format, args);
            
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                writer.WriteMessage(direction, endPoint.ToString(), args.Length == 0 ? format : string.Format(format, args));
            }
        }

        public void PrintWarning(ReportVerbosity verbosity, Direction direction, string ident, string format, params object[] args)
        {
            cmdReporter?.PrintWarning(verbosity, direction, ident, format, args);
            
            if (ShouldPrint(verbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                writer.WriteMessage(direction, ident, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        private void Print(string format, object[] args, ReportVerbosity reportVerbosity)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                writer.WriteMessage(Direction.Action, "", args.Length == 0 ? format : string.Format(format, args));
            }
        }

        private void Print(Direction direction, string ident, string format, object[] args, ReportVerbosity reportVerbosity)
        {
            if (ShouldPrint(reportVerbosity) == false)
            {
                return;
            }

            lock (syncLock)
            {
                writer.WriteMessage(direction, ident, args.Length == 0 ? format : string.Format(format, args));
            }
        }

        private void PrintOscMessage(Direction direction, OscMessage oscMessage)
        {
            if (OscMessageFilter?.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            writer.WriteMessage(direction, oscMessage.Origin.ToString(), oscMessage.ToString());
        }

        private void PrintOscMessage(Direction direction, IPEndPoint endPoint, OscMessage oscMessage)
        {
            if (OscMessageFilter?.ShouldPrintMessage(oscMessage) == false)
            {
                return;
            }

            writer.WriteMessage(direction, endPoint.ToString(), oscMessage.ToString());
        }

        private bool ShouldPrint(ReportVerbosity verbosity)
        {
            return (int)ReportVerbosity <= (int)verbosity;
        }

        public void Flush()
        {
            lock (syncLock)
            {
                writer.Flush();
            }
        }

        public void Dispose()
        {
            lock (syncLock)
            {
                writer.CloseFile();
            }
        }
    }
}