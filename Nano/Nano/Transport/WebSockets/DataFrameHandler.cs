﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Text;

namespace Nano.WebSockets
{
    public delegate void DataFrameBinaryEvent(DataFrameHandler handler, byte[] buffer, int index, int count);

    public delegate void DataFrameSendEvent(DataFrameHandler handler, byte[] buffer, int index, int count);

    public delegate void DataFrameTextEvent(DataFrameHandler handler, string text);

    public class DataFrameHandler
    {
        public readonly string EndPoint;

        //private readonly Stream stream;

        private readonly bool maskData;

        private readonly BinaryReader reader;
        private readonly MemoryStream readStream;
        private readonly BinaryWriter writer;
        private readonly MemoryStream writeStream;

        private IReporter reporter;

        public event DataFrameBinaryEvent BinaryFrame;

        public event DataFrameSendEvent FrameComplete;

        public event DataFrameTextEvent TextFrame;

        public DataFrameHandler(string endPoint, bool maskData, IReporter reporter)
        {
            EndPoint = endPoint;

            this.reporter = reporter;

            this.maskData = maskData;

            readStream = new MemoryStream();
            reader = new BinaryReader(readStream);

            writeStream = new MemoryStream();
            writer = new BinaryWriter(writeStream);
        }

        public void Process(byte[] buffer, int index, int length)
        {
            // cache the current read head
            long startPosition = readStream.Position;

            // move the read head to the last byte of the current buffer
            readStream.Position = readStream.Length;

            // write the next blob into the read buffer
            readStream.Write(buffer, index, length);
            
            // reset the position of the read head
            readStream.Position = startPosition;

            try
            {
                while (readStream.Length - readStream.Position > 0)
                {
                    // cache the position of the start of the data frame
                    startPosition = readStream.Position;

                    DataFrame frame = new DataFrame();

                    switch (frame.Read(reader))
                    {
                        case DataFrameReadState.Failed:
                            throw new Exception("Invalid data frame header");

                        case DataFrameReadState.NotEnoughBytesForHeader:
                        case DataFrameReadState.IncompletePayload:
                            // reset the read head to the start of the data frame 
                            readStream.Position = startPosition;
                            return;

                        case DataFrameReadState.DataFrameTooLarge:
                            throw new Exception("Data frame too large"); 

                        case DataFrameReadState.Complete:
                            break;

                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    reporter.PrintDebug(Direction.Receive, EndPoint, $@"{frame.Opcode} {(frame.HasMask ? "Masked" : "Unmasked")} ({frame.PayloadLength} bytes)");

                    switch (frame.Opcode)
                    {
                        case Opcode.Cont:
                            throw new NotImplementedException("Partial data frame detected");

                        case Opcode.Text:
                            TextFrame?.Invoke(this, Encoding.UTF8.GetString(frame.PayloadData));
                            break;

                        case Opcode.Binary:
                            BinaryFrame?.Invoke(this, frame.PayloadData, 0, frame.PayloadData.Length);
                            break;

                        case Opcode.Close:
                            break;

                        case Opcode.Ping:
                            SendPong(frame);
                            break;

                        case Opcode.Pong:
                            break;

                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            finally
            {
                if (readStream.Position == readStream.Length)
                {
                    readStream.Position = 0;
                    readStream.SetLength(0);
                }
            }
        }

        public void SendBinary(byte[] buffer)
        {
            DataFrame frame = new DataFrame
            {
                Opcode = Opcode.Binary,
                PayloadData = buffer,
                PayloadLength = buffer.Length
            };

            if (maskData == true)
            {
                frame.CreateMask();
            }

            SendDataFrame(frame);
        }

        public void SendBinary(byte[] data, int index, int count)
        {
            if (index == 0 && count == data.Length)
            {
                SendBinary(data);
            }
            else
            {
                byte[] copy = new byte[count];
                Array.ConstrainedCopy(data, index, copy, 0, count);
                SendBinary(copy);
            }
        }

        public void SendDataFrame(DataFrame frame)
        {
            frame.Write(writer);

            //reporter.PrintDebug(Direction.Transmit, EndPoint, $@"Data frame ({writeStream.Length} bytes)");
            //reporter.PrintDebug(Direction.Transmit, EndPoint, $@"{frame.Opcode} {(frame.HasMask ? "Masked" : "Unmasked")} ({frame.PayloadLength} bytes)");

            FrameComplete?.Invoke(this, writeStream.ToArray(), 0, (int)writeStream.Length);

            ClearWriteStream();
        }

        public void SendPing()
        {
            DataFrame frame = new DataFrame
            {
                Opcode = Opcode.Ping,
                PayloadData = new byte[0],
                PayloadLength = 0
            };

            if (maskData == true)
            {
                frame.CreateMask();
            }

            SendDataFrame(frame);
        }
        
        public void SendClose()
        {
            DataFrame frame = new DataFrame
            {
                Opcode = Opcode.Close,
                PayloadData = new byte[0],
                PayloadLength = 0
            };

            if (maskData == true)
            {
                frame.CreateMask();
            }

            SendDataFrame(frame);
        }

        public void SendPong(DataFrame ping)
        {
            DataFrame frame = new DataFrame
            {
                Opcode = Opcode.Pong,
                PayloadData = ping.PayloadData,
                PayloadLength = ping.PayloadLength
            };

            if (maskData == true)
            {
                frame.CreateMask();
            }

            SendDataFrame(frame);
        }

        public void SendText(string text)
        {
            byte[] data = Encoding.UTF8.GetBytes(text);

            DataFrame frame = new DataFrame
            {
                Opcode = Opcode.Text,
                PayloadData = data,
                PayloadLength = data.Length
            };

            if (maskData == true)
            {
                frame.CreateMask();
            }

            SendDataFrame(frame);
        }

        private void ClearWriteStream()
        {
            writeStream.Position = 0;
            writeStream.SetLength(0);
        }
    }
}