﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Security.Cryptography;
using System.Text;

namespace Nano.WebSockets
{
    public static class Keys
    {
        internal static readonly RandomNumberGenerator RandomNumber;

        private static readonly Guid ApiSecret = new Guid("CF507F6A-EF3C-4B36-8D09-578E14B501B0");
        private static readonly Guid WebSocketSecret = new Guid("258EAFA5-E914-47DA-95CA-C5AB0DC85B11");                                                                

        static Keys()
        {
            RandomNumber = RandomNumberGenerator.Create();
        }

        public static string GetAcceptString(string clientKey)
        {
            return ComputeSecretString(clientKey, WebSocketSecret);             
        }
        
        public static string GetOpenClientKey()
        {
            byte[] clientKeyBytes = new byte[]
            {
                227, 189, 198, 86,
                117, 173, 114, 171,
                7,   52,  3,   198,
                37,  220, 61,  79,
                135, 170, 76,  100,
                93,  248, 182, 55,
                196, 170, 86,  165,
                246, 127, 40,  33
            };

            return Convert.ToBase64String(clientKeyBytes);
        }

        public static string CreateClientKey()
        {
            byte[] clientKeyBytes = new byte[32];

            RandomNumber.GetBytes(clientKeyBytes);

            return Convert.ToBase64String(clientKeyBytes);
        }

        public static string GenerateSecret()
        {
            byte[] secretKeyBytes = new byte[32];

            RandomNumber.GetBytes(secretKeyBytes);

            return Convert.ToBase64String(secretKeyBytes);
        }

        public static string GetSecretString(string secret)
        {
            return ComputeSecretString(secret, ApiSecret);
        }

        public static bool VerifySecret(string secret, string key)
        {                 
            return GetSecretString(secret).Equals(key); 
        }

        public static string ComputeSecretString(string publicKey, Guid privateKey)
        {
            return Convert.ToBase64String(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(publicKey + privateKey.ToString().ToUpperInvariant())));
        }
    }
}