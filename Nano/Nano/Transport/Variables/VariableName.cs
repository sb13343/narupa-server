﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Rug.Osc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nano.Transport.Variables
{
    /// <summary>
    /// The imitable name of a variable.
    /// </summary>
    public sealed class VariableName : IEquatable<VariableName>, IEquatable<string>, IEquatable<int>
    {
        #region Static Members

        /// <summary>
        /// Density.
        /// </summary>
        public static readonly VariableName Density;

        /// <summary>
        /// Kinetic Energy.
        /// </summary>
        public static readonly VariableName KineticEnergy;

        /// <summary>
        /// Number Of Particles.
        /// </summary>
        public static readonly VariableName NumberOfParticles;

        /// <summary>
        /// Number Of Molecules.
        /// </summary>
        public static readonly VariableName NumberOfMolecules;

        public static readonly VariableName NumberOfMols;

        /// <summary>
        /// Whether this simulation is using a periodic box.
        /// </summary>
        public static readonly VariableName PeriodicBox;

        /// <summary>
        /// Potential Energy.
        /// </summary>
        public static readonly VariableName PotentialEnergy;

        /// <summary>
        /// Pressure.
        /// </summary>
        public static readonly VariableName Pressure;

        /// <summary>
        /// Bounding Box of Simulation.
        /// </summary>
        public static readonly VariableName SimBoundingBox;

        /// <summary>
        /// Target Temperature (Kelvin).
        /// </summary>
        public static readonly VariableName Temperature;

        /// <summary>
        /// Time Step (fs).
        /// </summary>
        public static readonly VariableName TimeStep;

        /// <summary>
        /// Volume.
        /// </summary>
        //public static readonly VariableName Volume;

        /// <summary>
        /// The simulation time (ps).
        /// </summary>
        public static readonly VariableName SimulationTime;

        /// <summary>
        /// The time spent running the simulation compute per frame.
        /// </summary>
        public static readonly VariableName ComputationTime;

        /// <summary>
        /// Name of the simulation currently running.
        /// </summary>
        public static readonly VariableName SimulationName;

        /// <summary>
        /// Gradient Scale Factor for Interactive Gaussian ForceField
        /// </summary>
        public static readonly VariableName GradientScaleFactor;

        /// <summary>
        /// Sigma for interactive gaussian forcefield.
        /// </summary>
        public static readonly VariableName Sigma;

        /// <summary>
        /// All Names.
        /// </summary>
        private static readonly Dictionary<int, VariableName> allNames = new Dictionary<int, VariableName>();

        static VariableName()
        {
            Density = CreateName("/density", "Density");
            NumberOfParticles = CreateName("/numAtoms", "Number Of Atoms");
            NumberOfMolecules = CreateName("/numMolecules", "Number Of Molecules");
            NumberOfMols = CreateName("/numMol", "Number of Mols");
            Pressure = CreateName("/pressure", "Pressure");
            Temperature = CreateName("/temp", "Temperature");
            KineticEnergy = CreateName("/kineticEnergy", "Kinetic Energy");
            PotentialEnergy = CreateName("/potentialEnergy", "Potential Energy");
            SimBoundingBox = CreateName("/box", "Simulation Box");
            TimeStep = CreateName("/time", "Time Step");
            PeriodicBox = CreateName("/periodicBox", "Periodic Box");
            SimulationTime = CreateName("/simulationTime", "Simulation Time");
            ComputationTime = CreateName("/computationTime", "Computation Time");
            SimulationName = CreateName("/simulationName", "Simulation Name");
            GradientScaleFactor = CreateName("/gradScaleFactor", "Gradient Scale Factor");
            Sigma = CreateName("/sigma", "Sigma");
        }

        /// <summary>
        /// Creates an instance of a variable name and add it to the global lookup.
        /// </summary>
        /// <param name="address">Address for the variable.</param>
        /// <param name="name">The friendly name of the variable.</param>
        /// <returns>A variable name.</returns>
        /// <exception cref="System.ArgumentException">address</exception>
        public static VariableName CreateName(string address, string name)
        {
            VariableName variableName = new VariableName(address, name);

            if (allNames.ContainsKey(variableName.HashCode) == true)
            {
                throw new ArgumentException(string.Format("A variable name with the address \"{0}\" already exists.", address), "address");
            }

            allNames.Add(variableName.HashCode, variableName);

            return variableName;
        }

        /// <summary>
        /// Gets the key hash code.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>The hash code for the address.</returns>
        /// <autogeneratedoc />
        public static int GetKeyHashCode(string address)
        {
            return address.GetHashCode();
        }

        /// <summary>
        /// Gets the variable name instance with the given address or throws an exception if none was found.
        /// </summary>
        /// <param name="address">A variable name address.</param>
        /// <returns>A variable name instance.</returns>
        /// <exception cref="System.ArgumentException">address</exception>
        public static VariableName GetName(string address)
        {
            VariableName name;

            if (allNames.TryGetValue(GetKeyHashCode(address), out name) == false)
            {
                throw new ArgumentException(string.Format("No variable name with the address \"{0}\" could be found.", address), "address");
            }

            return name;
        }

        /// <summary>
        /// Gets all variable names.
        /// </summary>
        /// <returns>VariableName[].</returns>
        public static VariableName[] GetNames()
        {
            return allNames.Values.ToArray();
        }

        #endregion Static Members

        /// <summary>
        /// Gets the OSC address of the variable, this is also used as the address.
        /// </summary>
        public readonly string Address;

        /// <summary>
        /// Gets the cached hash code.
        /// </summary>
        public readonly int HashCode;

        /// <summary>
        /// Gets the name of the variable.
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Creates a instance of a variable name.
        /// </summary>
        /// <param name="address">The address to use.</param>
        /// <param name="name">Friendly name of the variable.</param>
        /// <exception cref="System.ArgumentNullException">
        /// address
        /// or
        /// name
        /// </exception>
        /// <exception cref="System.ArgumentException">address</exception>
        internal VariableName(string address, string name)
        {
            if (string.IsNullOrEmpty(address) == true)
            {
                throw new ArgumentNullException("address");
            }

            if (OscAddress.IsValidAddressLiteral(address) == false)
            {
                throw new ArgumentException(string.Format("\"{0}\" is not a valid OSC literal address.", address), "address");
            }

            if (string.IsNullOrEmpty(name) == true)
            {
                throw new ArgumentNullException("name");
            }

            Address = address;
            Name = name;

            HashCode = GetKeyHashCode(Address);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        /// <autogeneratedoc />
        public override bool Equals(object obj)
        {
            switch (obj)
            {
                case VariableName variableName:
                    return Equals(variableName);
                case string @string:
                    return Equals(@string);
                case int i:
                    return Equals(i);
                default:
                    return false;
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="VariableName" /> is equal to this instance.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns><c>true</c> if the addresses of this instance and that of the other are equal, <c>false</c> otherwise.</returns>
        /// <autogeneratedoc />
        public bool Equals(VariableName other)
        {
            return Address.Equals(other.Address);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.String" /> is equal to this instance.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns><c>true</c> if the address of this instance and the value of the other are equal, <c>false</c> otherwise.</returns>
        /// <autogeneratedoc />
        public bool Equals(string other)
        {
            return Address.Equals(other);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Int32" /> is equal to this instance.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns><c>true</c> if the hash code of the address of this instance and the value of the other are equal; otherwise, <c>false</c>.</returns>
        /// <autogeneratedoc />
        public bool Equals(int other)
        {
            return HashCode == other;
        }

        /// <summary>
        /// Gets the cached hash code for the names address.
        /// </summary>
        /// <returns>A hash code for this variable name.</returns>
        public override int GetHashCode()
        {
            return HashCode;
        }

        /// <summary>
        /// Gets the name of the variable.
        /// </summary>
        /// <returns>A string form of the variable name.</returns>
        public override string ToString()
        {
            return Name;
        }
    }
}