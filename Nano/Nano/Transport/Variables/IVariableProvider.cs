﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Transport.Agents;

namespace Nano.Transport.Variables
{
    /// <summary>
    /// Interface IVariableProvider for providing instances of Variables.
    /// </summary>
    public interface IVariableProvider : IDisposable
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether the <see cref="IVariableProvider"/>  is initialised
        /// </summary>
        /// <value><c>true</c> if the IVariableProvider; otherwise, <c>false</c>.</value>
        bool VariablesInitialised { get; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Initialise IVariables and add them to the argument VariableManager
        /// </summary>
        /// <param name="variableManager">The variable manager.</param>
        void InitialiseVariables(VariableManager variableManager);

        /// <summary>
        /// Update output IVariables and broadcast values, overriding desired values
        /// </summary>
        void ResetVariableDesiredValues();

        /// <summary>
        /// Update input IVariables
        /// </summary>
        void UpdateVariableValues();

        #endregion Public Methods
    }
}