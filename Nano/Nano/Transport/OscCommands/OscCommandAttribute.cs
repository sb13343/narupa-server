﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.OSCCommands
{
    /// <summary>
    /// Attribute class for OSC Command methods.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class OscCommandAttribute : System.Attribute
    {
        /// <summary>
        /// The address of the command.
        /// </summary>
        public string Address;

        /// <summary>
        /// The example arguments string.
        /// </summary>
        public string ExampleArguments;

        /// <summary>
        /// The help message.
        /// </summary>
        public string Help;

        /// <summary>
        /// The OSC type tag string.
        /// </summary>
        public string TypeTagString = "";
    }
}