﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Streams
{

    /// <summary>
    /// Determines how streams should be handled when they are updated / flushed.
    /// </summary>
    public enum TransportDataStreamTransmitMode
    {
        /// <summary>
        /// Streams is in this mode the data will only be sent when requested by a network peer.
        /// </summary>
        /// <remarks>
        /// Streams in this mode are treated as <see cref="Comms.TransportPacketPriority.Critical"/>.
        /// </remarks>
        OnRequest,

        /// <summary>
        /// Streams is in this mode the data will be sent to listening network peers as soon as it is flushed. 
        /// </summary>
        /// <remarks>
        /// Streams in this mode are treated as <see cref="Comms.TransportPacketPriority.Critical"/>.
        /// </remarks>
        OnUpdate,

        /// <summary>
        /// Streams is in this mode the data will be sent to listening network peers as soon
        /// as it is flushed.
        /// </summary>
        /// <remarks>
        /// Streams in this mode are treated as <see cref="Comms.TransportPacketPriority.Normal"/>
        /// and as so will be amongst the first type of packet to be dropped when network
        /// congestion is detected.
        /// </remarks>
        Continuous,
    }
}
