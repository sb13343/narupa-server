﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Streams
{
    public interface IDataStream<Element>
    {
        int Count { get; set; }

        Element[] Data { get; }

        TransportStreamFormatDescriptor Format { get; }

        ushort ID { get; }

        int MaxCount { get; }

        int MaxSizeInBytes { get; }

        string Name { get; }

        void CopyFrom(byte[] bytes, ref int index);

        void CopyTo(byte[] bytes, ref int index);

        void Dispose();
    }
}