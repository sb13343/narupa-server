﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Rug.Osc;
using System;
using System.Collections.Generic;
using System.Net;

namespace Nano.Transport.Comms.ClientSocketImplementations
{
    public interface IClientSocketImplementation : IDisposable
    {
        /// <summary>
        /// Occurs when this instance is connected.
        /// </summary>
        event EventHandler Connected;

        /// <summary>
        /// Occurs when this instance is disconnected.
        /// </summary>
        event EventHandler Disconnected;

        /// <summary>
        /// Gets the state.
        /// </summary>
        /// <value>The state.</value>
        ConnectionState State { get; }

        /// <summary>
        /// Attaches this instance to a receiver.
        /// </summary>
        /// <param name="receiver">The receiver.</param>
        /// <exception cref="System.ArgumentNullException">receiver</exception>
        /// <exception cref="System.Exception"></exception>
        void AttachReceiver(ITransportPacketReceiver receiver);

        /// <summary>
        /// Broadcast a block of data to all receivers. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">Determines how this packet is treated. If it is critical then it wont be affected by packet choking.</param>
        /// <param name="packets">The packets.</param>
        /// <exception cref="System.Exception">Not connected</exception>
        void Broadcast(TransportPacketPriority priority, OscPacket[] packets);

        /// <summary>
        /// Broadcast a block of data to all receivers. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">Determines how this packet is treated. If it is critical then it wont be affected by packet choking.</param>
        /// <param name="data">Byte array.</param>
        /// <param name="index">Starting index in the array.</param>
        /// <param name="count">Number of bytes to send.</param>
        /// <exception cref="System.Exception">Not connected</exception>
        void Broadcast(TransportPacketPriority priority, byte[] data, int index, int count);

        void Flush();

        /// <summary>
        /// Connect to the server.
        /// </summary>
        /// <exception cref="System.Exception">
        /// </exception>
        void Start();

        /// <summary>
        /// Disconnect from server.
        /// </summary>
        void Stop();

        /// <summary>
        /// Transmits a block of data to a single endpoint. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">Determines how this packet is treated. If it is critical then it wont be affected by packet choking.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="packets">The packets.</param>
        void Transmit(TransportPacketPriority priority, IPEndPoint endpoint, OscPacket[] packets);

        /// <summary>
        /// Transmits one or more <see cref="Rug.Osc.OscPacket"/> to a set of client <see cref="IPEndPoint"/>s. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <param name="endpoints">The endpoints.</param>
        /// <param name="packets">The packets.</param>
        void Transmit(TransportPacketPriority priority, IEnumerable<IPEndPoint> endpoints, OscPacket[] packets);

        /// <summary>
        /// Transmits a block of data to a set of client <see cref="IPEndPoint"/>s. The data should be structured as a data packet.
        /// </summary>
        /// <param name="priority">Determines how this packet is treated. If it is critical then it wont be affected by packet choking.</param>
        /// <param name="data">Byte array.</param>
        /// <param name="index">Starting index in the array.</param>
        /// <param name="count">Number of bytes to send.</param>
        /// <param name="endpoints">Endpoints to send to.</param>
        void Transmit(TransportPacketPriority priority, byte[] data, int index, int count, IEnumerable<IPEndPoint> endpoints);
    }
}