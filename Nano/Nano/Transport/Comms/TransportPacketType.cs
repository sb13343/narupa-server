﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Comms
{
    /// <summary>
    /// Simbox protocol packet type.
    /// </summary>
    public enum TransportPacketType : ushort
    {
        /// <summary>
        /// Packet is an OSC bundle. 
        /// </summary>
        OscBundle = 0,

        /// <summary>
        /// Data packet id min value. 
        /// </summary>
        DataPacketMin = 1,

        /// <summary>
        /// Data packet id max value. 
        /// </summary>
        DataPacketMax = ushort.MaxValue, 
    }
}
