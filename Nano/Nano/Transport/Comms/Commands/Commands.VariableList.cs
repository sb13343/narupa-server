﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Linq;
using Nano.Transport.Variables;
using Rug.Osc;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// Client / server communication commands.
    /// </summary>
    public static partial class Commands
    {
        /// <summary>
        /// Holder for all the messages for the variable list commands.
        /// </summary>
        public static class VariableList
        {
            /// <summary>
            /// Create a request message for a list of variables.
            /// </summary>
            /// <returns>OscMessage.</returns>
            public static OscMessage Request()
            {
                return new OscMessage(GetAddress(Command.ListVariables), PackIdAndState(0, CommandState.Request));
            }

            /// <summary>
            /// Create the OSC messages for a list of variables success response command and the accompanying list items.
            /// </summary>
            /// <param name="variables">The collection of variables to send.</param>
            /// <returns>An array of OSC message.</returns>
            public static OscPacket[] Success(VariableCollection variables)
            {
                List<OscPacket> oscBuilder = new List<OscPacket>
                {
                    new OscMessage(GetAddress(Command.ListVariables), PackIdAndState(0, CommandState.ListHeader), variables.Count)
                };

                oscBuilder.AddRange(variables.Values.Select(variable => new OscMessage(GetAddress(Command.ListVariables), PackIdAndState(0, CommandState.ListItem), variable.Name.Name, variable.Name.Address, (byte) variable.VariableType)));

                return oscBuilder.ToArray();
            }

            /// <summary>
            /// Parse a  variables list command OSC message.
            /// </summary>
            /// <param name="message">OSC message to parse.</param>
            /// <param name="id">Outputs the ID of the stream.</param>
            /// <param name="state">Outputs the state of the command.</param>
            /// <param name="error">Outputs the error string of the command if it failed.</param>
            public static void Parse(OscMessage message, out ushort id, out CommandState state, out string error)
            {
                ParseStreamCommand(message, out id, out state, out error);
            }

            /// <summary>
            /// Parse a variable list header from an OSC message.
            /// </summary>
            /// <param name="message">An OSC message.</param>
            /// <param name="id">Outputs the id of the command.</param>
            /// <param name="state">Outputs the command state of the command.</param>
            /// <param name="numberOfItems">Outputs the number of items in the list.</param>
            /// <exception cref="SimboxException">
            /// </exception>
            public static void ParseHeader(OscMessage message, out ushort id, out CommandState state, out int numberOfItems)
            {
                numberOfItems = 0;

                Parse(message, out id, out state, out string error);

                if (state != CommandState.ListHeader)
                {
                    return;
                }

                if (message.Count < 2)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedNumberOfArguments);
                }

                if ((message[1] is int) == false)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, string.Format(Strings.Error_Argument_WrongType, 1));
                }

                numberOfItems = (int)message[1];
            }

            /// <summary>
            /// Parse a list item from an OSC message.
            /// </summary>
            /// <param name="message">An OSC message.</param>
            /// <param name="id">Outputs the id of the variable.</param>
            /// <param name="name">Outputs the name of the variable.</param>
            /// <param name="address">Outputs the name of the variable.</param>
            /// <param name="type">Outputs the variable type of the variable.</param>
            /// <exception cref="SimboxException">
            /// </exception>
            public static void ParseListItem(OscMessage message, out ushort id, out string name, out string address, out VariableType type)
            {
                CommandState state;
                string error;
                Parse(message, out id, out state, out error);

                if (state == CommandState.Failed)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedCommandState, Strings.Command_UnexpectedCommandState_ListItem);
                }

                if (message.Count < 4)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedNumberOfArguments);
                }

                if ((message[1] is string) == false)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, string.Format(Strings.Error_Argument_WrongType, 1));
                }

                if ((message[2] is string) == false)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, string.Format(Strings.Error_Argument_WrongType, 2));
                }

                if ((message[3] is byte) == false)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, string.Format(Strings.Error_Argument_WrongType, 3));
                }

                name = (string)message[1];
                address = (string)message[2];
                type = (VariableType)(byte)message[3];
            }
        }
    }
}
