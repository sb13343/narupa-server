﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Net;

namespace Nano.Transport.Comms.ServerSocketImplementations
{
    //public delegate void SocketSendPacket(byte[] buffer, int index, int count);

    //public delegate void SocketSendPacket(byte[] buffer, int index, int count);
    //public delegate void SocketStateChanged(ClientSessionState state);

    internal interface IServerSocketImplementation : IDisposable
    {
        /// <summary>
        /// Occurs when the session has ended.
        /// </summary>
        event EventHandler Ended;

        /// <summary>
        /// Occurs when the session begins.
        /// </summary>
        event EventHandler Started;

        /// <summary>
        /// The IP end point of the client.
        /// </summary>
        IPEndPoint EndPoint { get; }

        /// <summary>
        /// Flushes the buffer is BufferOutput is <c>true</c>.
        /// </summary>
        /// <returns><c>true</c> if flush was successful, <c>false</c> otherwise.</returns>
        bool Flush();

        /// <summary>
        /// Sends a packet using the specified priority.
        /// </summary>
        /// <param name="priority">The priority.</param>
        /// <param name="data">The data.</param>
        /// <param name="index">The index.</param>
        /// <param name="count">The count.</param>
        /// <returns><c>true</c> if the packet was queued; otherwise, <c>false</c>.</returns>
        /// <exception cref="System.ArgumentNullException">data</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// index
        /// or
        /// count
        /// </exception>
        bool Send(TransportPacketPriority priority, byte[] data, int index, int count);

        /// <summary>
        /// Starts this instance.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops this instance.
        /// </summary>
        void Stop();
    }
}