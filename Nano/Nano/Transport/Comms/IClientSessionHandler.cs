﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Net;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// A handler for client sessions.
    /// </summary>
    public interface IClientSessionHandler 
    {
        /// <summary>
        /// Called when a client session has started.
        /// </summary>
        /// <param name="endpoint">The IP end point of the client.</param>
        void OnSessionStarted(IPEndPoint endpoint);

        /// <summary>
        /// Called when a client session has ended.
        /// </summary>
        /// <param name="endpoint">The IP end point of the client.</param>
        void OnSessionEnded(IPEndPoint endpoint);
    }
}
