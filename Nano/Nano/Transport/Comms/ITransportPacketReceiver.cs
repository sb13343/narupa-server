﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Net;
using Rug.Osc;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// Interface for receiving packets.
    /// </summary>
    public interface ITransportPacketReceiver
    {
        /// <summary>
        /// Called when an OSC bundle is received.
        /// </summary>
        /// <param name="origin">The IP end point the is the origin of the OSC bundle.</param>
        /// <param name="bundle">An OSC bundle.</param>
        void OnOscPacket(IPEndPoint origin, OscBundle bundle);

        /// <summary>
        /// Called when an data packet is received.
        /// </summary>
        /// <param name="origin">The IP end point the is the origin of the data packet.</param>
        /// <param name="id">The id of the stream.</param>
        /// <param name="data">Byte array.</param>
        /// <param name="index">Starting index within the array.</param>
        /// <param name="length">Length of the data packet.</param>
        void OnDataPacket(IPEndPoint origin, ushort id, byte[] data, int index, int length);
    }
}
