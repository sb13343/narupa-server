﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections;
using System.Collections.Generic;
using SlimMath;

namespace Nano
{
    /// <summary>
    /// Mathematical utility functions
    /// </summary>
    public static class MathUtilities
    {
        #region Public Fields

        /// <summary>
        /// The Random instance.
        /// </summary>
        public static Random Rand = new Random();

        #endregion Public Fields

        #region Public Methods

        /// <summary>
        /// Calculates volume of bounding box
        /// </summary>
        /// <param name="box">The box.</param>
        /// <returns>System.Single.</returns>
        public static float CalculateVolume(BoundingBox box)
        {
            Vector3 diff = (box.Maximum - box.Minimum);
            return diff.X * diff.Y * diff.Z;
        }

        /// <summary>
        /// Computes an approximate exponentiation.
        /// </summary>
        /// <param name="x">Value to be exponentiated.</param>
        /// <returns>System.Single.</returns>
        /// <remarks>
        /// WARNING: May not be portable.
        /// Source: http://stackoverflow.com/questions/412019/math-optimization-in-c-sharp/412988#412988
        /// </remarks>
        public static float ComputeFastExp(float x)
        {
            long tmp = (long)(1512775 * x + (1072693248 - 60801));
            return (float)BitConverter.Int64BitsToDouble(tmp << 32);
        }

        /// <summary>
        /// Computes the absolute value of a float.
        /// </summary>
        /// <param name="x"> x</param>
        /// <returns>System.Single.</returns>
        public static float AbsF(float x)
        {
            return x > 0f ? x : -x;
        }

        /// <summary>
        /// Creates a zero-centred cubic bounding box.
        /// </summary>
        /// <param name="volume">The volume of the desired box.</param>
        /// <returns>BoundingBox.</returns>
        public static BoundingBox CreateCube(float volume)
        {
            Vector3 size = new Vector3((float)Math.Pow(volume, 1f / 3f) / 2);

            return new BoundingBox(-size, size);
        }

        /// <summary>
        /// Generates a value sampled from a Gaussian distribution
        /// with mean mu and std sigma.
        /// </summary>
        /// <param name="mu">Mean</param>
        /// <param name="sigma">Sigma</param>
        /// <returns></returns>
        public static float GenerateGaussianNoise(float mu, float sigma)
        {
            const float epsilon = float.Epsilon;
            const float two_pi = 2.0f * (float)Math.PI;
            float u1, u2;

            //Alternate between generating a new number
            //and returning the second value generated last
            //time this method was called
            generate = !generate;
            if (!generate)
            {
                return z1 * sigma + mu;
            }
            //Ensure that u1 and u2 are not 0
            do
            {
                u1 = (float)Rand.NextDouble();
                u2 = (float)Rand.NextDouble();
            } while (u1 <= epsilon);

            z0 = (float)Math.Sqrt(-2.0f * Math.Log(u1)) * (float)Math.Cos(two_pi * u2);
            z1 = (float)Math.Sqrt(-2.0f * Math.Log(u1)) * (float)Math.Sin(two_pi * u2);

            return z0 * sigma + mu;
        }

        /// <summary>
        /// Generates a random normal vector.
        /// </summary>
        /// <returns>Vector3.</returns>
        public static Vector3 GenerateRandomNormalVector()
        {
            Vector3 v = new Vector3();
            for (int j = 0; j < 3; j++)
            {
                v[j] = GenerateGaussianNoise(0f, 1f);
            }
            v = v / v.Length;
            return v;
        }

        public static List<double> GenerateListOfGaussianNoise(int count)
        {
            List<double> gaussianNoise = new List<double>(count);
            while (gaussianNoise.Count < count)
            {
                double x, y, rSqr;
                do
                {
                    x = 2.0 * Rand.NextDouble() - 1.0;
                    y = 2.0 * Rand.NextDouble() - 1.0;
                    rSqr = x * x + y * y;
                } while (rSqr >= 1.0 || rSqr == 0.0);
                double multiplier = Math.Sqrt((-2.0 * Math.Log(rSqr)) / rSqr);
                gaussianNoise.Add(x * multiplier);
                gaussianNoise.Add(y * multiplier);
            }
            return gaussianNoise;
        }

        /// <summary>
        /// Generates a random quartnernion.
        /// </summary>
        /// <remarks>
        /// Code snippet from http://planning.cs.uiuc.edu/node198.html that generates a randomly sampled quaternion
        /// </remarks>
        /// <returns>Quaternion.</returns>
        public static Quaternion GenerateRandomQuarternion()
        {
            double u1 = Rand.NextDouble();
            double u2 = Rand.NextDouble();
            double u3 = Rand.NextDouble();

            double u1sqrt = Math.Sqrt(u1);
            double u1m1sqrt = Math.Sqrt(1 - u1);
            double x = u1m1sqrt * Math.Sin(2 * Math.PI * u2);
            double y = u1m1sqrt * Math.Cos(2 * Math.PI * u2);
            double z = u1sqrt * Math.Sin(2 * Math.PI * u3);
            double w = u1sqrt * Math.Cos(2 * Math.PI * u3);

            return new Quaternion((float)x, (float)y, (float)w, (float)z);
        }

        /// <summary>
        /// Convert from Euler angles to quaternions.
        /// </summary>
        /// <param name="eulerAngles"></param>
        /// <param name="radians">Whether the passed angles are in radians or degrees.</param>
        /// <returns>Quaternion.</returns>
        /// <remarks>
        /// Rotation follows the Tait-Bryan order, like a plane taking off: yaw (Z), pitch, (Y), then roll (X).
        /// </remarks>
        public static Quaternion QuaternionFromEuler(Vector3 eulerAngles, bool radians = true)
        {
            return QuaternionFromEuler(eulerAngles.X, eulerAngles.Y, eulerAngles.Z, radians);
        }

        /// <summary>
        /// Convert from Euler angles
        /// </summary>
        /// <param name="pitch"></param>
        /// <param name="yaw"></param>
        /// <param name="roll"></param>
        /// <param name="radians">Whether the passed angles are in radians or degrees.</param>
        /// <returns></returns>
        public static Quaternion QuaternionFromEuler(double roll, double pitch, double yaw, bool radians = true)
        {
            if (radians == false)
            {
                pitch = RadFromDegree(pitch);
                yaw = RadFromDegree(yaw);
                roll = RadFromDegree(roll);
            }
            // Abbreviations for the various angular functions
            var cy = Math.Cos(yaw * 0.5);
            var sy = Math.Sin(yaw * 0.5);
            var cp = Math.Cos(pitch * 0.5);
            var sp = Math.Sin(pitch * 0.5);
            var cr = Math.Cos(roll * 0.5);
            var sr = Math.Sin(roll * 0.5);

            Quaternion q;
            q.W = (float) (cy * cp * cr + sy * sp * sr);
            q.X = (float) (cy * cp * sr - sy * sp * cr);
            q.Y = (float) (sy * cp * sr + cy * sp * cr);
            q.Z = (float) (sy * cp * cr - cy * sp * sr);
            return q;
        }

        private static double RadFromDegree(double angle)
        {
            return Math.PI / 180f * angle;
        }

        /// <summary>
        /// Gets the center of a collection of vectors.
        /// </summary>
        /// <param name="vectors">The vectors.</param>
        /// <returns>Vector3.</returns>
        public static Vector3 GetCenter(ICollection<Vector3> vectors)
        {
            Vector3 centre = Vector3.Zero;
            foreach (Vector3 v in vectors)
            {
                centre += v;
            }
            centre /= vectors.Count;
            return centre;
        }

        /// <summary>
        /// Centers a collection of vectors the about zero.
        /// </summary>
        /// <param name="vectors">The vectors.</param>
        /// <returns>ICollection&lt;Vector3&gt;.</returns>
        public static ICollection<Vector3> CenterAboutZero(ICollection<Vector3> vectors)
        {
            List<Vector3> centred = new List<Vector3>();
            Vector3 centre = GetCenter(vectors);
            foreach (Vector3 vector in vectors)
            {
                centred.Add(vector - centre);
            }
            return centred;
        }

        public static ICollection<Vector3> CorrectDistanceUnits(ICollection<Vector3> vectors, float factor)
        {
            List<Vector3> corrected = new List<Vector3>();
            foreach (Vector3 vector in vectors)
            {
                corrected.Add(new Vector3(vector.X * factor, vector.Y * factor, vector.Z * factor));
            }
            return corrected;
        }

        /// <summary>
        /// Gets the nearest vector in the collection to the passed vector.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <param name="vectors">The vectors.</param>
        /// <param name="cutoff">The cutoff.</param>
        /// <returns>Vector3.</returns>
        public static Vector3 GetNearestVector(Vector3 vector, IEnumerable vectors, float cutoff = 0.5f)
        {
            float minDist2 = cutoff * cutoff;
            float dist = minDist2;

            Vector3 closest = Vector3.Zero;
            foreach (Vector3 v in vectors)
            {
                dist = (vector - v).LengthSquared;
                if (dist < minDist2)
                {
                    minDist2 = dist;
                    closest = v;
                }
            }
            return closest;
        }

        #endregion Public Methods

        #region Private Fields

        private static bool generate = false;
        private static float z0;
        private static float z1;

        /// <summary>
        /// Normalizes a vector between 0,1 in each dimension with respect to the simulation Box.
        /// </summary>
        /// <param name="vector3"></param>
        /// <param name="simulationBox"></param>
        /// <returns></returns>
        public static Vector3 NormalizeVector(Vector3 vector3, BoundingBox simulationBox)
        {
            Vector3 v = vector3;
            Vector3 boxLengths = simulationBox.Maximum - simulationBox.Minimum;
            for (int i = 0; i < 3; i++)
            {
                v[i] = v[i] / boxLengths[i] + 0.5f;
            }
            return v;
        }

        #endregion Private Fields

        /// <summary>
        /// Generates the Euler angles from a quaternion.
        /// </summary>
        /// <param name="rotation">Quaternion representing rotation.</param>
        /// <param name="radians">Whether to output the Euler angles in radians.</param>
        /// <returns></returns>
        public static Vector3 EulerFromQuaternion(Quaternion rotation, bool radians=true)
        {
            
            // roll (x-axis rotation)
            var sinrCosp = +2.0 * (rotation.W * rotation.X + rotation.Y * rotation.Z);
            var cosrCosp = +1.0 - 2.0 * (rotation.X * rotation.X + rotation.Y * rotation.Y);
            var roll = Math.Atan2(sinrCosp, cosrCosp);

            // pitch (y-axis rotation)
            var sinp = +2.0 * (rotation.W * rotation.Y - rotation.Z * rotation.X);
            double pitch;
            if (Math.Abs(sinp) >= 1)
                pitch = Math.Sign(sinp) * Math.PI; // use 90 degrees if out of range
            else
                pitch = Math.Asin(sinp);

            // yaw (z-axis rotation)
            var sinyCosp = +2.0 * (rotation.W * rotation.Z + rotation.X * rotation.Y);
            var cosyCosp = +1.0 - 2.0 * (rotation.Y * rotation.Y + rotation.Z * rotation.Z);  
            var yaw = Math.Atan2(sinyCosp, cosyCosp);
            var resultRad = new Vector3((float) roll, (float) pitch, (float) yaw);
            if (radians == false)
                resultRad *= (float) (180f / Math.PI);
            return resultRad;
        }
    }
}