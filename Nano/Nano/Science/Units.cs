﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Science
{
    /// <summary>
    /// Class Units containing conversions between common units.
    /// </summary>
    /// <remarks>
    /// In nano Simbox, we use the following units for everything, following the convention laid down by OpenMM.
    ///
    /// distance: nm
    /// time: ps
    /// mass: atomic mass units
    /// charge: proton charge
    /// temperature: Kelvin
    /// angle: radians
    /// energy: kJ/mol
    ///
    /// To quote from the OpenMM user guide:
    ///
    /// "These units have the important feature that they form an internally consistent set.
    /// For example, a force always has the same units (kJ/mol/nm)
    /// whether it is calculated as the gradient of an energy or as the product of a mass and an acceleration.
    /// This is not true in some other widely used unit systems, such as those that express energy in kcal/mol."
    ///
    ///
    /// </remarks>
    public static class Units
    {
        #region Mass

        /// <summary>Convert from AMU to KG.</summary>
        public const float KgPerAmu = 1.0f / (PhysicalConstants.AvogadroC * 1.0e3f);

        #endregion Mass

        #region Energy

        /// <summary>Convert Calorie to Joule.</summary>
        public const float KJPerKCal = 4.184f;

        /// <summary>
        /// Convert from Joule to Calorie.
        /// </summary>
        public const float KCalPerKJ = 1.0f / KJPerKCal;

        #endregion Energy

        #region Space

        /// <summary>
        /// Convert from nanometers (nm) to Angstroms.
        /// </summary>
        public const float AngstromsPerNm = 10f;

        /// <summary>
        /// Convert from Angstroms (A) to nanometers (nm).
        /// </summary>
        public const float NmPerAngstrom = 1.0f / AngstromsPerNm;

		/// <summary>
		/// Convert from nanometers (nm) to Bohr.
		/// </summary>
		public const float BohrPerNm = 18.89725989f;

		/// <summary>
		/// Convert from Bohr to nanometers (nm).
		/// </summary>
		public const float NmPerBohr = 1.0f / BohrPerNm;

        /// <summary>
        /// Convert from nanometers to meters.
        /// </summary>
        public const float MPerNm = 1e-09f;

        /// <summary>
        /// Convert from meters to nanometers.
        /// </summary>
        public const float NmPerM = 1.0f / MPerNm;

        #endregion Space

        #region Time

        /// <summary>
        /// Convert from femtoseconds (fs) to picoseconds (ps).
        /// </summary>
        public const float PsPerFs = 0.001f;

        /// <summary>
        /// Convert from picoseconds (ps) to femtoseconds (fs).
        /// </summary>
        public const float FsPerPs = 1.0f / PsPerFs;

        /// <summary>
        /// Convert from seconds (s) to picoseconds (ps).
        /// </summary>
        public const float PsPerS = 1e12f;

        /// <summary>
        /// Convert from picoseconds (ps) to seconds (s).
        /// </summary>
        public const float SPerPs = 1.0f / PsPerS;

        #endregion Time

        #region Force

        /// <summary>
        /// Convert from kJ/(nm*mol) to Newtons
        /// </summary>
        public const float NewtonsPerKJPerNmMol = 1.660539e-12f;

        #endregion Force

        #region Pressure

        /// <summary>
        /// Convert from pascal to bar.
        /// </summary>
        public const float BarsPerPascal = 1e-05f;

        /// <summary>
        /// Convert from bar to pascal.
        /// </summary>
        public const float PascalsPerBar = 1.0f / BarsPerPascal;

        /// <summary>
        /// Convert from atm to bar
        /// </summary>
        public const float BarsPerAtm = 1.0f / AtmPerBars;

        /// <summary>
        /// Convert from bar to atm
        /// </summary>
        public const float AtmPerBars = 0.98692316931f;

        /// <summary>
        /// Convert from SI units (N/nm^2 = kg*s^-2/nm = kJ/nm^3) to Bar
        /// </summary>
        public const float BarPerSIUnits = 0.980665e-25f;

        /// <summary>
        /// Convert from SI units (N/nm^2 = kg*s^-2/nm = kJ/nm^3) to Pascal
        /// </summary>
        public const float PascalPerSIUnits = 9.80665e-21f;

        /// <summary>
        /// Convert from SI units (N/nm^2 = kg*s^-2/nm = kJ/nm^3) to Atm
        /// </summary>
        public const float AtmPerSIUnits = 0.96784100984e-25f;

        #endregion Pressure
    }
}
