/* THIS FILE WAS GENERATED AUTOMATICALLY. DO NOT MODIFY. */ 

namespace Nano.Science
{
	public enum Element : ushort
	{
		/// <summary> Virtual (0)</summary>
		 Virtual = 0,
		
		/// <summary> Hydrogen (1)</summary>
		 Hydrogen = 1,
		
		/// <summary> Helium (2)</summary>
		 Helium = 2,
		
		/// <summary> Lithium (3)</summary>
		 Lithium = 3,
		
		/// <summary> Beryllium (4)</summary>
		 Beryllium = 4,
		
		/// <summary> Boron (5)</summary>
		 Boron = 5,
		
		/// <summary> Carbon (6)</summary>
		 Carbon = 6,
		
		/// <summary> Nitrogen (7)</summary>
		 Nitrogen = 7,
		
		/// <summary> Oxygen (8)</summary>
		 Oxygen = 8,
		
		/// <summary> Fluorine (9)</summary>
		 Fluorine = 9,
		
		/// <summary> Neon (10)</summary>
		 Neon = 10,
		
		/// <summary> Sodium (11)</summary>
		 Sodium = 11,
		
		/// <summary> Magnesium (12)</summary>
		 Magnesium = 12,
		
		/// <summary> Aluminium (13)</summary>
		 Aluminium = 13,
		
		/// <summary> Silicon (14)</summary>
		 Silicon = 14,
		
		/// <summary> Phosphorus (15)</summary>
		 Phosphorus = 15,
		
		/// <summary> Sulfur (16)</summary>
		 Sulfur = 16,
		
		/// <summary> Chlorine (17)</summary>
		 Chlorine = 17,
		
		/// <summary> Argon (18)</summary>
		 Argon = 18,
		
		/// <summary> Potassium (19)</summary>
		 Potassium = 19,
		
		/// <summary> Calcium (20)</summary>
		 Calcium = 20,
		
		/// <summary> Scandium (21)</summary>
		 Scandium = 21,
		
		/// <summary> Titanium (22)</summary>
		 Titanium = 22,
		
		/// <summary> Vanadium (23)</summary>
		 Vanadium = 23,
		
		/// <summary> Chromium (24)</summary>
		 Chromium = 24,
		
		/// <summary> Manganese (25)</summary>
		 Manganese = 25,
		
		/// <summary> Iron (26)</summary>
		 Iron = 26,
		
		/// <summary> Cobalt (27)</summary>
		 Cobalt = 27,
		
		/// <summary> Nickel (28)</summary>
		 Nickel = 28,
		
		/// <summary> Copper (29)</summary>
		 Copper = 29,
		
		/// <summary> Zinc (30)</summary>
		 Zinc = 30,
		
		/// <summary> Gallium (31)</summary>
		 Gallium = 31,
		
		/// <summary> Germanium (32)</summary>
		 Germanium = 32,
		
		/// <summary> Arsenic (33)</summary>
		 Arsenic = 33,
		
		/// <summary> Selenium (34)</summary>
		 Selenium = 34,
		
		/// <summary> Bromine (35)</summary>
		 Bromine = 35,
		
		/// <summary> Krypton (36)</summary>
		 Krypton = 36,
		
		/// <summary> Rubidium (37)</summary>
		 Rubidium = 37,
		
		/// <summary> Strontium (38)</summary>
		 Strontium = 38,
		
		/// <summary> Yttrium (39)</summary>
		 Yttrium = 39,
		
		/// <summary> Zirconium (40)</summary>
		 Zirconium = 40,
		
		/// <summary> Niobium (41)</summary>
		 Niobium = 41,
		
		/// <summary> Molybdenum (42)</summary>
		 Molybdenum = 42,
		
		/// <summary> Technetium (43)</summary>
		 Technetium = 43,
		
		/// <summary> Ruthenium (44)</summary>
		 Ruthenium = 44,
		
		/// <summary> Rhodium (45)</summary>
		 Rhodium = 45,
		
		/// <summary> Palladium (46)</summary>
		 Palladium = 46,
		
		/// <summary> Silver (47)</summary>
		 Silver = 47,
		
		/// <summary> Cadmium (48)</summary>
		 Cadmium = 48,
		
		/// <summary> Indium (49)</summary>
		 Indium = 49,
		
		/// <summary> Tin (50)</summary>
		 Tin = 50,
		
		/// <summary> Antimony (51)</summary>
		 Antimony = 51,
		
		/// <summary> Tellurium (52)</summary>
		 Tellurium = 52,
		
		/// <summary> Iodine (53)</summary>
		 Iodine = 53,
		
		/// <summary> Xenon (54)</summary>
		 Xenon = 54,
		
		/// <summary> Cesium (55)</summary>
		 Cesium = 55,
		
		/// <summary> Barium (56)</summary>
		 Barium = 56,
		
		/// <summary> Lanthanum (57)</summary>
		 Lanthanum = 57,
		
		/// <summary> Cerium (58)</summary>
		 Cerium = 58,
		
		/// <summary> Praseodymium (59)</summary>
		 Praseodymium = 59,
		
		/// <summary> Neodymium (60)</summary>
		 Neodymium = 60,
		
		/// <summary> Promethium (61)</summary>
		 Promethium = 61,
		
		/// <summary> Samarium (62)</summary>
		 Samarium = 62,
		
		/// <summary> Europium (63)</summary>
		 Europium = 63,
		
		/// <summary> Gadolinium (64)</summary>
		 Gadolinium = 64,
		
		/// <summary> Terbium (65)</summary>
		 Terbium = 65,
		
		/// <summary> Dysprosium (66)</summary>
		 Dysprosium = 66,
		
		/// <summary> Holmium (67)</summary>
		 Holmium = 67,
		
		/// <summary> Erbium (68)</summary>
		 Erbium = 68,
		
		/// <summary> Thulium (69)</summary>
		 Thulium = 69,
		
		/// <summary> Ytterbium (70)</summary>
		 Ytterbium = 70,
		
		/// <summary> Lutetium (71)</summary>
		 Lutetium = 71,
		
		/// <summary> Hafnium (72)</summary>
		 Hafnium = 72,
		
		/// <summary> Tantalum (73)</summary>
		 Tantalum = 73,
		
		/// <summary> Tungsten (74)</summary>
		 Tungsten = 74,
		
		/// <summary> Rhenium (75)</summary>
		 Rhenium = 75,
		
		/// <summary> Osmium (76)</summary>
		 Osmium = 76,
		
		/// <summary> Iridium (77)</summary>
		 Iridium = 77,
		
		/// <summary> Platinum (78)</summary>
		 Platinum = 78,
		
		/// <summary> Gold (79)</summary>
		 Gold = 79,
		
		/// <summary> Mercury (80)</summary>
		 Mercury = 80,
		
		/// <summary> Thallium (81)</summary>
		 Thallium = 81,
		
		/// <summary> Lead (82)</summary>
		 Lead = 82,
		
		/// <summary> Bismuth (83)</summary>
		 Bismuth = 83,
		
		/// <summary> Polonium (84)</summary>
		 Polonium = 84,
		
		/// <summary> Astatine (85)</summary>
		 Astatine = 85,
		
		/// <summary> Radon (86)</summary>
		 Radon = 86,
		
		/// <summary> Francium (87)</summary>
		 Francium = 87,
		
		/// <summary> Radium (88)</summary>
		 Radium = 88,
		
		/// <summary> Actinium (89)</summary>
		 Actinium = 89,
		
		/// <summary> Thorium (90)</summary>
		 Thorium = 90,
		
		/// <summary> Protactinium (91)</summary>
		 Protactinium = 91,
		
		/// <summary> Uranium (92)</summary>
		 Uranium = 92,
		
		/// <summary> Neptunium (93)</summary>
		 Neptunium = 93,
		
		/// <summary> Plutonium (94)</summary>
		 Plutonium = 94,
		
		/// <summary> Americium (95)</summary>
		 Americium = 95,
		
		/// <summary> Curium (96)</summary>
		 Curium = 96,
		
		/// <summary> Berkelium (97)</summary>
		 Berkelium = 97,
		
		/// <summary> Californium (98)</summary>
		 Californium = 98,
		
		/// <summary> Einsteinium (99)</summary>
		 Einsteinium = 99,
		
		/// <summary> Fermium (100)</summary>
		 Fermium = 100,
		
		/// <summary> Mendelevium (101)</summary>
		 Mendelevium = 101,
		
		/// <summary> Nobelium (102)</summary>
		 Nobelium = 102,
		
		/// <summary> Lawrencium (103)</summary>
		 Lawrencium = 103,
		
		/// <summary> Rutherfordium (104)</summary>
		 Rutherfordium = 104,
		
		/// <summary> Dubnium (105)</summary>
		 Dubnium = 105,
		
		/// <summary> Seaborgium (106)</summary>
		 Seaborgium = 106,
		
		/// <summary> Bohrium (107)</summary>
		 Bohrium = 107,
		
		/// <summary> Hassium (108)</summary>
		 Hassium = 108,
		
		/// <summary> Meitnerium (109)</summary>
		 Meitnerium = 109,
		
		/// <summary> Darmstadtium (110)</summary>
		 Darmstadtium = 110,
		
		/// <summary> Roentgenium (111)</summary>
		 Roentgenium = 111,
		
		/// <summary> Copernicium (112)</summary>
		 Copernicium = 112,
		
		/// <summary> Ununtrium (113)</summary>
		 Ununtrium = 113,
		
		/// <summary> Ununquadium (114)</summary>
		 Ununquadium = 114,
		
		/// <summary> Ununpentium (115)</summary>
		 Ununpentium = 115,
		
		/// <summary> Ununhexium (116)</summary>
		 Ununhexium = 116,
		
		/// <summary> Ununseptium (117)</summary>
		 Ununseptium = 117,
		
		/// <summary>MAX (118)</summary>
		MAX = 118,    		
	}

    public static partial class PeriodicTable
    {
		/// <summary> Virtual (0)</summary>
		public static readonly ElementProperties Virtual = new ElementProperties(0, "V", "Virtual", 0, "000000", "", 0f, 0.037f, 0f, 0.12f, 0f, 0f, "", "", "", 0f, 0f, 0f, "nonmetal", "Unknown");
		
		/// <summary> Hydrogen (1)</summary>
		public static readonly ElementProperties Hydrogen = new ElementProperties(1, "H", "Hydrogen", 1.00794, "FFFFFF", "1s1", 2.2f, 0.037f, 0f, 0.12f, 1312f, -73f, "gas", "-1, 1", "diatomic", 14f, 20f, 8.99E-05f, "nonmetal", "1766");
		
		/// <summary> Helium (2)</summary>
		public static readonly ElementProperties Helium = new ElementProperties(2, "He", "Helium", 4.002602, "FF6633", "1s2", 0f, 0.032f, 0f, 0.14f, 2372f, 0f, "gas", "", "atomic", 0f, 4f, 0f, "noble gas", "1868");
		
		/// <summary> Lithium (3)</summary>
		public static readonly ElementProperties Lithium = new ElementProperties(3, "Li", "Lithium", 6.941, "CC80FF", "[He] 2s1", 0.98f, 0.134f, 0.07600001f, 0.182f, 520f, -60f, "solid", "1", "metallic", 454f, 1615f, 0.54f, "alkali metal", "1817");
		
		/// <summary> Beryllium (4)</summary>
		public static readonly ElementProperties Beryllium = new ElementProperties(4, "Be", "Beryllium", 9.012182, "C2FF00", "[He] 2s2", 1.57f, 0.09f, 0.045f, 0f, 900f, 0f, "solid", "2", "metallic", 1560f, 2743f, 1.85f, "alkaline earth metal", "1798");
		
		/// <summary> Boron (5)</summary>
		public static readonly ElementProperties Boron = new ElementProperties(5, "B", "Boron", 10.811, "FFB5B5", "[He] 2s2 2p1", 2.04f, 0.082f, 0.027f, 0f, 801f, -27f, "solid", "1, 2, 3", "covalent network", 2348f, 4273f, 2.46f, "metalloid", "1807");
		
		/// <summary> Carbon (6)</summary>
		public static readonly ElementProperties Carbon = new ElementProperties(6, "C", "Carbon", 12.0107, "909090", "[He] 2s2 2p2", 2.55f, 0.07700001f, 0.016f, 0.17f, 1087f, -154f, "solid", "-4, -3, -2, -1, 1, 2, 3, 4", "covalent network", 3823f, 4300f, 2.26f, "nonmetal", "Ancient");
		
		/// <summary> Nitrogen (7)</summary>
		public static readonly ElementProperties Nitrogen = new ElementProperties(7, "N", "Nitrogen", 14.0067, "3050F8", "[He] 2s2 2p3", 3.04f, 0.075f, 0.146f, 0.155f, 1402f, -7f, "gas", "-3, -2, -1, 1, 2, 3, 4, 5", "diatomic", 63f, 77f, 0f, "nonmetal", "1772");
		
		/// <summary> Oxygen (8)</summary>
		public static readonly ElementProperties Oxygen = new ElementProperties(8, "O", "Oxygen", 15.9994, "FF0D0D", "[He] 2s2 2p4", 3.44f, 0.07300001f, 0.14f, 0.152f, 1314f, -141f, "gas", "-2, -1, 1, 2", "diatomic", 55f, 90f, 0f, "nonmetal", "1774");
		
		/// <summary> Fluorine (9)</summary>
		public static readonly ElementProperties Fluorine = new ElementProperties(9, "F", "Fluorine", 18.9984, "1FF01F", "[He] 2s2 2p5", 3.98f, 0.071f, 0.133f, 0.147f, 1681f, -328f, "gas", "-1", "atomic", 54f, 85f, 0f, "halogen", "1670");
		
		/// <summary> Neon (10)</summary>
		public static readonly ElementProperties Neon = new ElementProperties(10, "Ne", "Neon", 20.1797, "3399FF", "[He] 2s2 2p6", 0f, 0.06900001f, 0f, 0.154f, 2081f, 0f, "gas", "", "atomic", 25f, 27f, 0f, "noble gas", "1898");
		
		/// <summary> Sodium (11)</summary>
		public static readonly ElementProperties Sodium = new ElementProperties(11, "Na", "Sodium", 22.98977, "AB5CF2", "[Ne] 3s1", 0.93f, 0.154f, 0.102f, 0.227f, 496f, -53f, "solid", "-1, 1", "metallic", 371f, 1156f, 0.97f, "alkali metal", "1807");
		
		/// <summary> Magnesium (12)</summary>
		public static readonly ElementProperties Magnesium = new ElementProperties(12, "Mg", "Magnesium", 24.305, "8AFF00", "[Ne] 3s2", 1.31f, 0.13f, 0.072f, 0.173f, 738f, 0f, "solid", "1, 2", "metallic", 923f, 1363f, 1.74f, "alkaline earth metal", "1808");
		
		/// <summary> Aluminium (13)</summary>
		public static readonly ElementProperties Aluminium = new ElementProperties(13, "Al", "Aluminium", 26.98154, "BFA6A6", "[Ne] 3s2 3p1", 1.61f, 0.118f, 0.0535f, 0.184f, 578f, -43f, "solid", "1, 3", "metallic", 933f, 2792f, 2.7f, "metal", "Ancient");
		
		/// <summary> Silicon (14)</summary>
		public static readonly ElementProperties Silicon = new ElementProperties(14, "Si", "Silicon", 28.0855, "F0C8A0", "[Ne] 3s2 3p2", 1.9f, 0.111f, 0.04f, 0.21f, 787f, -134f, "solid", "-4, -3, -2, -1, 1, 2, 3, 4", "metallic", 1687f, 3173f, 2.33f, "metalloid", "1854");
		
		/// <summary> Phosphorus (15)</summary>
		public static readonly ElementProperties Phosphorus = new ElementProperties(15, "P", "Phosphorus", 30.97376, "FF8000", "[Ne] 3s2 3p3", 2.19f, 0.106f, 0.044f, 0.18f, 1012f, -72f, "solid", "-3, -2, -1, 1, 2, 3, 4, 5", "covalent network", 317f, 554f, 1.82f, "nonmetal", "1669");
		
		/// <summary> Sulfur (16)</summary>
		public static readonly ElementProperties Sulfur = new ElementProperties(16, "S", "Sulfur", 32.065, "FFFF30", "[Ne] 3s2 3p4", 2.58f, 0.102f, 0.184f, 0.18f, 1000f, -200f, "solid", "-2, -1, 1, 2, 3, 4, 5, 6", "covalent network", 388f, 718f, 1.96f, "nonmetal", "Ancient");
		
		/// <summary> Chlorine (17)</summary>
		public static readonly ElementProperties Chlorine = new ElementProperties(17, "Cl", "Chlorine", 35.453, "1FF01F", "[Ne] 3s2 3p5", 3.16f, 0.09900001f, 0.181f, 0.175f, 1251f, -349f, "gas", "-1, 1, 2, 3, 4, 5, 6, 7", "covalent network", 172f, 239f, 0f, "halogen", "1774");
		
		/// <summary> Argon (18)</summary>
		public static readonly ElementProperties Argon = new ElementProperties(18, "Ar", "Argon", 39.948, "CCB3FF", "[Ne] 3s2 3p6", 0f, 0.097f, 0f, 0.188f, 1521f, 0f, "gas", "", "atomic", 84f, 87f, 0f, "noble gas", "1894");
		
		/// <summary> Potassium (19)</summary>
		public static readonly ElementProperties Potassium = new ElementProperties(19, "K", "Potassium", 39.0983, "8F40D4", "[Ar] 4s1", 0.82f, 0.196f, 0.138f, 0.275f, 419f, -48f, "solid", "1", "metallic", 337f, 1032f, 0.86f, "alkali metal", "1807");
		
		/// <summary> Calcium (20)</summary>
		public static readonly ElementProperties Calcium = new ElementProperties(20, "Ca", "Calcium", 40.078, "3DFF00", "[Ar] 4s2", 1f, 0.174f, 0.1f, 0.231f, 590f, -2f, "solid", "2", "metallic", 1115f, 1757f, 1.55f, "alkaline earth metal", "Ancient");
		
		/// <summary> Scandium (21)</summary>
		public static readonly ElementProperties Scandium = new ElementProperties(21, "Sc", "Scandium", 44.95591, "E6E6E6", "[Ar] 3d1 4s2", 1.36f, 0.144f, 0.0745f, 0f, 633f, -18f, "solid", "1, 2, 3", "metallic", 1814f, 3103f, 2.99f, "transition metal", "1876");
		
		/// <summary> Titanium (22)</summary>
		public static readonly ElementProperties Titanium = new ElementProperties(22, "Ti", "Titanium", 47.867, "BFC2C7", "[Ar] 3d2 4s2", 1.54f, 0.136f, 0.086f, 0f, 659f, -8f, "solid", "-1, 2, 3, 4", "metallic", 1941f, 3560f, 4.51f, "transition metal", "1791");
		
		/// <summary> Vanadium (23)</summary>
		public static readonly ElementProperties Vanadium = new ElementProperties(23, "V", "Vanadium", 50.9415, "A6A6AB", "[Ar] 3d3 4s2", 1.63f, 0.125f, 0.079f, 0f, 651f, -51f, "solid", "-1, 2, 3, 4", "metallic", 2183f, 3680f, 6.11f, "transition metal", "1803");
		
		/// <summary> Chromium (24)</summary>
		public static readonly ElementProperties Chromium = new ElementProperties(24, "Cr", "Chromium", 51.9961, "8A99C7", "[Ar] 3d5 4s1", 1.66f, 0.127f, 0.08000001f, 0f, 653f, -64f, "solid", "-2, -1, 1, 2, 3, 4, 5, 6", "metallic", 2180f, 2944f, 7.14f, "transition metal", "Ancient");
		
		/// <summary> Manganese (25)</summary>
		public static readonly ElementProperties Manganese = new ElementProperties(25, "Mn", "Manganese", 54.93805, "9C7AC7", "[Ar] 3d5 4s2", 1.55f, 0.139f, 0.067f, 0f, 717f, 0f, "solid", "-3, -2, -1, 1, 2, 3, 4, 5, 6, 7", "metallic", 1519f, 2334f, 7.47f, "transition metal", "1774");
		
		/// <summary> Iron (26)</summary>
		public static readonly ElementProperties Iron = new ElementProperties(26, "Fe", "Iron", 55.845, "E06633", "[Ar] 3d6 4s2", 1.83f, 0.125f, 0.078f, 0.14f, 763f, -16f, "solid", "-2, -1, 1, 2, 3, 4, 5, 6", "metallic", 1811f, 3134f, 7.87f, "transition metal", "Ancient");
		
		/// <summary> Cobalt (27)</summary>
		public static readonly ElementProperties Cobalt = new ElementProperties(27, "Co", "Cobalt", 58.93319, "F090A0", "[Ar] 3d7 4s2", 1.88f, 0.126f, 0.0745f, 0f, 760f, -64f, "solid", "-1, 1, 2, 3, 4, 5", "metallic", 1768f, 3200f, 8.9f, "transition metal", "Ancient");
		
		/// <summary> Nickel (28)</summary>
		public static readonly ElementProperties Nickel = new ElementProperties(28, "Ni", "Nickel", 58.6934, "50D050", "[Ar] 3d8 4s2", 1.91f, 0.121f, 0.06900001f, 0.163f, 737f, -112f, "solid", "-1, 1, 2, 3, 4", "metallic", 1728f, 3186f, 8.91f, "transition metal", "1751");
		
		/// <summary> Copper (29)</summary>
		public static readonly ElementProperties Copper = new ElementProperties(29, "Cu", "Copper", 63.546, "C88033", "[Ar] 3d10 4s1", 1.9f, 0.138f, 0.07700001f, 0.14f, 746f, -118f, "solid", "1, 2, 3, 4", "metallic", 1358f, 3200f, 8.92f, "transition metal", "Ancient");
		
		/// <summary> Zinc (30)</summary>
		public static readonly ElementProperties Zinc = new ElementProperties(30, "Zn", "Zinc", 65.38, "7D80B0", "[Ar] 3d10 4s2", 1.65f, 0.131f, 0.074f, 0.139f, 906f, 0f, "solid", "2", "metallic", 693f, 1180f, 7.14f, "transition metal", "1746");
		
		/// <summary> Gallium (31)</summary>
		public static readonly ElementProperties Gallium = new ElementProperties(31, "Ga", "Gallium", 69.723, "C28F8F", "[Ar] 3d10 4s2 4p1", 1.81f, 0.126f, 0.062f, 0.187f, 579f, -29f, "solid", "1, 2, 3", "metallic", 303f, 2477f, 5.9f, "metal", "1875");
		
		/// <summary> Germanium (32)</summary>
		public static readonly ElementProperties Germanium = new ElementProperties(32, "Ge", "Germanium", 72.64, "668F8F", "[Ar] 3d10 4s2 4p2", 2.01f, 0.122f, 0.07300001f, 0f, 762f, -119f, "solid", "-4, 1, 2, 3, 4", "metallic", 1211f, 3093f, 5.32f, "metalloid", "1886");
		
		/// <summary> Arsenic (33)</summary>
		public static readonly ElementProperties Arsenic = new ElementProperties(33, "As", "Arsenic", 74.9216, "BD80E3", "[Ar] 3d10 4s2 4p3", 2.18f, 0.119f, 0.058f, 0.185f, 947f, -78f, "solid", "-3, 2, 3, 5", "metallic", 1090f, 887f, 5.73f, "metalloid", "Ancient");
		
		/// <summary> Selenium (34)</summary>
		public static readonly ElementProperties Selenium = new ElementProperties(34, "Se", "Selenium", 78.96, "FFA100", "[Ar] 3d10 4s2 4p4", 2.55f, 0.116f, 0.198f, 0.19f, 941f, -195f, "solid", "-2, 2, 4, 6", "metallic", 494f, 958f, 4.82f, "nonmetal", "1817");
		
		/// <summary> Bromine (35)</summary>
		public static readonly ElementProperties Bromine = new ElementProperties(35, "Br", "Bromine", 79.904, "A62929", "[Ar] 3d10 4s2 4p5", 2.96f, 0.114f, 0.196f, 0.185f, 1140f, -325f, "liquid", "-1, 1, 3, 4, 5, 7", "covalent network", 266f, 332f, 3.12f, "halogen", "1826");
		
		/// <summary> Krypton (36)</summary>
		public static readonly ElementProperties Krypton = new ElementProperties(36, "Kr", "Krypton", 83.798, "00CC00", "[Ar] 3d10 4s2 4p6", 0f, 0.11f, 0f, 0.202f, 1351f, 0f, "gas", "2", "atomic", 116f, 120f, 0f, "noble gas", "1898");
		
		/// <summary> Rubidium (37)</summary>
		public static readonly ElementProperties Rubidium = new ElementProperties(37, "Rb", "Rubidium", 85.4678, "702EB0", "[Kr] 5s1", 0.82f, 0.211f, 0.152f, 0f, 403f, -47f, "solid", "1", "metallic", 312f, 961f, 1.53f, "alkali metal", "1861");
		
		/// <summary> Strontium (38)</summary>
		public static readonly ElementProperties Strontium = new ElementProperties(38, "Sr", "Strontium", 87.62, "00FF00", "[Kr] 5s2", 0.95f, 0.192f, 0.118f, 0f, 550f, -5f, "solid", "2", "metallic", 1050f, 1655f, 2.63f, "alkaline earth metal", "1790");
		
		/// <summary> Yttrium (39)</summary>
		public static readonly ElementProperties Yttrium = new ElementProperties(39, "Y", "Yttrium", 88.90585, "94FFFF", "[Kr] 4d1 5s2", 1.22f, 0.162f, 0.09f, 0f, 600f, -30f, "solid", "1, 2, 3", "metallic", 1799f, 3618f, 4.47f, "transition metal", "1794");
		
		/// <summary> Zirconium (40)</summary>
		public static readonly ElementProperties Zirconium = new ElementProperties(40, "Zr", "Zirconium", 91.224, "94E0E0", "[Kr] 4d2 5s2", 1.33f, 0.148f, 0.072f, 0f, 640f, -41f, "solid", "1, 2, 3, 4", "metallic", 2128f, 4682f, 6.51f, "transition metal", "1789");
		
		/// <summary> Niobium (41)</summary>
		public static readonly ElementProperties Niobium = new ElementProperties(41, "Nb", "Niobium", 92.90638, "73C2C9", "[Kr] 4d4 5s1", 1.6f, 0.137f, 0.072f, 0f, 652f, -86f, "solid", "-1, 2, 3, 4, 5", "metallic", 2750f, 5017f, 8.57f, "transition metal", "1801");
		
		/// <summary> Molybdenum (42)</summary>
		public static readonly ElementProperties Molybdenum = new ElementProperties(42, "Mo", "Molybdenum", 95.96, "54B5B5", "[Kr] 4d5 5s1", 2.16f, 0.145f, 0.06900001f, 0f, 684f, -72f, "solid", "-2, -1, 1, 2, 3, 4, 5, 6", "metallic", 2896f, 4912f, 10.28f, "transition metal", "1778");
		
		/// <summary> Technetium (43)</summary>
		public static readonly ElementProperties Technetium = new ElementProperties(43, "Tc", "Technetium", 98, "3B9E9E", "[Kr] 4d5 5s2", 1.9f, 0.156f, 0.0645f, 0f, 702f, -53f, "solid", "-3, -1, 1, 2, 3, 4, 5, 6, 7", "metallic", 2430f, 4538f, 11.5f, "transition metal", "1937");
		
		/// <summary> Ruthenium (44)</summary>
		public static readonly ElementProperties Ruthenium = new ElementProperties(44, "Ru", "Ruthenium", 101.07, "248F8F", "[Kr] 4d7 5s1", 2.2f, 0.126f, 0.068f, 0f, 710f, -101f, "solid", "-2, 1, 2, 3, 4, 5, 6, 7, 8", "metallic", 2607f, 4423f, 12.37f, "transition metal", "1827");
		
		/// <summary> Rhodium (45)</summary>
		public static readonly ElementProperties Rhodium = new ElementProperties(45, "Rh", "Rhodium", 102.9055, "0A7D8C", "[Kr] 4d8 5s1", 2.28f, 0.135f, 0.0665f, 0f, 720f, -110f, "solid", "-1, 1, 2, 3, 4, 5, 6", "metallic", 2237f, 3968f, 12.45f, "transition metal", "1803");
		
		/// <summary> Palladium (46)</summary>
		public static readonly ElementProperties Palladium = new ElementProperties(46, "Pd", "Palladium", 106.42, "6985", "[Kr] 4d10", 2.2f, 0.131f, 0.059f, 0.163f, 804f, -54f, "solid", "2, 4", "metallic", 1828f, 3236f, 12.02f, "transition metal", "1803");
		
		/// <summary> Silver (47)</summary>
		public static readonly ElementProperties Silver = new ElementProperties(47, "Ag", "Silver", 107.8682, "C0C0C0", "[Kr] 4d10 5s1", 1.93f, 0.153f, 0.115f, 0.172f, 731f, -126f, "solid", "1, 2, 3", "metallic", 1235f, 2435f, 10.49f, "transition metal", "Ancient");
		
		/// <summary> Cadmium (48)</summary>
		public static readonly ElementProperties Cadmium = new ElementProperties(48, "Cd", "Cadmium", 112.411, "FFD98F", "[Kr] 4d10 5s2", 1.69f, 0.148f, 0.09500001f, 0.158f, 868f, 0f, "solid", "2", "metallic", 594f, 1040f, 8.65f, "transition metal", "1817");
		
		/// <summary> Indium (49)</summary>
		public static readonly ElementProperties Indium = new ElementProperties(49, "In", "Indium", 114.818, "A67573", "[Kr] 4d10 5s2 5p1", 1.78f, 0.144f, 0.08000001f, 0.193f, 558f, -29f, "solid", "1, 2, 3", "metallic", 430f, 2345f, 7.31f, "metal", "1863");
		
		/// <summary> Tin (50)</summary>
		public static readonly ElementProperties Tin = new ElementProperties(50, "Sn", "Tin", 118.71, "668080", "[Kr] 4d10 5s2 5p2", 1.96f, 0.141f, 0.112f, 0.217f, 709f, -107f, "solid", "-4, 2, 4", "metallic", 505f, 2875f, 7.31f, "metal", "Ancient");
		
		/// <summary> Antimony (51)</summary>
		public static readonly ElementProperties Antimony = new ElementProperties(51, "Sb", "Antimony", 121.76, "9E63B5", "[Kr] 4d10 5s2 5p3", 2.05f, 0.138f, 0.07600001f, 0f, 834f, -103f, "solid", "-3, 3, 5", "metallic", 904f, 1860f, 6.7f, "metalloid", "Ancient");
		
		/// <summary> Tellurium (52)</summary>
		public static readonly ElementProperties Tellurium = new ElementProperties(52, "Te", "Tellurium", 127.6, "D47A00", "[Kr] 4d10 5s2 5p4", 2.1f, 0.135f, 0.221f, 0.206f, 869f, -190f, "solid", "-2, 2, 4, 5, 6", "metallic", 723f, 1261f, 6.24f, "metalloid", "1782");
		
		/// <summary> Iodine (53)</summary>
		public static readonly ElementProperties Iodine = new ElementProperties(53, "I", "Iodine", 126.9045, "940094", "[Kr] 4d10 5s2 5p5", 2.66f, 0.133f, 0.22f, 0.198f, 1008f, -295f, "solid", "-1, 1, 3, 5, 7", "covalent network", 387f, 457f, 4.94f, "halogen", "1811");
		
		/// <summary> Xenon (54)</summary>
		public static readonly ElementProperties Xenon = new ElementProperties(54, "Xe", "Xenon", 131.293, "99994d", "[Kr] 4d10 5s2 5p6", 0f, 0.13f, 0.048f, 0.216f, 1170f, 0f, "gas", "2, 4, 6, 8", "atomic", 161f, 165f, 0.01f, "noble gas", "1898");
		
		/// <summary> Cesium (55)</summary>
		public static readonly ElementProperties Cesium = new ElementProperties(55, "Cs", "Cesium", 132.9055, "57178F", "[Xe] 6s1", 0.79f, 0.225f, 0.167f, 0f, 376f, -46f, "solid", "1", "metallic", 302f, 944f, 1.88f, "alkali metal", "1860");
		
		/// <summary> Barium (56)</summary>
		public static readonly ElementProperties Barium = new ElementProperties(56, "Ba", "Barium", 137.327, "00C900", "[Xe] 6s2", 0.89f, 0.198f, 0.135f, 0f, 503f, -14f, "solid", "2", "metallic", 1000f, 2143f, 3.51f, "alkaline earth metal", "1808");
		
		/// <summary> Lanthanum (57)</summary>
		public static readonly ElementProperties Lanthanum = new ElementProperties(57, "La", "Lanthanum", 138.9055, "70D4FF", "[Xe] 5d1 6s2", 1.1f, 0.169f, 0.1032f, 0f, 538f, -48f, "solid", "2, 3", "metallic", 1193f, 3737f, 6.15f, "lanthanoid", "1839");
		
		/// <summary> Cerium (58)</summary>
		public static readonly ElementProperties Cerium = new ElementProperties(58, "Ce", "Cerium", 140.116, "FFFFC7", "[Xe] 4f1 5d1 6s2", 1.12f, 0f, 0.102f, 0f, 534f, -50f, "solid", "2, 3, 4", "metallic", 1071f, 3633f, 6.69f, "lanthanoid", "1803");
		
		/// <summary> Praseodymium (59)</summary>
		public static readonly ElementProperties Praseodymium = new ElementProperties(59, "Pr", "Praseodymium", 140.9077, "D9FFC7", "[Xe] 4f3 6s2", 1.13f, 0f, 0.09900001f, 0f, 527f, -50f, "solid", "2, 3, 4", "metallic", 1204f, 3563f, 6.64f, "lanthanoid", "1885");
		
		/// <summary> Neodymium (60)</summary>
		public static readonly ElementProperties Neodymium = new ElementProperties(60, "Nd", "Neodymium", 144.242, "C7FFC7", "[Xe] 4f4 6s2", 1.14f, 0f, 0.129f, 0f, 533f, -50f, "solid", "2, 3", "metallic", 1294f, 3373f, 7.01f, "lanthanoid", "1885");
		
		/// <summary> Promethium (61)</summary>
		public static readonly ElementProperties Promethium = new ElementProperties(61, "Pm", "Promethium", 145, "A3FFC7", "[Xe] 4f5 6s2", 1.13f, 0f, 0.097f, 0f, 540f, -50f, "solid", "3", "metallic", 1373f, 3273f, 7.26f, "lanthanoid", "1947");
		
		/// <summary> Samarium (62)</summary>
		public static readonly ElementProperties Samarium = new ElementProperties(62, "Sm", "Samarium", 150.36, "8FFFC7", "[Xe] 4f6 6s2", 1.17f, 0f, 0.122f, 0f, 545f, -50f, "solid", "2, 3", "metallic", 1345f, 2076f, 7.35f, "lanthanoid", "1853");
		
		/// <summary> Europium (63)</summary>
		public static readonly ElementProperties Europium = new ElementProperties(63, "Eu", "Europium", 151.964, "61FFC7", "[Xe] 4f7 6s2", 1.2f, 0f, 0.117f, 0f, 547f, -50f, "solid", "2, 3", "metallic", 1095f, 1800f, 5.24f, "lanthanoid", "1901");
		
		/// <summary> Gadolinium (64)</summary>
		public static readonly ElementProperties Gadolinium = new ElementProperties(64, "Gd", "Gadolinium", 157.25, "45FFC7", "[Xe] 4f7 5d1 6s2", 1.2f, 0f, 0.09380001f, 0f, 593f, -50f, "solid", "1, 2, 3", "metallic", 1586f, 3523f, 7.9f, "lanthanoid", "1880");
		
		/// <summary> Terbium (65)</summary>
		public static readonly ElementProperties Terbium = new ElementProperties(65, "Tb", "Terbium", 158.9254, "30FFC7", "[Xe] 4f9 6s2", 1.2f, 0f, 0.09230001f, 0f, 566f, -50f, "solid", "1, 3, 4", "metallic", 1629f, 3503f, 8.22f, "lanthanoid", "1843");
		
		/// <summary> Dysprosium (66)</summary>
		public static readonly ElementProperties Dysprosium = new ElementProperties(66, "Dy", "Dysprosium", 162.5, "1FFFC7", "[Xe] 4f10 6s2", 1.22f, 0f, 0.107f, 0f, 573f, -50f, "solid", "2, 3", "metallic", 1685f, 2840f, 8.55f, "lanthanoid", "1886");
		
		/// <summary> Holmium (67)</summary>
		public static readonly ElementProperties Holmium = new ElementProperties(67, "Ho", "Holmium", 164.9303, "00FF9C", "[Xe] 4f11 6s2", 1.23f, 0f, 0.09010001f, 0f, 581f, -50f, "solid", "3", "metallic", 1747f, 2973f, 8.8f, "lanthanoid", "1878");
		
		/// <summary> Erbium (68)</summary>
		public static readonly ElementProperties Erbium = new ElementProperties(68, "Er", "Erbium", 167.259, "0.00E+00", "[Xe] 4f12 6s2", 1.24f, 0f, 0.089f, 0f, 589f, -50f, "solid", "3", "metallic", 1770f, 3141f, 9.07f, "lanthanoid", "1842");
		
		/// <summary> Thulium (69)</summary>
		public static readonly ElementProperties Thulium = new ElementProperties(69, "Tm", "Thulium", 168.9342, "00D452", "[Xe] 4f13 6s2", 1.25f, 0f, 0.103f, 0f, 597f, -50f, "solid", "2, 3", "metallic", 1818f, 2223f, 9.32f, "lanthanoid", "1879");
		
		/// <summary> Ytterbium (70)</summary>
		public static readonly ElementProperties Ytterbium = new ElementProperties(70, "Yb", "Ytterbium", 173.054, "00BF38", "[Xe] 4f14 6s2", 1.1f, 0f, 0.102f, 0f, 603f, -50f, "solid", "2, 3", "metallic", 1092f, 1469f, 6.57f, "lanthanoid", "1878");
		
		/// <summary> Lutetium (71)</summary>
		public static readonly ElementProperties Lutetium = new ElementProperties(71, "Lu", "Lutetium", 174.9668, "00AB24", "[Xe] 4f14 5d1 6s2", 1.27f, 0.16f, 0.0861f, 0f, 524f, -50f, "solid", "3", "metallic", 1936f, 3675f, 9.84f, "transition metal", "1907");
		
		/// <summary> Hafnium (72)</summary>
		public static readonly ElementProperties Hafnium = new ElementProperties(72, "Hf", "Hafnium", 178.49, "4DC2FF", "[Xe] 4f14 5d2 6s2", 1.3f, 0.15f, 0.071f, 0f, 659f, 0f, "solid", "2, 3, 4", "metallic", 2506f, 4876f, 13.31f, "transition metal", "1923");
		
		/// <summary> Tantalum (73)</summary>
		public static readonly ElementProperties Tantalum = new ElementProperties(73, "Ta", "Tantalum", 180.9479, "4DA6FF", "[Xe] 4f14 5d3 6s2", 1.5f, 0.138f, 0.072f, 0f, 761f, -31f, "solid", "-1, 2, 3, 4, 5", "metallic", 3290f, 5731f, 16.65f, "transition metal", "1802");
		
		/// <summary> Tungsten (74)</summary>
		public static readonly ElementProperties Tungsten = new ElementProperties(74, "W", "Tungsten", 183.84, "2194D6", "[Xe] 4f14 5d4 6s2", 2.36f, 0.146f, 0.066f, 0f, 770f, -79f, "solid", "-2, -1, 1, 2, 3, 4, 5, 6", "metallic", 3695f, 5828f, 19.25f, "transition metal", "1783");
		
		/// <summary> Rhenium (75)</summary>
		public static readonly ElementProperties Rhenium = new ElementProperties(75, "Re", "Rhenium", 186.207, "267DAB", "[Xe] 4f14 5d5 6s2", 1.9f, 0.159f, 0.063f, 0f, 760f, -15f, "solid", "-3, -1, 1, 2, 3, 4, 5, 6, 7", "metallic", 3459f, 5869f, 21.02f, "transition metal", "1925");
		
		/// <summary> Osmium (76)</summary>
		public static readonly ElementProperties Osmium = new ElementProperties(76, "Os", "Osmium", 190.23, "266696", "[Xe] 4f14 5d6 6s2", 2.2f, 0.128f, 0.063f, 0f, 840f, -106f, "solid", "-2, -1, 1, 2, 3, 4, 5, 6, 7, 8", "metallic", 3306f, 5285f, 22.59f, "transition metal", "1803");
		
		/// <summary> Iridium (77)</summary>
		public static readonly ElementProperties Iridium = new ElementProperties(77, "Ir", "Iridium", 192.217, "175487", "[Xe] 4f14 5d7 6s2", 2.2f, 0.137f, 0.068f, 0f, 880f, -151f, "solid", "-3, -1, 1, 2, 3, 4, 5, 6", "metallic", 2739f, 4701f, 22.56f, "transition metal", "1803");
		
		/// <summary> Platinum (78)</summary>
		public static readonly ElementProperties Platinum = new ElementProperties(78, "Pt", "Platinum", 195.084, "D0D0E0", "[Xe] 4f14 5d9 6s1", 2.28f, 0.128f, 0.086f, 0.175f, 870f, -205f, "solid", "2, 4, 5, 6", "metallic", 2041f, 4098f, 21.09f, "transition metal", "Ancient");
		
		/// <summary> Gold (79)</summary>
		public static readonly ElementProperties Gold = new ElementProperties(79, "Au", "Gold", 196.9666, "FFD123", "[Xe] 4f14 5d10 6s1", 2.54f, 0.144f, 0.137f, 0.166f, 890f, -223f, "solid", "-1, 1, 2, 3, 5", "metallic", 1337f, 3129f, 19.3f, "transition metal", "Ancient");
		
		/// <summary> Mercury (80)</summary>
		public static readonly ElementProperties Mercury = new ElementProperties(80, "Hg", "Mercury", 200.59, "B8B8D0", "[Xe] 4f14 5d10 6s2", 2f, 0.149f, 0.119f, 0.155f, 1007f, 0f, "liquid", "1, 2, 4", "metallic", 234f, 630f, 13.53f, "transition metal", "Ancient");
		
		/// <summary> Thallium (81)</summary>
		public static readonly ElementProperties Thallium = new ElementProperties(81, "Tl", "Thallium", 204.3833, "A6544D", "[Xe] 4f14 5d10 6s2 6p1", 2.04f, 0.148f, 0.15f, 0.196f, 589f, -19f, "solid", "1, 3", "metallic", 577f, 1746f, 11.85f, "metal", "1861");
		
		/// <summary> Lead (82)</summary>
		public static readonly ElementProperties Lead = new ElementProperties(82, "Pb", "Lead", 207.2, "575961", "[Xe] 4f14 5d10 6s2 6p2", 2.33f, 0.147f, 0.119f, 0.202f, 716f, -35f, "solid", "-4, 2, 4", "metallic", 601f, 2022f, 11.34f, "metal", "Ancient");
		
		/// <summary> Bismuth (83)</summary>
		public static readonly ElementProperties Bismuth = new ElementProperties(83, "Bi", "Bismuth", 208.9804, "9E4FB5", "[Xe] 4f14 5d10 6s2 6p3", 2.02f, 0.146f, 0.103f, 0f, 703f, -91f, "solid", "-3, 3, 5", "metallic", 544f, 1837f, 9.78f, "metal", "Ancient");
		
		/// <summary> Polonium (84)</summary>
		public static readonly ElementProperties Polonium = new ElementProperties(84, "Po", "Polonium", 209, "AB5C00", "[Xe] 4f14 5d10 6s2 6p4", 2f, 0f, 0.094f, 0f, 812f, -183f, "solid", "-2, 2, 4, 6", "metallic", 527f, 1235f, 9.2f, "metalloid", "1898");
		
		/// <summary> Astatine (85)</summary>
		public static readonly ElementProperties Astatine = new ElementProperties(85, "At", "Astatine", 210, "754F45", "[Xe] 4f14 5d10 6s2 6p5", 2.2f, 0f, 0.062f, 0f, 920f, -270f, "solid", "-1, 1, 3, 5", "covalent network", 575f, 0f, 0f, "halogen", "1940");
		
		/// <summary> Radon (86)</summary>
		public static readonly ElementProperties Radon = new ElementProperties(86, "Rn", "Radon", 222, "428296", "[Xe] 4f14 5d10 6s2 6p6", 0f, 0.145f, 0f, 0f, 1037f, 0f, "gas", "2", "atomic", 202f, 211f, 0.01f, "noble gas", "1900");
		
		/// <summary> Francium (87)</summary>
		public static readonly ElementProperties Francium = new ElementProperties(87, "Fr", "Francium", 223, "420066", "[Rn] 7s1", 0.7f, 0f, 0.18f, 0f, 380f, 0f, "solid", "1", "metallic", 0f, 0f, 0f, "alkali metal", "1939");
		
		/// <summary> Radium (88)</summary>
		public static readonly ElementProperties Radium = new ElementProperties(88, "Ra", "Radium", 226, "007D00", "[Rn] 7s2", 0.9f, 0f, 0.148f, 0f, 509f, 0f, "solid", "2", "metallic", 973f, 2010f, 5f, "alkaline earth metal", "1898");
		
		/// <summary> Actinium (89)</summary>
		public static readonly ElementProperties Actinium = new ElementProperties(89, "Ac", "Actinium", 227, "70ABFA", "[Rn] 6d1 7s2", 1.1f, 0f, 0.112f, 0f, 499f, 0f, "solid", "3", "metallic", 1323f, 3473f, 10.07f, "actinoid", "1899");
		
		/// <summary> Thorium (90)</summary>
		public static readonly ElementProperties Thorium = new ElementProperties(90, "Th", "Thorium", 232.0381, "00BAFF", "[Rn] 6d2 7s2", 1.3f, 0f, 0.094f, 0f, 587f, 0f, "solid", "2, 3, 4", "metallic", 2023f, 5093f, 11.72f, "actinoid", "1828");
		
		/// <summary> Protactinium (91)</summary>
		public static readonly ElementProperties Protactinium = new ElementProperties(91, "Pa", "Protactinium", 231.0359, "00A1FF", "[Rn] 5f2 6d1 7s2", 1.5f, 0f, 0.104f, 0f, 568f, 0f, "solid", "3, 4, 5", "metallic", 1845f, 4273f, 15.37f, "actinoid", "1913");
		
		/// <summary> Uranium (92)</summary>
		public static readonly ElementProperties Uranium = new ElementProperties(92, "U", "Uranium", 238.0289, "008FFF", "[Rn] 5f3 6d1 7s2", 1.38f, 0f, 0.1025f, 0.186f, 598f, 0f, "solid", "3, 4, 5, 6", "metallic", 1408f, 4200f, 19.05f, "actinoid", "1789");
		
		/// <summary> Neptunium (93)</summary>
		public static readonly ElementProperties Neptunium = new ElementProperties(93, "Np", "Neptunium", 237, "0080FF", "[Rn] 5f4 6d1 7s2", 1.36f, 0f, 0.11f, 0f, 605f, 0f, "solid", "3, 4, 5, 6, 7", "metallic", 917f, 4273f, 20.45f, "actinoid", "1940");
		
		/// <summary> Plutonium (94)</summary>
		public static readonly ElementProperties Plutonium = new ElementProperties(94, "Pu", "Plutonium", 244, "006BFF", "[Rn] 5f6 7s2", 1.28f, 0f, 0.1f, 0f, 585f, 0f, "solid", "3, 4, 5, 6, 7", "metallic", 913f, 3503f, 19.82f, "actinoid", "1940");
		
		/// <summary> Americium (95)</summary>
		public static readonly ElementProperties Americium = new ElementProperties(95, "Am", "Americium", 243, "545CF2", "[Rn] 5f7 7s2", 1.3f, 0f, 0.126f, 0f, 578f, 0f, "solid", "2, 3, 4, 5, 6", "metallic", 1449f, 2284f, 13.67f, "actinoid", "1944");
		
		/// <summary> Curium (96)</summary>
		public static readonly ElementProperties Curium = new ElementProperties(96, "Cm", "Curium", 247, "785CE3", "[Rn] 5f7 6d1 7s2", 1.3f, 0f, 0.097f, 0f, 581f, 0f, "solid", "3, 4", "metallic", 1618f, 3383f, 13.51f, "actinoid", "1944");
		
		/// <summary> Berkelium (97)</summary>
		public static readonly ElementProperties Berkelium = new ElementProperties(97, "Bk", "Berkelium", 247, "8A4FE3", "[Rn] 5f9 7s2", 1.3f, 0f, 0.096f, 0f, 601f, 0f, "solid", "3, 4", "metallic", 1323f, 0f, 14.78f, "actinoid", "1949");
		
		/// <summary> Californium (98)</summary>
		public static readonly ElementProperties Californium = new ElementProperties(98, "Cf", "Californium", 251, "A136D4", "[Rn] 5f10 7s2", 1.3f, 0f, 0.09500001f, 0f, 608f, 0f, "solid", "2, 3, 4", "metallic", 1173f, 0f, 15.1f, "actinoid", "1950");
		
		/// <summary> Einsteinium (99)</summary>
		public static readonly ElementProperties Einsteinium = new ElementProperties(99, "Es", "Einsteinium", 252, "B31FD4", "[Rn] 5f11 7s2", 1.3f, 0f, 0f, 0f, 619f, 0f, "solid", "2, 3", "", 1133f, 0f, 0f, "actinoid", "1952");
		
		/// <summary> Fermium (100)</summary>
		public static readonly ElementProperties Fermium = new ElementProperties(100, "Fm", "Fermium", 257, "B31FBA", "[Rn] 5f12 7s2", 1.3f, 0f, 0f, 0f, 627f, 0f, "", "2, 3", "", 1800f, 0f, 0f, "actinoid", "1952");
		
		/// <summary> Mendelevium (101)</summary>
		public static readonly ElementProperties Mendelevium = new ElementProperties(101, "Md", "Mendelevium", 258, "B30DA6", "[Rn] 5f13 7s2", 1.3f, 0f, 0f, 0f, 635f, 0f, "", "2, 3", "", 1100f, 0f, 0f, "actinoid", "1955");
		
		/// <summary> Nobelium (102)</summary>
		public static readonly ElementProperties Nobelium = new ElementProperties(102, "No", "Nobelium", 259, "BD0D87", "[Rn] 5f14 7s2", 1.3f, 0f, 0f, 0f, 642f, 0f, "", "2, 3", "", 1100f, 0f, 0f, "actinoid", "1957");
		
		/// <summary> Lawrencium (103)</summary>
		public static readonly ElementProperties Lawrencium = new ElementProperties(103, "Lr", "Lawrencium", 262, "C70066", "[Rn] 5f14 7s2 7p1", 1.3f, 0f, 0f, 0f, 0f, 0f, "", "3", "", 1900f, 0f, 0f, "transition metal", "1961");
		
		/// <summary> Rutherfordium (104)</summary>
		public static readonly ElementProperties Rutherfordium = new ElementProperties(104, "Rf", "Rutherfordium", 267, "CC0059", "[Rn] 5f14 6d2 7s2", 0f, 0f, 0f, 0f, 0f, 0f, "", "4", "", 0f, 0f, 0f, "transition metal", "1969");
		
		/// <summary> Dubnium (105)</summary>
		public static readonly ElementProperties Dubnium = new ElementProperties(105, "Db", "Dubnium", 268, "D1004F", "[Rn].5f14.6d3.7s2", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "transition metal", "1967");
		
		/// <summary> Seaborgium (106)</summary>
		public static readonly ElementProperties Seaborgium = new ElementProperties(106, "Sg", "Seaborgium", 271, "D90045", "[Rn].5f14.6d4.7s2", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "transition metal", "1974");
		
		/// <summary> Bohrium (107)</summary>
		public static readonly ElementProperties Bohrium = new ElementProperties(107, "Bh", "Bohrium", 272, "E00038", "[Rn].5f14.6d5.7s2", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "transition metal", "1976");
		
		/// <summary> Hassium (108)</summary>
		public static readonly ElementProperties Hassium = new ElementProperties(108, "Hs", "Hassium", 270, "E6002E", "[Rn].5f14.6d6.7s2", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "transition metal", "1984");
		
		/// <summary> Meitnerium (109)</summary>
		public static readonly ElementProperties Meitnerium = new ElementProperties(109, "Mt", "Meitnerium", 276, "EB0026", "[Rn].5f14.6d7.7s2", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "transition metal", "1982");
		
		/// <summary> Darmstadtium (110)</summary>
		public static readonly ElementProperties Darmstadtium = new ElementProperties(110, "Ds", "Darmstadtium", 281, "", "[Rn].5f14.6d9.7s1", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "transition metal", "1994");
		
		/// <summary> Roentgenium (111)</summary>
		public static readonly ElementProperties Roentgenium = new ElementProperties(111, "Rg", "Roentgenium", 280, "", "[Rn].5f14.6d10.7s1", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "transition metal", "1994");
		
		/// <summary> Copernicium (112)</summary>
		public static readonly ElementProperties Copernicium = new ElementProperties(112, "Cn", "Copernicium", 285, "", "[Rn].5f14.6d10.7s2", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "transition metal", "1996");
		
		/// <summary> Ununtrium (113)</summary>
		public static readonly ElementProperties Ununtrium = new ElementProperties(113, "Uut", "Ununtrium", 284, "", "[Rn].5f14.6d10.7s2.7p1", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "metal", "2003");
		
		/// <summary> Ununquadium (114)</summary>
		public static readonly ElementProperties Ununquadium = new ElementProperties(114, "Uuq", "Ununquadium", 289, "", "[Rn].5f14.6d10.7s2.7p2", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "metal", "1998");
		
		/// <summary> Ununpentium (115)</summary>
		public static readonly ElementProperties Ununpentium = new ElementProperties(115, "Uup", "Ununpentium", 288, "", "[Rn].5f14.6d10.7s2.7p3", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "halogen", "2003");
		
		/// <summary> Ununhexium (116)</summary>
		public static readonly ElementProperties Ununhexium = new ElementProperties(116, "Uuh", "Ununhexium", 293, "", "[Rn].5f14.6d10.7s2.7p4", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "noble gas", "2000");
		
		/// <summary> Ununseptium (117)</summary>
		public static readonly ElementProperties Ununseptium = new ElementProperties(117, "Uus", "Ununseptium", 0, "", "[Rn].5f14.6d10.7s2.7p5", 0f, 0f, 0f, 0f, 0f, 0f, "", "", "", 0f, 0f, 0f, "alkali metal", "2010");    
		
		static PeriodicTable() 
		{
			Elements = new ElementProperties[118];
			
			Elements[0] =  Virtual;
			Elements[1] =  Hydrogen;
			Elements[2] =  Helium;
			Elements[3] =  Lithium;
			Elements[4] =  Beryllium;
			Elements[5] =  Boron;
			Elements[6] =  Carbon;
			Elements[7] =  Nitrogen;
			Elements[8] =  Oxygen;
			Elements[9] =  Fluorine;
			Elements[10] =  Neon;
			Elements[11] =  Sodium;
			Elements[12] =  Magnesium;
			Elements[13] =  Aluminium;
			Elements[14] =  Silicon;
			Elements[15] =  Phosphorus;
			Elements[16] =  Sulfur;
			Elements[17] =  Chlorine;
			Elements[18] =  Argon;
			Elements[19] =  Potassium;
			Elements[20] =  Calcium;
			Elements[21] =  Scandium;
			Elements[22] =  Titanium;
			Elements[23] =  Vanadium;
			Elements[24] =  Chromium;
			Elements[25] =  Manganese;
			Elements[26] =  Iron;
			Elements[27] =  Cobalt;
			Elements[28] =  Nickel;
			Elements[29] =  Copper;
			Elements[30] =  Zinc;
			Elements[31] =  Gallium;
			Elements[32] =  Germanium;
			Elements[33] =  Arsenic;
			Elements[34] =  Selenium;
			Elements[35] =  Bromine;
			Elements[36] =  Krypton;
			Elements[37] =  Rubidium;
			Elements[38] =  Strontium;
			Elements[39] =  Yttrium;
			Elements[40] =  Zirconium;
			Elements[41] =  Niobium;
			Elements[42] =  Molybdenum;
			Elements[43] =  Technetium;
			Elements[44] =  Ruthenium;
			Elements[45] =  Rhodium;
			Elements[46] =  Palladium;
			Elements[47] =  Silver;
			Elements[48] =  Cadmium;
			Elements[49] =  Indium;
			Elements[50] =  Tin;
			Elements[51] =  Antimony;
			Elements[52] =  Tellurium;
			Elements[53] =  Iodine;
			Elements[54] =  Xenon;
			Elements[55] =  Cesium;
			Elements[56] =  Barium;
			Elements[57] =  Lanthanum;
			Elements[58] =  Cerium;
			Elements[59] =  Praseodymium;
			Elements[60] =  Neodymium;
			Elements[61] =  Promethium;
			Elements[62] =  Samarium;
			Elements[63] =  Europium;
			Elements[64] =  Gadolinium;
			Elements[65] =  Terbium;
			Elements[66] =  Dysprosium;
			Elements[67] =  Holmium;
			Elements[68] =  Erbium;
			Elements[69] =  Thulium;
			Elements[70] =  Ytterbium;
			Elements[71] =  Lutetium;
			Elements[72] =  Hafnium;
			Elements[73] =  Tantalum;
			Elements[74] =  Tungsten;
			Elements[75] =  Rhenium;
			Elements[76] =  Osmium;
			Elements[77] =  Iridium;
			Elements[78] =  Platinum;
			Elements[79] =  Gold;
			Elements[80] =  Mercury;
			Elements[81] =  Thallium;
			Elements[82] =  Lead;
			Elements[83] =  Bismuth;
			Elements[84] =  Polonium;
			Elements[85] =  Astatine;
			Elements[86] =  Radon;
			Elements[87] =  Francium;
			Elements[88] =  Radium;
			Elements[89] =  Actinium;
			Elements[90] =  Thorium;
			Elements[91] =  Protactinium;
			Elements[92] =  Uranium;
			Elements[93] =  Neptunium;
			Elements[94] =  Plutonium;
			Elements[95] =  Americium;
			Elements[96] =  Curium;
			Elements[97] =  Berkelium;
			Elements[98] =  Californium;
			Elements[99] =  Einsteinium;
			Elements[100] =  Fermium;
			Elements[101] =  Mendelevium;
			Elements[102] =  Nobelium;
			Elements[103] =  Lawrencium;
			Elements[104] =  Rutherfordium;
			Elements[105] =  Dubnium;
			Elements[106] =  Seaborgium;
			Elements[107] =  Bohrium;
			Elements[108] =  Hassium;
			Elements[109] =  Meitnerium;
			Elements[110] =  Darmstadtium;
			Elements[111] =  Roentgenium;
			Elements[112] =  Copernicium;
			Elements[113] =  Ununtrium;
			Elements[114] =  Ununquadium;
			Elements[115] =  Ununpentium;
			Elements[116] =  Ununhexium;
			Elements[117] =  Ununseptium;    	
	
			CacheElementProperties();
		}
    }
}
