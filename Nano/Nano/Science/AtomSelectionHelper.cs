﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Rug.Osc;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Nano.Science
{
    /// <summary>
    /// Helper methods for selecting atoms from flattened OSC address list.
    /// </summary>
    public static class AtomSelectionHelper
    {
        /// <summary>
        /// Enum AtomSelectionType managing which method to use to perform atom selection.
        /// </summary>
        public enum AtomSelectionType
        {
            /// <summary>
            /// OscAddress
            /// </summary>
            /// <remarks>
            /// Example: //CO/*/*
            /// Addresses matched: /Atmos/CO/*/*
            /// </remarks>
            OscAddress,

            /// <summary>
            /// Select using a substring of the address.
            /// </summary>
            /// <remarks>
            /// Example: CO
            /// Addresses matched: /Atmos/CO2/*/*, /Atmos/CO/*/*
            /// </remarks>
            Substring,

            /// <summary>
            /// Select using a regular expression.
            /// </summary>
            /// <remarks>
            /// Example: .*/CO/
            /// Addresses matched: /Atmos/CO/*/*
            /// </remarks>
            Regex,

            StartsWith,
            EndsWith
        }

        /// <summary>
        /// Gets the child atoms of the specified address.
        /// </summary>
        /// <param name="addressSelected">The address selected.</param>
        /// <param name="addresses">The addresses.</param>
        /// <param name="atoms">The atoms.</param>
        public static void GetChildrenOf(string addressSelected, List<string> addresses, HashSet<ushort> atoms)
        {
            if (atoms == null) atoms = new HashSet<ushort>();
            addressSelected += "/";
            atoms.UnionWith(SelectAtomsByRegex(addresses, addressSelected));
        }

        /// <summary>
        /// Expands the selected address to include neighbours and their children.
        /// </summary>
        /// <param name="addressOfSelected">The address of selected.</param>
        /// <param name="addresses">The addresses.</param>
        /// <param name="atoms">The atoms.</param>
        /// <returns>System.String.</returns>
        public static string ExpandSelection(string addressOfSelected, List<string> addresses, HashSet<ushort> atoms)
        {
            if (atoms == null) atoms = new HashSet<ushort>();
            string parentAddress = GetParentAddress(addressOfSelected);
            GetChildrenOf(parentAddress, addresses, atoms);
            return parentAddress;
        }

        /// <summary>
        /// Gets the parent address of the given address.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns>System.String.</returns>
        public static string GetParentAddress(string address)
        {
            OscAddress oscAddress = new OscAddress(address);
            string parentAddress = "";
            for (int i = 0; i < oscAddress.Count - 2; i++)
            {
                parentAddress += oscAddress[i].Value;
            }
            return parentAddress;
        }

        /// <summary>
        /// Selects the atoms that match the given osc address.
        /// </summary>
        /// <param name="addresses">The addresses.</param>
        /// <param name="address">The address to match.</param>
        /// <returns>List&lt;System.UInt16&gt;.</returns>
        public static List<ushort> SelectAtomsByOscAddress(List<string> addresses, OscAddress address, List<ushort> atoms = null)
        {
            if (atoms == null) atoms = new List<ushort>();
            ushort index = 0;
            foreach (string atomAddress in addresses)
            {
                if (address.Match(atomAddress))
                {
                    atoms.Add(index);
                }

                index++;
            }

            return atoms;
        }

        /// <summary>
        /// Selects the atoms that starts with the given pattern.
        /// </summary>
        /// <param name="addresses">The addresses.</param>
        /// <param name="pattern">The pattern.</param>
        /// <param name="atoms">The atoms.</param>
        /// <returns>List&lt;System.UInt16&gt;.</returns>
        public static List<ushort> SelectAtomsByStartsWith(List<string> addresses, string pattern, List<ushort> atoms = null)
        {
            if (atoms == null) atoms = new List<ushort>();
            ushort index = 0;
            foreach (string atomAddress in addresses)
            {
                if (atomAddress.StartsWith(pattern))
                {
                    atoms.Add(index);
                }
                index++;
            }
            return atoms;
        }

        /// <summary>
        /// Selects the atoms that ends with the given pattern.
        /// </summary>
        /// <param name="addresses">The addresses.</param>
        /// <param name="pattern">The pattern.</param>
        /// <param name="atoms">The atoms.</param>
        /// <returns>List&lt;System.UInt16&gt;.</returns>
        public static List<ushort> SelectAtomsByEndsWith(List<string> addresses, string pattern, List<ushort> atoms = null)
        {
            if (atoms == null) atoms = new List<ushort>();
            ushort index = 0;
            foreach (string atomAddress in addresses)
            {
                if (atomAddress.EndsWith(pattern))
                {
                    atoms.Add(index);
                }
                index++;
            }
            return atoms;
        }

        /// <summary>
        /// Selects the atoms by regex pattern.
        /// </summary>
        /// <param name="addresses">The addresses.</param>
        /// <param name="regexPattern">The regex pattern.</param>
        /// <param name="atoms">The atoms.</param>
        /// <returns>List&lt;System.UInt16&gt;.</returns>
        public static List<ushort> SelectAtomsByRegex(List<string> addresses, string regexPattern, List<ushort> atoms = null)
        {
            if (atoms == null) atoms = new List<ushort>();
            ushort index = 0;
            Regex r = new Regex(regexPattern);
            foreach (string atomAddress in addresses)
            {
                Match m = r.Match(atomAddress);
                if (m.Success)
                {
                    atoms.Add(index);
                }
                index++;
            }
            return atoms;
        }

        /// <summary>
        /// Selects the atoms by substring in the address.
        /// </summary>
        /// <param name="substring">The substring.</param>
        /// <returns>List&lt;System.UInt16&gt;.</returns>
        public static List<ushort> SelectAtomsBySubstring(List<string> addresses, string substring, List<ushort> atoms = null)
        {
            if (atoms == null) atoms = new List<ushort>();
            ushort index = 0;
            foreach (string atomAddress in addresses)
            {
                if (atomAddress.Contains(substring))
                {
                    atoms.Add(index);
                }

                index++;
            }

            return atoms;
        }

        public static List<ushort> SelectAtoms(List<string> addresses, string pattern, List<ushort> atoms = null, AtomSelectionType selectionType = AtomSelectionType.OscAddress)
        {
            switch (selectionType)
            {
                case AtomSelectionType.OscAddress:
                    return SelectAtomsByOscAddress(addresses, new OscAddress(pattern), atoms);

                case AtomSelectionType.Substring:
                    return SelectAtomsBySubstring(addresses, pattern, atoms);

                case AtomSelectionType.Regex:
                    return SelectAtomsByRegex(addresses, pattern, atoms);

                case AtomSelectionType.StartsWith:
                    return SelectAtomsByStartsWith(addresses, pattern, atoms);

                case AtomSelectionType.EndsWith:
                    return SelectAtomsByEndsWith(addresses, pattern, atoms);

                default:
                    return null;
            }
        }

        public static List<ushort> SelectRange(List<string> addresses, string pattern, IEnumerable<string> suffixes, List<ushort> atoms = null, AtomSelectionType selectionType = AtomSelectionType.OscAddress)
        {
            char[] separator = { '/' };
            pattern = pattern.TrimEnd(separator);

            foreach (string suffix in suffixes)
            {
                var trimmedSuffix = suffix.TrimStart(separator);
                string address = pattern + "/" + trimmedSuffix;
                atoms = SelectAtoms(addresses, address, atoms, selectionType);
            }
            return atoms;
        }

        public static List<ushort> SelectRange(List<string> addresses, string pattern, ushort start, ushort end, List<ushort> atoms = null, AtomSelectionType selectionType = AtomSelectionType.OscAddress)
        {
            var rangeNumeral = Enumerable.Range(start, end - start);
            List<string> suffixes = new List<string>(end - start);
            foreach (ushort value in rangeNumeral)
            {
                suffixes.Add(value.ToString());
            }
            return SelectRange(addresses, pattern, suffixes, atoms, selectionType);
        }
    }
}