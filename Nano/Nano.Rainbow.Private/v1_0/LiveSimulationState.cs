﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the ^GPL. See License.txt in the project root for license information.

namespace Nano.Rainbow.Private.v1_0
{
    public class LiveSimulationState
    {
        public static readonly string MethodPath = "/1.0/server/details";

        public string ActiveSimulation { get; set; }

        public float ComputationalLoad { get; set; }

        public int NumberOfClients { get; set; }

        public string State { get; set; }
    }
}