﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;
using Nano.Rainbow.Private.Broker;
using Nano.Rainbow.Private.v1_0;
using Nano.Rainbow.v1_0;
using Nano.Transport.Comms.Security;
using Nano.WebSockets;
using Newtonsoft.Json;

namespace Nano.Rainbow.Private.Reservation
{
    public class ReservationServer : RainbowServer, IServiceDiscoveryServer
    {
        private readonly ManualResetEvent brokerForceUpdate = new ManualResetEvent(false);

        private readonly ConcurrentDictionary<Uri, BrokerState> brokerServers = new ConcurrentDictionary<Uri, BrokerState>();

        public LiveSimulation LiveSimulation { get; }

        public ITransportSecurityProvider SecurityProvider { get; }

        /// <inheritdoc />
        public ReservationServer(LiveSimulation liveSimulation, ITransportSecurityProvider securityProvider, IReporter reporter)
            : base(liveSimulation.Server.ReservationHost, null, false, reporter)
        {
            SecurityProvider = securityProvider;

            LiveSimulation = liveSimulation;

            SecurityProvider.ReservationMade += SecurityProvider_ReservationMade;
            SecurityProvider.ReservationReleased += SecurityProvider_ReservationReleased;
            SecurityProvider.ReservationExpired += SecurityProvider_ReservationExpired;

            AddHandler(ReservationRequest.MethodPath, HandleReservation);
        }

        /// <inheritdoc />
        public override async Task Start(CancellationToken cancel)
        {
            await Task.WhenAll(base.Start(cancel), UpdateBrokers(cancel));
        }

        public void AddBroker(Uri brokerUri)
        {
            brokerServers.TryAdd(
                brokerUri,
                new BrokerState
                {
                    LastUpdate = DateTime.MinValue
                }
            );
        }

        public void RemoveBroker(Uri brokerUri)
        {
            brokerServers.TryRemove(brokerUri, out BrokerState _);
        }

        private async Task<byte[]> HandleReservation(Uri uri, Header header, byte[] body)
        {
            if (header.TryGetValue("server-host", out string brokerHost) == false)
            {
                return ErrorResponse(HttpStatusCode.BadRequest, "Missing host header");
            }

            try
            {
                Uri brokerUri = new Uri(brokerHost);

                if (brokerServers.ContainsKey(brokerUri) == false)
                {
                    return ErrorResponse(HttpStatusCode.Forbidden, "Broker request from unknown broker");
                }
            }
            catch (Exception ex)
            {
                return ExceptionResponse(HttpStatusCode.BadRequest, "Invalid reservation request", ex);
            }

            if (header.TryGetValue("authorization", out string authorizationToken) == false)
            {
                return ErrorResponse(HttpStatusCode.BadRequest, "Missing authorization header");
            }

            if (BrokerConstants.ValidateToken(brokerHost, authorizationToken) == false)
            {
                return ErrorResponse(HttpStatusCode.Forbidden, "Invalid authorization token");
            }

            ReservationRequest reservationRequest;

            try
            {
                reservationRequest = JsonConvert.DeserializeObject<ReservationRequest>(Encoding.UTF8.GetString(body));
            }
            catch (Exception ex)
            {
                return ExceptionResponse(HttpStatusCode.BadRequest, "Invalid reservation request", ex);
            }

            PrintObject(reservationRequest);

            bool reserved = SecurityProvider.TryServerReservation(reservationRequest.SimulationPath, out string clientKey);

            ReservationResponse reservationResponse = new ReservationResponse
            {
                Response = reserved ? ReservationResponseType.Reserved : ReservationResponseType.Unavailable,
                Host = LiveSimulation.Server.Host,
                Name = LiveSimulation.Server.Name,
                Descriptor = LiveSimulation.Server.Version,
                SessionToken = clientKey
            };

            ResetBrokerTimer();

            PrintObject(reservationResponse);

            return GetJsonResponse(
                new Header(reserved ? HttpStatusCode.OK : HttpStatusCode.ServiceUnavailable)
                    .Add("authorization", BrokerConstants.GenerateAuthorizationToken(LiveSimulation.Server.Host.ToString())),
                AcceptsGzip(header),
                JsonConvert.SerializeObject(reservationResponse)
            );
        }

        private void ResetBrokerTimer()
        {
            foreach (BrokerState state in brokerServers.Values)
            {
                state.LastUpdate = DateTime.MinValue;
            }

            brokerForceUpdate.Set();
        }

        private void SecurityProvider_ReservationExpired()
        {
            LiveSimulation.Reservation.SessionState = (int) ConnectionSessionState.Unreserved;

            ResetBrokerTimer();
        }

        private void SecurityProvider_ReservationMade()
        {
            LiveSimulation.Reservation.SessionState = ConnectionSessionState.Reserved;

            ResetBrokerTimer();
        }

        private void SecurityProvider_ReservationReleased()
        {
            LiveSimulation.Reservation.SessionState = ConnectionSessionState.Unreserved;

            ResetBrokerTimer();
        }

        private async Task<bool> SendObject(
            Uri host,
            string methodPath,
            BrokerState brokerState,
            object @object)
        {
            Uri method = new Uri(host, new Uri(methodPath.TrimStart('/'), UriKind.Relative));

            try
            {
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(method);

                request.KeepAlive = false;
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("server-host", LiveSimulation.Server.Host.ToString());
                request.Headers.Add("authorization", BrokerConstants.GenerateAuthorizationToken(LiveSimulation.Server.Host.ToString()));

                using (StreamWriter streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(JsonConvert.SerializeObject(@object));
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                using (HttpWebResponse response = (HttpWebResponse) await Task.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null))
                {
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            Reporter.PrintDebug(Direction.Transmit, method.ToString(), "Broker updated");
                            return true;

                        default:
                            Reporter.PrintError(Direction.Transmit, method.ToString(), $"Could not update broker ({response.StatusCode})");
                            brokerState.HasMadeContact = false;
                            return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Reporter.PrintError(Direction.Transmit, method.ToString(), "Could not make contact with broker, " + ex.Message);
                brokerState.HasMadeContact = false;

                return false;
            }
        }

        private async Task UpdateBroker(Uri host, BrokerState brokerState)
        {
            try
            {
                if (brokerState.HasMadeContact == false)
                {
                    Reporter?.PrintDetail(Direction.Action, host.ToString(), "Sending server record");

                    if (await SendObject(host, LiveSimulationHost.MethodPath, brokerState, LiveSimulation.Server) == false)
                    {
                        return;
                    }
                }

                Reporter?.PrintDetail(Direction.Action, host.ToString(), "Sending server state");

                if (await SendObject(host, LiveSimulationState.MethodPath, brokerState, LiveSimulation.Details) == false)
                {
                    return;
                }

                if (await SendObject(host, ReservationState.MethodPath, brokerState, LiveSimulation.Reservation) == false)
                {
                    return;
                }

                brokerState.HasMadeContact = true;
                brokerState.LastUpdate = DateTime.Now;
            }
            catch (Exception ex)
            {
                Reporter.PrintException(ex, "Update broker exception");
            }
        }

        private async Task UpdateBrokers(CancellationToken cancel)
        {
            Reporter.PrintEmphasized("Started update brokers:");

            foreach (Uri brokerServer in brokerServers.Keys)
            {
                Reporter.PrintEmphasized(Direction.Action, "broker", brokerServer.ToString());
            }

            await Task.Yield();

            try
            {
                do
                {
                    SecurityProvider.CheckReservationState();

                    DateTime now = DateTime.Now;
                    DateTime thresholdTime = now - TimeSpan.FromMinutes(1);

                    await Task.WhenAll(
                        (
                            from kvp in brokerServers
                            where kvp.Value.LastUpdate < thresholdTime
                            select kvp)
                        .Select(kvp => UpdateBroker(kvp.Key, kvp.Value))
                    );

                    //Task.WaitAny(new Task[] {Task.Delay(30000, cancel), new MyWaitHandle(brokerForceUpdate.GetSafeWaitHandle()).ToTask()}, cancel);
                    await Task.Delay(100, cancel);
                }
                while (cancel.IsCancellationRequested == false);
            }
            catch (TaskCanceledException _)
            {
            }
            catch (OperationCanceledException _)
            {
            }
            catch (Exception ex)
            {
                Reporter.PrintException(ex, "Check for expired servers failed with exception");
            }
        }

        private class MyWaitHandle : WaitHandle
        {
            public MyWaitHandle(SafeWaitHandle handle)
            {
                SafeWaitHandle = handle;
            }
        }

        private class BrokerState
        {
            public bool HasMadeContact { get; set; }

            public DateTime LastUpdate { get; set; }
        }
    }


    public static class AwaitExtensions
    {
        /// <summary>
        ///     Provides await functionality for ordinary <see cref="WaitHandle" />s.
        /// </summary>
        /// <param name="handle">The handle to wait on.</param>
        /// <returns>The awaiter.</returns>
        public static TaskAwaiter GetAwaiter(this WaitHandle handle)
        {
            //Contract.Requires<ArgumentNullException>(handle != null);

            return handle.ToTask()
                .GetAwaiter();
        }

        /// <summary>
        ///     Creates a TPL Task that is marked as completed when a <see cref="WaitHandle" /> is signaled.
        /// </summary>
        /// <param name="handle">The handle whose signal triggers the task to be completed.</param>
        /// <returns>A Task that is completed after the handle is signaled.</returns>
        /// <remarks>
        ///     There is a (brief) time delay between when the handle is signaled and when the task is marked as completed.
        /// </remarks>
        public static Task ToTask(this WaitHandle handle)
        {
            //Contract.Requires<ArgumentNullException>(handle != null);
            //Contract.Ensures(Contract.Result<Task>() != null);

            TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();

            RegisteredWaitHandle shared = null;

            RegisteredWaitHandle produced = ThreadPool.RegisterWaitForSingleObject(
                handle,
                (state, timedOut) =>
                {
                    tcs.SetResult(null);

                    while (true)
                    {
                        RegisteredWaitHandle consumed = Interlocked.CompareExchange(ref shared, null, null);

                        if (consumed != null)
                        {
                            consumed.Unregister(null);
                            break;
                        }
                    }
                },
                null,
                Timeout.Infinite,
                true
            );

            // Publish the RegisteredWaitHandle so that the callback can see it.
            Interlocked.CompareExchange(ref shared, produced, null);

            return tcs.Task;
        }
    }
}