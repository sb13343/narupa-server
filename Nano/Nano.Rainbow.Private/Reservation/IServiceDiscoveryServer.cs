﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Threading;
using System.Threading.Tasks;

namespace Nano.Rainbow.Private.Reservation
{
    public interface IServiceDiscoveryServer
    {
        Task Start(CancellationToken cancel);
    }
}