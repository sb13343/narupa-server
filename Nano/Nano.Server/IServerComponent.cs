﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using Rug.Cmd;

namespace Nano.Server
{
    public interface IServerComponent : ILoadable
    {
        void RegisterArguments(ArgumentParser parser);

        void ValidateArguments(IReporter reporter);                
    }
}