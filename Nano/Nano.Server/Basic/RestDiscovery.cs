﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Nano.Loading;
using Nano.Rainbow.Private.Reservation;
using Nano.Rainbow.Private.v1_0;
using Rug.Cmd;

namespace Nano.Server.Basic
{
    [XmlName("RestDiscovery")]
    public class RestDiscovery : IDiscovery
    {
        private readonly StringArgument restEndpointArgument = new StringArgument("rest-endpoint", "The end-point uri that the rest service exists at", "The end-point uri that the rest service exists at");
        private readonly CsvArgument brokerUrlsArgument  = new CsvArgument("broker-urls", "The urls of the broker service", "The url of the broker service");
        
        public Uri EndpointUri { get; set; }
        public List<Uri> BrokerUrls { get; } = new List<Uri>();  

        /// <inheritdoc />
        public async Task Start(IServer serverContext, CancellationToken cancel)
        {
            IReporter reporter = serverContext.ReportManager.BeginReport(ReportContext.Discovery, "rest-server", out Uri logIdentifier);
            
            try
            {
                LiveSimulation serverRecord = new LiveSimulation
                {
                    LastContact = DateTime.Now,

                    Server = new LiveSimulationHost
                    {
                        Host = serverContext.Service.Endpoint.EndpointUri,
                        Started = DateTime.Now,
                        Name = serverContext.Name,
                        Version = serverContext.Description,

                        ReservationHost = EndpointUri,
                        SecurityType = ConnectionSecurityType.Reservation
                    },

                    Details = new LiveSimulationState
                    {
                        ActiveSimulation = "",
                        ComputationalLoad = 0f,
                        NumberOfClients = 0,
                        State = string.Empty
                    },

                    Reservation = new ReservationState
                    {
                        SecretKey = "",
                        SessionState = (int) ConnectionSessionState.Unreserved
                    }
                };

                ReservationServer reservationServer = new ReservationServer(
                    serverRecord,
                    serverContext.SecurityProvider.TransportSecurityProvider,
                    reporter);
                
                foreach (Uri brokerUrl in BrokerUrls)
                {
                    reservationServer.AddBroker(brokerUrl);
                }

                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "rest-server started");

                // resolve service information here
                await reservationServer.Start(cancel);
            }
            catch (TaskCanceledException _)
            {
                reporter.PrintDebug(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "rest-server task canceled");
            }
            catch (Exception ex)
            {
                reporter.PrintException(ex, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff} Exception in rest-server task");
            }
            finally
            {
                reporter.PrintNormal(Direction.Action, $"{DateTime.UtcNow:dd-MM-yyyy HH:mm:ss.fff}", "rest-server ended");

                serverContext.ReportManager.EndReport(logIdentifier);
            }
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            EndpointUri = new Uri(Helper.GetAttributeValue(node, nameof(EndpointUri), "tcp://0.0.0.0:8001"), UriKind.Absolute);

            string brokerUrls = Helper.GetAttributeValue(node, nameof(BrokerUrls), null);

            ParseBrokerUrls(brokerUrls);
        }

        private void ParseBrokerUrls(string brokerUrls)
        {
            BrokerUrls.Clear();

            if (brokerUrls != null)
            {
                foreach (string brokerUrl in brokerUrls.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries))
                {
                    BrokerUrls.Add(new Uri(brokerUrl, UriKind.Absolute));
                }
            }
        }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(EndpointUri), EndpointUri.ToString());

            Helper.AppendAttributeAndValue(element, nameof(BrokerUrls), string.Join(",", BrokerUrls));             

            return element;
        }

        /// <inheritdoc />
        public void RegisterArguments(ArgumentParser parser)
        {
            restEndpointArgument.Reset();
            brokerUrlsArgument.Reset(); 
            
            parser.Add("-", "Rest-endpoint", restEndpointArgument);
            parser.Add("-", "Brokers", brokerUrlsArgument);
        }

        /// <inheritdoc />       
        public void ValidateArguments(IReporter reporter)
        {
            if (restEndpointArgument.Defined == false && EndpointUri == null)
            {
                throw new ArgumentException("No rest service endpoint defined");
            }

            if (restEndpointArgument.Defined == false)
            {
                return;
            }

            EndpointUri = new Uri(restEndpointArgument.Value);

            if (brokerUrlsArgument.Defined == true)
            {
                foreach (string brokerUrl in brokerUrlsArgument.Value)
                {
                    BrokerUrls.Add(new Uri(brokerUrl, UriKind.Absolute));
                }
            }
        }
    }
}