﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;

namespace Nano.Server
{
    public enum ReportContext : int
    {
        None = 0,
        Load,
        Discovery, 
        Report, 
        Monitor, 
        Service, 
        Client, 
        Session,
        Security
    }

    public interface IReportManager : IServerComponent
    {
        void Initialize(IServer serverContext, IReporter reporter); 
        
        IReporter BeginReport(ReportContext context, string name, out Uri identifier); 
        
        void EndReport(Uri identifier); 
    }
}