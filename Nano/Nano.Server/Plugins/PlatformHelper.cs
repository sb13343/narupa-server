﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;

namespace Nano.Server.Plugins
{
    /// <summary>
    /// Helper methods for determining platform.
    /// </summary>
    public static class PlatformHelper
    {
        /// <summary>
        /// Enum Platform
        /// </summary>
        public enum Platform
        {
            /// <summary>
            /// Windows Platform.
            /// </summary>
            Windows,

            /// <summary>
            /// Linux Platform.
            /// </summary>
            Linux,

            /// <summary>
            /// OS X Platform
            /// </summary>
            OSX
        }

        /// <summary>
        /// Returns the platform that is running.
        /// </summary>
        /// <returns>Platform.</returns>
        /// <remarks> http://stackoverflow.com/questions/10138040/how-to-detect-properly-windows-linux-mac-operating-systems </remarks>
        public static Platform RunningPlatform()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    // Well, there are chances MacOSX is reported as Unix instead of MacOSX.
                    // Instead of platform check, we'll do a feature checks (Mac specific root folders)
                    if (Directory.Exists("/Applications")
                        & Directory.Exists("/System")
                        & Directory.Exists("/Users")
                        & Directory.Exists("/Volumes"))
                        return Platform.OSX;
                    else
                        return Platform.Linux;

                case PlatformID.MacOSX:
                    return Platform.OSX;

                default:
                    return Platform.Windows;
            }
        }
    }
}