﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Xml;

namespace Nano.Server.Plugins
{
    /// <summary>
    /// Loads plugins and sets appropriate paths.
    /// </summary>
    /// <seealso cref="Nano.Loading.ILoadable" />
    public class PluginLoader : ILoadable
    {
        /// <summary>
        /// The manifest file for the plugin.
        /// </summary>
        public string ManifestFile;

        private static IReporter reporter;
        private string name;

        /// <summary>
        /// Given a path, appends the current runtime details.
        /// </summary>
        /// <param name="pluginPath">The plugin path.</param>
        /// <returns>System.String.</returns>
        /// <remarks>
        /// Useful for resolving which library/native code to run. For example, in debug mode on a 32-bit process, this method will append "/x86/Debug" to the path.
        /// </remarks>
        public static string AppendConfigurationAndArchitectureToPath(string pluginPath)
        {
            pluginPath = GetPlatformPath(pluginPath);
            //Set up native paths for plugin.
            if (IntPtr.Size == 8)
            {
                pluginPath += "/x64";
            }
            else
            {
                pluginPath += "/x86";
            }

            string finalPluginPath = pluginPath + "/Release";

            AddDebugPath(ref finalPluginPath, ref pluginPath);

            //AddReleasePath(ref pluginPath);
            return Helper.ResolvePath(finalPluginPath);
        }

        /// <summary>
        /// Scans the plugin directory and loads any plugins it finds there.
        /// </summary>
        /// <param name="reporter">The reporter.</param>
        /// <param name="pluginDirectory">The plugin directory.</param>
        public static void ScanPluginDirectory(IReporter reporter, string pluginDirectory = "^/Plugins")
        {
            PluginLoader.reporter = reporter;
            LoadContext context = new LoadContext(PluginLoader.reporter);
            pluginDirectory = Helper.ResolvePath(pluginDirectory);

            reporter.PrintEmphasized("Searching for plugins in: " + pluginDirectory);

            if (Directory.Exists(pluginDirectory) == false)
            {
                PluginLoader.reporter.PrintWarning(ReportVerbosity.Normal, "Could not find plugin directory!");
                return;
            }
            
            foreach (string dir in Directory.GetDirectories(Helper.ResolvePath(pluginDirectory)))
            {
                string pluginManifestPath = $"{dir}/PluginManifest.xml";
                
                if (File.Exists(Helper.ResolvePath(pluginManifestPath)))
                {
                    PluginManifest manifest = LoadPluginManifest(pluginManifestPath, context);
                    PluginLoader.reporter.PrintDetail("Found plugin {0}, loading assemblies...", manifest.Name);
                    
                    try
                    {
                        LoadPlugin(manifest);
                    }
                    catch (Exception e)
                    {
                        string error = "Failed to load plugin: " + manifest.Name;
                        PluginLoader.reporter.PrintException(e, error);
                        context.Error(error);
                    }
                }
                else
                {
                    string error = "Missing manifest for plugin in path: " + dir;
                    context.Error(error);
                    PluginLoader.reporter.PrintError(error);
                }
            }
            
            reporter.PrintDebug("State of library path variable: {0}", LibraryPathManager.GetPath());
        }

        /// <summary>
        /// Loads this instance from the specified node.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="node">The node.</param>
        public void Load(LoadContext context, XmlNode node)
        {
            name = Helper.GetAttributeValue(node, "Name", "");
            ManifestFile = Helper.GetAttributeValue(node, "ManifestFile", "");
            if (ManifestFile == "")
                context.Error("Missing manifest file for plugin " + name, true, true, SimboxExceptionType.InvalidPlugin);

            PluginManifest manifest = LoadPluginManifest(ManifestFile, context);

            LoadPlugin(manifest);
        }

        /// <summary>
        /// Saves this instance to the specified element.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="element">The element.</param>
        /// <returns>XmlElement.</returns>
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", name);
            Helper.AppendAttributeAndValue(element, "ManifestFile", ManifestFile);
            element.AppendChild(Loader.SaveObject(context, element, ManifestFile));
            return element;
        }

        /// <summary>
        /// Adds the debug path to the specified string.
        /// </summary>
        /// <param name="s">The s.</param>
        [Conditional("DEBUG")]
        private static void AddDebugPath(ref string finalPluginPath, ref string s)
        {
            finalPluginPath = s + "/Debug";
        }

        private static string GetPlatformPath(string pluginPath)
        {
            PlatformHelper.Platform platform = PlatformHelper.RunningPlatform();
            switch (platform)
            {
                case PlatformHelper.Platform.Windows:
                    pluginPath += "/Windows";
                    break;

                case PlatformHelper.Platform.Linux:
                    pluginPath += "/UNIX";
                    break;

                case PlatformHelper.Platform.OSX:
                    pluginPath += "/OSX";
                    break;

                default:
                    break;
            }
            return pluginPath;
        }

        /// <summary>
        /// Loads the managed assemblies required for the plugin.
        /// </summary>
        /// <param name="pluginRootPath">The plugin root path.</param>
        /// <param name="assemblyNames">The assembly names.</param>
        /// <remarks>
        /// The plugin is expected to have the following structure for managed code:
        /// PluginRoot/Managed/Plugin.dll
        /// If this structure is not found, then the following architecture specific path is tried:
        /// PluginRoot/Managed/[Platform]/[Architecture]/[Debug/Release]
        /// </remarks>
        /// <exception cref="Exception">
        /// Failed to find appropriate native path for current platform/runtime configuration: " + pluginManagedPath
        /// or
        /// Could not find assembly to load types from: " + assemblyNames + ", for plugin path: " + pluginRootPath
        /// </exception>
        private static void LoadManagedAssemblies(string pluginRootPath, List<LoadableAssembly> assemblyNames)
        {
           
            string pluginManagedPath = pluginRootPath + "/Managed";
            bool foundDlls = false; 
            
            reporter.PrintDetail("Scanning for managed assemblies from path {0}", pluginManagedPath);
            string[] dlls = Directory.GetFiles(pluginManagedPath, "*.dll");
            if (dlls.Length > 0) foundDlls = true;

            if (!foundDlls)
            {
            
                reporter.PrintDetail($"No DLLs found in path: {pluginManagedPath}, looking for architecture specific path...");
                pluginManagedPath = AppendConfigurationAndArchitectureToPath(pluginManagedPath);

                if (Directory.Exists(Helper.ResolvePath(pluginManagedPath)) == false)
                    throw new Exception($"There were no managed DLLs in the path {pluginRootPath}/Managed, and the appropriate managed path for current platform/runtime configuration does not exist: {pluginManagedPath}");
                
                PathManager.Add(pluginManagedPath);
                reporter.PrintDetail("Loading managed assemblies from path {0}", pluginManagedPath);
                dlls = Directory.GetFiles(pluginManagedPath, "*.dll");
            }

            if (dlls.Length == 0)
            {
                throw new Exception(
                    $"There were no managed libraries to load from the plugin path {pluginManagedPath}, check configuration.");
            }
            
            Dictionary<string, Assembly> assemblies = new Dictionary<string, Assembly>();
            //Loop over and load all the assemblies.
            foreach (string dll in dlls)
            {
                assemblies.Add(Path.GetFileName(dll), Assembly.LoadFrom(dll));
                reporter.PrintDetail("Loaded assembly: {0}", dll);
            }
            //Loop over all the assemblies that need to be scanned by ILoadable.
            foreach (LoadableAssembly loadable in assemblyNames)
            {
                try
                {
                    Assembly assembly = assemblies[loadable.Name];

                    LoadManager.ScanAssembly(assembly);
                    reporter.PrintDetail("Scanned assembly for LoadManager {0}", assembly);
                }
                catch (KeyNotFoundException e)
                {
                    throw new Exception("Could not find assembly to load types from: " + loadable.Name + ", for plugin path: " + pluginRootPath, e);
                }
            }
        }

        /// <summary>
        /// Loads the native paths for Windows systems, where paths can be added to at runtime.
        /// </summary>
        /// <param name="pluginPath">The plugin path.</param>
        /// <exception cref="Exception">Failed to find appropriate native path for current platform/runtime configuration: " + pluginPath</exception>
        /// <remarks>
        /// The plugin is expected to have one of the following structures for native code:
        /// PluginRoot/Native/ 
        /// PluginRoot/Native/[Platform]/[Architecture]/[Debug/Release]
        /// </remarks>
        private static void LoadNativePaths(string pluginPath)
        {
            pluginPath += "/Native";
            
            
            string nativePluginPath = AppendConfigurationAndArchitectureToPath(pluginPath);
            nativePluginPath = Helper.ResolvePath(nativePluginPath);
            if (Directory.Exists(nativePluginPath) == false)
            {
                if (Directory.Exists(Helper.ResolvePath(pluginPath)))
                {
                    nativePluginPath = Helper.ResolvePath(pluginPath);
                }
                else
                {
                    return;
                }
            }

            LibraryPathManager.Add(nativePluginPath);
            PathManager.Add(nativePluginPath);
        }

        /// <summary>
        /// Loads the plugin.
        /// </summary>
        /// <param name="pluginManifest">The plugin manifest.</param>
        private static void LoadPlugin(PluginManifest pluginManifest)
        {
            reporter.PrintNormal($"Loading plugin {pluginManifest.Name} from path {pluginManifest.PluginPath}");
            LoadManagedAssemblies(pluginManifest.PluginPath, pluginManifest.AssemblyNames);

            //Only load native paths on windows.
            if (IsRunningWindows())
            {
                LoadNativePaths(pluginManifest.PluginPath);
            }
        }

        private static bool IsRunningWindows()
        {
            OperatingSystem os = Environment.OSVersion;
            PlatformID     pid = os.Platform;
            switch (pid)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                    return true;
                default:
                    return false;
            }
            
        }

        /// <summary>
        /// Loads the plugin manifest.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="context">The context.</param>
        /// <returns>PluginManifest.</returns>
        private static PluginManifest LoadPluginManifest(string path, LoadContext context)
        {
            XmlDocument doc = new XmlDocument();
            path = Helper.ResolvePath(path);
            try
            {
                doc.Load(path);
            }
            catch (Exception e)
            {
                string error = "Failed to load manifest file for plugin with path: " + path;
                reporter.PrintException(e, error);
                context.Error(error);
            }
            PluginManifest manifest = new PluginManifest();
            manifest.Load(context, doc.DocumentElement);

            manifest.PluginPath = Path.GetDirectoryName(path);
            return manifest;
        }
  
    }
}