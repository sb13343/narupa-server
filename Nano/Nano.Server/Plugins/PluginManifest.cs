﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using System;
using System.Collections.Generic;
using System.Xml;

namespace Nano.Server.Plugins
{
    [XmlName("Assembly")]
    internal class LoadableAssembly : ILoadable
    {
        public string Name;

        public void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", "");
            if (Name == "")
            {
                string error = "Missing attribute Name in Assembly element";
                context.Error(error, true, true, SimboxExceptionType.InvalidPlugin);
            }
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);
            return element;
        }
    }

    [XmlName("Plugin")]
    internal class PluginManifest : ILoadable
    {
        /// <summary>
        /// The Simbox API Version the plugin is compatible with.
        /// </summary>
        public string APIVersion;

        /// <summary>
        /// The list of managed assemblies that implement <see cref="ILoadable"/>.
        /// </summary>
        public List<LoadableAssembly> AssemblyNames = new List<LoadableAssembly>();

        /// <summary>
        /// The name of the plugin.
        /// </summary>
        public string Name;

        /// <summary>
        /// The plugin path, from which to load the plugin.
        /// </summary>
        public string PluginPath;

        public void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", "");
            if (Name == "")
            {
                string error = "Missing plugin name in plugin manifest.";
                context.Error(error, true);
                throw new Exception(error);
            }
            APIVersion = Helper.GetAttributeValue(node, "APIVersion", "");
            AssemblyNames.AddRange(Loader.LoadObjects<LoadableAssembly>(context, node.SelectSingleNode("LoadableAssemblies")));
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);
            Helper.AppendAttributeAndValue(element, "APIVersion", APIVersion);
            element.AppendChild(Loader.SaveObjects(context, element, AssemblyNames, "LoadableAssemblies"));
            return element;
        }
    }
}